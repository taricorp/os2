 DEFINE PAGE1D, SPACE=ROM
 SEGMENT PAGE1D

 PUBLIC _JErrorNo,_UnlockFlash,_GetKey
 EXTERN JErrorNo,UnlockFlash,GetKey,keyscnlnk,Rec1stByteNC,RecAByteIO,MemClear,ClrTxtShd
 EXTERN ClrLCDFull,PutS,GetCSC,CursorOff,CursorOn,hideCursor,showCursor,ClrScrnFull,KeyToString
 EXTERN JForceCmdNoChar,JForceCmd,PowerOff,LCD_DRIVERON

;4000h:
_JErrorNo:
       DW JErrorNo
       DB 00h
;_FontHook		equ 4003h
       DW 0000h
       DB 00h
;_call_character_hook	equ 4006h ;calls character(localize) hook
       DW 0000h
       DB 00h
;_ldHLind		equ 4009h ;l=a=(hl),h=(hl+1)
       DW 0000h
       DB 00h
;_CpHLDE			equ 400Ch
       DW 0000h
       DB 00h
;_DivHLBy10		equ 400Fh
       DW 0000h
       DB 00h
;_DivHLByA		equ 4012h
       DW 0000h
       DB 00h
;_kdbScan		equ 4015h
       DW 0000h
       DB 00h
;_GetCSC			equ 4018h ;key board scan codes
_GetCSC:
       DW GetCSC
       DB 00h
;_coorMon		equ 401Bh ;
       DW 0000h
       DB 00h
;_Mon			equ 401Eh ;system monitor, customized through the context vectors
       DW 0000h
       DB 0F1h
;_monForceKey		equ 4021h ;
       DW 0000h
       DB 00h
;_sendKPress		equ 4024h
       DW 0000h
       DB 00h
_JForceCmdNoChar:
       DW JForceCmdNoChar
       DB 00h
_JForceCmd:
       DW JForceCmd
       DB 00h
;_sysErrHandler		equ 402Dh ;loads error context? sp=(onSP)
       DW 0000h
       DB 00h
;_newContext		equ 4030h ;(kbdKey)=0, loads context, restores page in 4000h-7fffh
       DW 0000h
       DB 00h
;_newContext0		equ 4033h ;loads context
       DW 0000h
       DB 00h
;_cxPutAway		equ 4036h ;
       DW 0000h
       DB 00h
;_cxPutAway2		equ 4039h ;same but also does a B_CALL CanAlphIns
       DW 0000h
       DB 00h
;_cxPPutAway		equ 403Ch
       DW 0000h
       DB 00h
;_cxSizeWind		equ 403Fh
       DW 0000h
       DB 00h
;_cxErrEP		equ 4042h
       DW 0000h
       DB 00h
;_cxMain			equ 4045h
       DW 0000h
       DB 00h
;_monErrHand		equ 4048h ;installs errorhandler to start of ROM call, loads error context, jumps to mon
       DW 0000h
       DB 00h
;_AppInit		equ 404Bh ;loads vector data at hl to cxMain and the rest of the vectors
       DW 0000h
       DB 00h
;_resetRam		equ 404Eh
       DW 0000h
       DB 0F3h
;_lcd_busy		equ 4051h ;wait till bit 1 of port 2 is set
       DW 0000h
       DB 00h
;_Min			equ 4054h ;op1 = lowest number between op1 and op2
       DW 0000h
       DB 00h
;_Max			equ 4057h ;op1 = highest number between op1 and op2 
       DW 0000h
       DB 00h
;405Ah
       DW 0000h
       DB 00h
;405Dh
       DW 0000h
       DB 00h
;_Trunc			equ 4060h
       DW 0000h
       DB 00h
;_InvSub			equ 4063h
       DW 0000h
       DB 00h
;_Times2			equ 4066h
       DW 0000h
       DB 00h
;_Plus1			equ 4069h ;op1=op1+1
       DW 0000h
       DB 00h
;_Minus1			equ 406Ch
       DW 0000h
       DB 00h
;_FPSub			equ 406Fh
       DW 0000h
       DB 00h
;_FPAdd			equ 4072h
       DW 0000h
       DB 00h
;_DToR			equ 4075h
       DW 0000h
       DB 00h
;_RToD			equ 4078h
       DW 0000h
       DB 00h
;_Cube			equ 407Bh
       DW 0000h
       DB 00h
;_TimesPt5		equ 407Eh
       DW 0000h
       DB 00h
;_FPSquare		equ 4081h
       DW 0000h
       DB 00h
;_FPMult			equ 4084h ;op1=op1*op2
       DW 0000h
       DB 00h
;_LJRND			equ 4087h ;adjusts op1 if 0s precede the actual number... rounding? when
       DW 0000h
       DB 00h
;_InvOP1SC		equ 408Ah
       DW 0000h
       DB 00h
;_InvOP1S		equ 408Dh
       DW 0000h
       DB 00h
;_InvOP2S		equ 4090h
       DW 0000h
       DB 0F5h
;_frac			equ 4093h
       DW 0000h
       DB 00h
;_fprecip		equ 4096h
       DW 0000h
       DB 00h
;_fpdiv			equ 4099h
       DW 0000h
       DB 00h
;_SqRoot			equ 409Ch
       DW 0000h
       DB 00h
;_RndGuard		equ 409Fh
       DW 0000h
       DB 00h
;_RnFx			equ 40A2h
       DW 0000h
       DB 00h
;_int			equ 40A5h
       DW 0000h
       DB 00h
;_Round			equ 40A8h
       DW 0000h
       DB 00h
;_LnX			equ 40ABh
       DW 0000h
       DB 00h
;_LogX			equ 40AEh
       DW 0000h
       DB 0F2h
;_LJNORND		equ 40B1h ;like _LJRND but no rounding
       DW 0000h
       DB 00h
;_EToX			equ 40B4h
       DW 0000h
       DB 00h
;_TenX			equ 40B7h
       DW 0000h
       DB 00h
;_SinCosRad		equ 40BAh
       DW 0000h
       DB 00h
;_Sin			equ 40BDh
       DW 0000h
       DB 00h
;_cos			equ 40C0h
       DW 0000h
       DB 00h
;_Tan			equ 40C3h
       DW 0000h
       DB 00h
;_SinHCosH		equ 40C6h
       DW 0000h
       DB 00h
;_TanH			equ 40C9h
       DW 0000h
       DB 00h
;_cosh			equ 40CCh
       DW 0000h
       DB 00h
;_SinH			equ 40CFh
       DW 0000h
       DB 00h
;_ACosRad		equ 40D2h
       DW 0000h
       DB 00h
;_ATanRad		equ 40D5h
       DW 0000h
       DB 00h
;_ATan2Rad		equ 40D8h
       DW 0000h
       DB 00h
;_ASinRad		equ 40DBh
       DW 0000h
       DB 00h
;_ACos			equ 40DEh
       DW 0000h
       DB 0F4h
;_ATan			equ 40E1h
       DW 0000h
       DB 00h
;_ASin			equ 40E4h
       DW 0000h
       DB 00h
;_ATan2			equ 40E7h
       DW 0000h
       DB 00h
;_ATanH			equ 40EAh
       DW 0000h
       DB 00h
;_ASinH			equ 40EDh
       DW 0000h
       DB 00h
;_ACosH			equ 40F0h
       DW 0000h
       DB 00h
;_PtoR			equ 40F3h
       DW 0000h
       DB 00h
;_RToP			equ 40F6h
       DW 0000h
       DB 00h
;_HLTimes9		equ 40F9h
       DW 0000h
       DB 00h
;_CkOP1Cplx		equ 40FCh
       DW 0000h
       DB 00h
;_CkOP1Real		equ 40FFh
       DW 0000h
       DB 00h
;_Angle			equ 4102h
       DW 0000h
       DB 00h
;_COP1Set0		equ 4105h
       DW 0000h
       DB 00h
;_CpOP4OP3		equ 4108h
       DW 0000h
       DB 00h
;_Mov9OP2Cp		equ 410Bh
       DW 0000h
       DB 00h
;_AbsO1O2Cp		equ 410Eh
       DW 0000h
       DB 00h
;_cpop1op2		equ 4111h
       DW 0000h
       DB 00h
;_OP3ToOP4		equ 4114h
       DW 0000h
       DB 00h
;_OP1ToOP4		equ 4117h
       DW 0000h
       DB 00h
;_OP2ToOP4		equ 411Ah
       DW 0000h
       DB 00h
;_OP4ToOP2		equ 411Dh
       DW 0000h
       DB 00h
;4120h
       DW 0000h
       DB 00h
;_OP1ToOP3		equ 4123h
       DW 0000h
       DB 00h
;_OP5ToOP2		equ 4126h
       DW 0000h
       DB 00h
;_OP5ToOP6		equ 4129h
       DW 0000h
       DB 00h
;_OP5ToOP4		equ 412Ch
       DW 0000h
       DB 00h
;_OP1ToOP2		equ 412Fh
       DW 0000h
       DB 00h
;_OP6ToOP2		equ 4132h
       DW 0000h
       DB 00h
;_OP6ToOP1		equ 4135h
       DW 0000h
       DB 00h
;_OP4ToOP1		equ 4138h
       DW 0000h
       DB 00h
;_OP5ToOP1		equ 413Bh
       DW 0000h
       DB 00h
;_OP3ToOP1		equ 413Eh
       DW 0000h
       DB 00h
;_OP6ToOP5		equ 4141h
       DW 0000h
       DB 00h
;_OP4ToOP5		equ 4144h
       DW 0000h
       DB 00h
;_OP3ToOP5		equ 4147h
       DW 0000h
       DB 00h
;_OP2ToOP5		equ 414Ah
       DW 0000h
       DB 00h
;_OP2ToOP6		equ 414Dh
       DW 0000h
       DB 00h
;_OP1ToOP6		equ 4150h
       DW 0000h
       DB 00h
;_OP1ToOP5		equ 4153h
       DW 0000h
       DB 00h
;_OP2ToOP1		equ 4156h
       DW 0000h
       DB 00h
;_Mov11B			equ 4159h
       DW 0000h
       DB 00h
;_Mov10B			equ 415Ch
       DW 0000h
       DB 00h
;_Mov9B			equ 415Fh
       DW 0000h
       DB 00h
;_mov9B2			equ 4162h ;points to _mov9B
       DW 0000h
       DB 00h
;_Mov8B			equ 4165h
       DW 0000h
       DB 00h
;_Mov7B			equ 4168h
       DW 0000h
       DB 00h
;_Mov7B2			equ 416Bh ;same pointer as _Mov7B
       DW 0000h
       DB 00h
;_OP2ToOP3		equ 416Eh
       DW 0000h
       DB 00h
;_OP4ToOP3		equ 4171h
       DW 0000h
       DB 00h
;_OP5ToOP3		equ 4174h
       DW 0000h
       DB 00h
;_OP4ToOP6		equ 4177h
       DW 0000h
       DB 00h
;_Mov9ToOP1		equ 417Ah
       DW 0000h
       DB 00h
;_Mov9OP1OP2		equ 417Dh
       DW 0000h
       DB 00h
;_Mov9ToOP2		equ 4180h
       DW 0000h
       DB 00h
;_MovFrOP1		equ 4183h
       DW 0000h
       DB 00h
;_OP4Set1		equ 4186h
       DW 0000h
       DB 00h
;_OP3Set1		equ 4189h
       DW 0000h
       DB 00h
;_OP2Set8		equ 418Ch
       DW 0000h
       DB 00h
;_OP2Set5		equ 418Fh
       DW 0000h
       DB 00h
;_OP2SetA		equ 4192h
       DW 0000h
       DB 00h
;_OP2Set4		equ 4195h
       DW 0000h
       DB 00h
;_OP2Set3		equ 4198h
       DW 0000h
       DB 00h
;_OP1Set1		equ 419Bh
       DW 0000h
       DB 00h
;_OP1Set4		equ 419Eh
       DW 0000h
       DB 00h
;_OP1Set3		equ 41A1h
       DW 0000h
       DB 00h
;_OP3Set2		equ 41A4h
       DW 0000h
       DB 00h
;_OP1Set2		equ 41A7h
       DW 0000h
       DB 00h
;_OP2Set2		equ 41AAh
       DW 0000h
       DB 00h
;_OP2Set1		equ 41ADh
       DW 0000h
       DB 00h
;_Zero16D		equ 41B0h
       DW 0000h
       DB 00h
;_OP5Set0		equ 41B3h
       DW 0000h
       DB 00h
;_OP4Set0		equ 41B6h
       DW 0000h
       DB 00h
;_OP3Set0		equ 41B9h
       DW 0000h
       DB 00h
;_OP2Set0		equ 41BCh
       DW 0000h
       DB 00h
;_OP1Set0		equ 41BFh
       DW 0000h
       DB 00h
;_OPSet0			equ 41C2h ;hl = location to write floating point 0
       DW 0000h
       DB 00h
;_ZeroOP1		equ 41C5h
       DW 0000h
       DB 00h
;_ZeroOP2		equ 41C8h
       DW 0000h
       DB 00h
;_ZeroOP3		equ 41CBh
       DW 0000h
       DB 00h
;_ZeroOP			equ 41CEh
       DW 0000h
       DB 00h
;_ClrLp			equ 41D1h
       DW 0000h
       DB 00h
;_ShRAcc			equ 41D4h ;move high nibble in a to low nibble
       DW 0000h
       DB 00h
;_ShLAcc			equ 41D7h ;move low nibble in a to high nibble
       DW 0000h
       DB 00h
;_ShR18			equ 41DAh ;insert a 0 nibble at high nibble of (hl), shift 9 bytes 1 nibble to right
       DW 0000h
       DB 00h
;_SHR18A			equ 41DDh ;insert low nibble in a at high nibble of (hl), shift 9 bytes 1 nibble to right
       DW 0000h
       DB 00h
;_SHR16			equ 41E0h ;insert a 0 nibble at highnibble of (hl), shift 8 bytes 1 nibble to right
       DW 0000h
       DB 00h
;_SHR14			equ 41E3h ;insert low nibble in a at high nibble of (hl), shift 7 bytes 1 nibble to right
       DW 0000h
       DB 00h
;_SHL16			equ 41E6h ;insert nibble of 0 in low nibble of (hl), shift 8 bytes (before and including (hl)) to the left 1 nibble
       DW 0000h
       DB 00h
;_SHL14			equ 41E9h ;insert low nibble of a in low nibble of (hl), shift 7 bytes (before and including (hl)) to the left 1 nibble
       DW 0000h
       DB 00h
;_SRDO1			equ 41ECh
       DW 0000h
       DB 00h
;_SHRDRND		equ 41EFh
       DW 0000h
       DB 00h
;_MANTPA			equ 41F2h ;adds the value of a to hl which points to the end of the bcd 7 bytes long
       DW 0000h
       DB 00h
;_ADDPROP		equ 41F5h ;adds the value of a to hl which points to the end of the bcd (b bytes long)
       DW 0000h
       DB 00h
;_ADDPROPLP		equ 41F8h ;adds the value of a and carry to hl which points to the end of the bcd (b bytes long)
       DW 0000h
       DB 00h
;_ADD16D 		equ 41FBh ;adds the bcd numbers at (hl-7) and (de-7)
       DW 0000h
       DB 00h
;_ADD14D			equ 41FEh ;adds the bcd numbers at (hl-6) and (de-6)
       DW 0000h
       DB 00h
;_SUB16D			equ 4201h ;subtracts bcd numbers at (hl-7) and (de-7)
       DW 0000h
       DB 00h
;_SUB14D			equ 4204h ;subtracts bcd numbers at (hl-6) and (de-6)
       DW 0000h
       DB 00h
;_OP2ExOP6		equ 4207h
       DW 0000h
       DB 00h
;_OP5ExOP6		equ 420Ah
       DW 0000h
       DB 00h
;_OP1ExOP5		equ 420Dh
       DW 0000h
       DB 00h
;_OP1ExOP6		equ 4210h
       DW 0000h
       DB 00h
;_OP2ExOP4		equ 4213h
       DW 0000h
       DB 00h
;_OP2ExOP5		equ 4216h
       DW 0000h
       DB 00h
;_OP1ExOP3		equ 4219h
       DW 0000h
       DB 00h
;_OP1ExOP4		equ 421Ch
       DW 0000h
       DB 00h
;_OP1ExOP2		equ 421Fh
       DW 0000h
       DB 00h
;_ExLp			equ 4222h
       DW 0000h
       DB 00h
;_CkOP1C0		equ 4225h
       DW 0000h
       DB 00h
;_CkOP1FP0		equ 4228h
       DW 0000h
       DB 00h
;_CkOP2FP0		equ 422Bh
       DW 0000h
       DB 00h
;_PosNo0Int		equ 422Eh
       DW 0000h
       DB 00h
;_CKPosInt		equ 4231h
       DW 0000h
       DB 00h
;_CKInt			equ 4234h
       DW 0000h
       DB 00h
;_CKOdd			equ 4237h
       DW 0000h
       DB 00h
;_CKOP1M			equ 423Ah
       DW 0000h
       DB 00h
;_GETCONOP1		equ 423Dh ;a=0 opX=57.29577951308232 (1 radian in degrees)
       DW 0000h
       DB 00h
;_GETCONOP2		equ 4240h ;a=1 opX=1.570796326794897 (90 deg = pi/2)
       DW 0000h
       DB 00h
;_PIDIV2			equ 4243h ;not code, but a pointer to:	.db 80h,15h,70h,79h,63h,26h,79h,48h,97h
       DW 0000h
       DB 00h
;_PIDIV4			equ 4246h ;				.db 7fh,78h,53h,98h,16h,33h,97h,44h,83h
       DW 0000h
       DB 00h
;_PItimes2		equ 4249h ;not code, but a pointer to a 2*pi in non-OP format (no exponent byte)
       DW 0000h
       DB 00h
;_PI			equ 424Ch ;not code, but a pointer to a pi in non-OP format (no exponent byte)
       DW 0000h
       DB 00h
;_ExpToHex		equ 424Fh
       DW 0000h
       DB 00h
;_OP1ExpToDec		equ 4252h
       DW 0000h
       DB 00h
;_ckop2pos		equ 4255h
       DW 0000h
       DB 00h
;_CkOP1Pos		equ 4258h
       DW 0000h
       DB 00h
;_ClrOP2S		equ 425Bh
       DW 0000h
       DB 00h
;_ClrOP1S		equ 425Eh
       DW 0000h
       DB 00h
;_FDIV100		equ 4261h ;op1=op1/100
       DW 0000h
       DB 00h
;_FDIV10			equ 4264h ;op1=op1/10
       DW 0000h
       DB 00h
;_DecO1Exp		equ 4267h ;decrease exponent by 1, this can go from 0 to FF
       DW 0000h
       DB 00h
;_INCO1EXP		equ 426Ah ;op1=op1*10
       DW 0000h
       DB 00h
;_INCEXP			equ 426Dh ;hl points to the floating point's exponent to be multiplied by 10
       DW 0000h
       DB 00h
;_CkValidNum		equ 4270h
       DW 0000h
       DB 00h
;_GETEXP			equ 4273h ;a=OP1's exponent, carry set if negative exponent, Z if e0
       DW 0000h
       DB 00h
;_HTimesL		equ 4276h
       DW 0000h
       DB 00h
;_EOP1NotReal		equ 4279h
       DW 0000h
       DB 00h
;_ThetaName		equ 427Ch
       DW 0000h
       DB 00h
;_RName			equ 427Fh
       DW 0000h
       DB 00h
;_REGEQNAME		equ 4282h
       DW 0000h
       DB 00h
;_RECURNNAME		equ 4285h
       DW 0000h
       DB 00h
;_XName			equ 4288h
       DW 0000h
       DB 00h
;_YName			equ 428Bh
       DW 0000h
       DB 00h
;_TName			equ 428Eh
       DW 0000h
       DB 00h
;_REALNAME		equ 4291h
       DW 0000h
       DB 00h
;_SETesTOfps		equ 4294h ;moves the word at fps to es
       DW 0000h
       DB 00h
;_markTableDirty		equ 4297h ;looks up table variable and marks VAT entry as "dirty" or selected
       DW 0000h
       DB 00h
;_OP1MOP2EXP		equ 429Ah ;op1's exponent = op1's expoent - op2's exponent
       DW 0000h
       DB 00h
;_OP1EXPMinusE		equ 429Dh ;a=(op1+1)-e
       DW 0000h
       DB 00h
;_CHKERRBREAK		equ 42A0h
       DW 0000h
       DB 00h
;_isA2ByteTok		equ 42A3h
       DW 0000h
       DB 00h
;_GETLASTENTRY		equ 42A6h
       DW 0000h
       DB 00h
;_GETLASTENTRYPTR	equ 42A9h
       DW 0000h
       DB 00h
;_REGCLRCHNG		equ 42ACh
       DW 0000h
       DB 00h
;_RESETWINTOP		equ 42AFh ;takes into account grfsplitoverride and grfsplit flags
       DW 0000h
       DB 00h
;_SetYUp			equ 42B2h ;loads 7 to port 10... what does this do?
       DW 0000h
       DB 00h
;_SetXUp			equ 42B5h ;loads 5 to port 10...
       DW 0000h
       DB 00h
;_ISO1NONTLSTorPROG	equ 42B8h ;checks if op1 contains a list, program, group, or appvar obj name
       DW 0000h
       DB 00h
;_ISO1NONTEMPLST		equ 42BBh ;checks if op1 contains a list (why would op1+1 contain 01, or 0d for a var name?)
       DW 0000h
       DB 00h
;_IS_A_LSTorCLST		equ 42BEh ;checks if a = 1 or 0Dh
       DW 0000h
       DB 00h
;_CHK_HL_999		equ 42C1h ;returns nc if less than 999, throws invalid dim error if greater than or equal to 999
       DW 0000h
       DB 00h
;_equ_or_newequ		equ 42C4h
       DW 0000h
       DB 00h
;_errd_op1notpos		equ 42C7h
       DW 0000h
       DB 00h
;_ErrD_OP1Not_R		equ 42CAh
       DW 0000h
       DB 00h
;_ErrD_OP1NotPosInt 	equ 42CDh
       DW 0000h
       DB 00h
;_ErrD_OP1_LE_0		equ 42D0h
       DW 0000h
       DB 00h
;_ErrD_OP1_0		equ 42D3h
       DW 0000h
       DB 00h
;_FINDSYM_GET_SIZE 	equ 42D6h ;like findsym, but on output hl is the size of the variable
       DW 0000h
       DB 00h
;_STO_STATVAR		equ 42D9h
       DW 0000h
       DB 00h
;_Rcl_StatVar		equ 42DCh
       DW 0000h
       DB 00h
;_CkOP2Real		equ 42DFh
       DW 0000h
       DB 00h
;_GET_X_INDIRECT		equ 42E2h ;whatever this is, it uses the imathptrX locations
       DW 0000h
       DB 00h
;_MemChk			equ 42E5h
       DW 0000h
       DB 00h
;_CMPPRGNAMLEN1		equ 42E8h ;gets variable name length from HL
       DW 0000h
       DB 00h
;_CMPPRGNAMLEN		equ 42EBh ;gets variable name length from OP1
       DW 0000h
       DB 00h
;_FINDPROGSYM		equ 42EEh ;find the program whose name is in op1 (see chkfindsym in SDK)
       DW 0000h
       DB 00h
;_ChkFindSym		equ 42F1h
       DW 0000h
       DB 00h
;_FindSym		equ 42F4h
       DW 0000h
       DB 00h
;_InsertMem		equ 42F7h
       DW 0000h
       DB 00h
;_INSERTMEMA		equ 42FAh ;not sure how this differs from insertmem
       DW 0000h
       DB 00h
;_EnoughMem		equ 42FDh
       DW 0000h
       DB 00h
;_CMPMEMNEED		equ 4300h
       DW 0000h
       DB 00h
;_CREATEPVAR4		equ 4303h
       DW 0000h
       DB 00h
;_CREATEPVAR3		equ 4306h
       DW 0000h
       DB 00h
;_CREATEVAR3		equ 4309h
       DW 0000h
       DB 00h
;_CreateCplx		equ 430Ch
       DW 0000h
       DB 00h
;_CreateReal		equ 430Fh
       DW 0000h
       DB 00h
;_CreateTempRList	equ 4312h
       DW 0000h
       DB 00h
;_CreateRList		equ 4315h
       DW 0000h
       DB 00h
;_CREATETCLIST		equ 4318h
       DW 0000h
       DB 00h
;_CreateCList		equ 431Bh
       DW 0000h
       DB 00h
;_CreateTempRMat		equ 431Eh
       DW 0000h
       DB 00h
;_CreateRMat		equ 4321h
       DW 0000h
       DB 00h
;_CreateTempString	equ 4324h
       DW 0000h
       DB 00h
;_CreateStrng		equ 4327h
       DW 0000h
       DB 00h
;_Create0Equ		equ 432Ah
       DW 0000h
       DB 00h
;_CreateTempEqu		equ 432Dh
       DW 0000h
       DB 00h
;_CreateEqu		equ 4330h
       DW 0000h
       DB 00h
;_CreatePict		equ 4333h
       DW 0000h
       DB 00h
;_CreateGDB		equ 4336h
       DW 0000h
       DB 00h
;_CreateProg		equ 4339h
       DW 0000h
       DB 00h
;_CHKDEL			equ 433Ch
       DW 0000h
       DB 00h
;_CHKDELA		equ 433Fh
       DW 0000h
       DB 00h
;_ADJPARSER		equ 4342h
       DW 0000h
       DB 00h
;_ADJMATH		equ 4345h
       DW 0000h
       DB 00h
;_ADJM7			equ 4348h
       DW 0000h
       DB 00h
;_DELMEMA		equ 434Bh
       DW 0000h
       DB 00h
;_GET_FORM_NUM		equ 434Eh
       DW 0000h
       DB 00h
;_DelVar			equ 4351h
       DW 0000h
       DB 00h
;_DELVARIO		equ 4354h
       DW 0000h
       DB 00h
;_DelMem			equ 4357h
       DW 0000h
       DB 00h
;_DELVAR3D		equ 435Ah
       DW 0000h
       DB 00h
;_DELVAR3C		equ 435Dh
       DW 0000h
       DB 00h
;_DELVAR3DC		equ 4360h ;may be incorrect
       DW 0000h
       DB 00h
;_IsFixedName		equ 4363h
       DW 0000h
       DB 00h
;_DelVarEntry		equ 4366h
       DW 0000h
       DB 00h
;_DataSizeA		equ 4369h
       DW 0000h
       DB 00h
;_DataSize		equ 436Ch
       DW 0000h
       DB 00h
;_POPMCPLXO1		equ 436Fh
       DW 0000h
       DB 00h
;_POPMCPLX		equ 4372h
       DW 0000h
       DB 00h
;_MOVCPLX		equ 4375h
       DW 0000h
       DB 00h
;_popOP5			equ 4378h
       DW 0000h
       DB 00h
;_popOP3			equ 437Bh
       DW 0000h
       DB 00h
;_popOP1			equ 437Eh
       DW 0000h
       DB 00h
;_PopRealO6		equ 4381h
       DW 0000h
       DB 00h
;_PopRealO5		equ 4384h
       DW 0000h
       DB 00h
;_PopRealO4		equ 4387h
       DW 0000h
       DB 00h
;_PopRealO3		equ 438Ah
       DW 0000h
       DB 00h
;_PopRealO2		equ 438Dh
       DW 0000h
       DB 00h
;_PopRealO1		equ 4390h
       DW 0000h
       DB 00h
;_PopReal		equ 4393h
       DW 0000h
       DB 00h
;_FPOPCPLX		equ 4396h
       DW 0000h
       DB 00h
;_FPOPREAL		equ 4399h
       DW 0000h
       DB 00h
;_FPOPFPS		equ 439Ch
       DW 0000h
       DB 00h
;_DeallocFPS		equ 439Fh
       DW 0000h
       DB 00h
;_DeallocFPS1		equ 43A2h
       DW 0000h
       DB 00h
;_AllocFPS		equ 43A5h
       DW 0000h
       DB 00h
;_AllocFPS1		equ 43A8h
       DW 0000h
       DB 00h
;_PushRealO6		equ 43ABh
       DW 0000h
       DB 00h
;_PushRealO5		equ 43AEh
       DW 0000h
       DB 00h
;_PushRealO4		equ 43B1h
       DW 0000h
       DB 00h
;_PushRealO3		equ 43B4h
       DW 0000h
       DB 00h
;_PushRealO2		equ 43B7h
       DW 0000h
       DB 00h
;_PushRealO1		equ 43BAh
       DW 0000h
       DB 00h
;_PushReal		equ 43BDh
       DW 0000h
       DB 00h
;_PushOP5		equ 43C0h
       DW 0000h
       DB 00h
;_PushOP3		equ 43C3h
       DW 0000h
       DB 00h
;_PUSHMCPLXO3		equ 43C6h
       DW 0000h
       DB 00h
;_PushOP1		equ 43C9h
       DW 0000h
       DB 00h
;_PUSHMCPLXO1		equ 43CCh
       DW 0000h
       DB 00h
;_PUSHMCPLX		equ 43CFh
       DW 0000h
       DB 00h
;_ExMCplxO1		equ 43D2h
       DW 0000h
       DB 00h
;_Exch9			equ 43D5h
       DW 0000h
       DB 00h
;_CpyTo1FPS11		equ 43D8h
       DW 0000h
       DB 00h
;_CpyTo2FPS5		equ 43DBh
       DW 0000h
       DB 00h
;_CpyTo1FPS5		equ 43DEh
       DW 0000h
       DB 00h
;_CpyTo2FPS6		equ 43E1h
       DW 0000h
       DB 00h
;_CpyTo1FPS6		equ 43E4h
       DW 0000h
       DB 00h
;_CpyTo2FPS7		equ 43E7h
       DW 0000h
       DB 00h
;_CpyTo1FPS7		equ 43EAh
       DW 0000h
       DB 00h
;_CpyTo1FPS8		equ 43EDh
       DW 0000h
       DB 00h
;_CpyTo2FPS8		equ 43F0h
       DW 0000h
       DB 00h
;_CpyTo1FPS10		equ 43F3h
       DW 0000h
       DB 00h
;_CpyTo1FPS9		equ 43F6h
       DW 0000h
       DB 00h
;_CpyTo2FPS4		equ 43F9h
       DW 0000h
       DB 00h
;_CpyTo6FPS3		equ 43FCh
       DW 0000h
       DB 0FCh
;_CpyTo6FPS2		equ 43FFh
       DW 0000h
       DB 00h
;_CpyTo2FPS3		equ 4402h
       DW 0000h
       DB 00h
;_CPYCTO1FPS3		equ 4405h
       DW 0000h
       DB 00h
;_CpyTo1FPS3		equ 4408h
       DW 0000h
       DB 00h
;_CPYFPS3		equ 440Bh
       DW 0000h
       DB 00h
;_CpyTo1FPS4		equ 440Eh
       DW 0000h
       DB 00h
;_CpyTo3FPS2		equ 4411h
       DW 0000h
       DB 00h
;_CpyTo5FPST		equ 4414h
       DW 0000h
       DB 00h
;_CpyTo6FPST		equ 4417h
       DW 0000h
       DB 00h
;_CpyTo4FPST		equ 441Ah
       DW 0000h
       DB 00h
;_CpyTo3FPST		equ 441Dh
       DW 0000h
       DB 00h
;_CpyTo2FPST		equ 4420h
       DW 0000h
       DB 00h
;_CpyTo1FPST		equ 4423h
       DW 0000h
       DB 00h
;_CPYFPST		equ 4426h
       DW 0000h
       DB 00h
;_CpyStack		equ 4429h
       DW 0000h
       DB 00h
;_CpyTo3FPS1		equ 442Ch
       DW 0000h
       DB 00h
;_CpyTo2FPS1		equ 442Fh
       DW 0000h
       DB 00h
;_CpyTo1FPS1		equ 4432h
       DW 0000h
       DB 00h
;_CPYFPS1		equ 4435h
       DW 0000h
       DB 00h
;_CpyTo2FPS2		equ 4438h
       DW 0000h
       DB 00h
;_CpyTo1FPS2		equ 443Bh
       DW 0000h
       DB 00h
;_CPYFPS2		equ 443Eh
       DW 0000h
       DB 00h
;_CpyO3ToFPST		equ 4441h
       DW 0000h
       DB 00h
;_CpyO2ToFPST		equ 4444h
       DW 0000h
       DB 00h
;_CpyO6ToFPST		equ 4447h
       DW 0000h
       DB 00h
;_CpyO1ToFPST		equ 444Ah
       DW 0000h
       DB 00h
;_CpyToFPST		equ 444Dh
       DW 0000h
       DB 00h
;_CpyToStack		equ 4450h
       DW 0000h
       DB 00h
;_CpyO3ToFPS1		equ 4453h
       DW 0000h
       DB 00h
;_CpyO5ToFPS1		equ 4456h
       DW 0000h
       DB 00h
;_CpyO2ToFPS1		equ 4459h
       DW 0000h
       DB 00h
;_CpyO1ToFPS1		equ 445Ch
       DW 0000h
       DB 00h
;_CpyToFPS1		equ 445Fh
       DW 0000h
       DB 00h
;_CpyO2ToFPS2		equ 4462h
       DW 0000h
       DB 00h
;_CpyO3ToFPS2		equ 4465h
       DW 0000h
       DB 00h
;_CpyO6ToFPS2		equ 4468h
       DW 0000h
       DB 00h
;_CpyO1ToFPS2		equ 446Bh
       DW 0000h
       DB 00h
;_CpyToFPS2		equ 446Eh
       DW 0000h
       DB 00h
;_CpyO5ToFPS3		equ 4471h
       DW 0000h
       DB 00h
;_CpyO2ToFPS3		equ 4474h
       DW 0000h
       DB 00h
;_CpyO1ToFPS3		equ 4477h
       DW 0000h
       DB 00h
;_CpyToFPS3		equ 447Ah
       DW 0000h
       DB 00h
;_CpyO1ToFPS6		equ 447Dh
       DW 0000h
       DB 00h
;_CpyO1ToFPS7		equ 4480h
       DW 0000h
       DB 00h
;_CpyO1ToFPS5		equ 4483h
       DW 0000h
       DB 00h
;_CpyO2ToFPS4		equ 4486h
       DW 0000h
       DB 00h
;_CpyO1ToFPS4		equ 4489h
       DW 0000h
       DB 00h
;_ErrNotEnoughMem 	equ 448Ch ;only if not HL bytes free
       DW 0000h
       DB 00h
;_FPSMINUS9		equ 448Fh
       DW 0000h
       DB 00h
;_HLMINUS9		equ 4492h
       DW 0000h
       DB 00h
;_ErrOverflow		equ 4495h
       DW 0000h
       DB 00h
;_ErrDivBy0		equ 4498h
       DW 0000h
       DB 00h
;_ErrSingularMat		equ 449Bh
       DW 0000h
       DB 00h
;_ErrDomain		equ 449Eh
       DW 0000h
       DB 00h
;_ErrIncrement		equ 44A1h
       DW 0000h
       DB 00h
;_ErrNon_Real		equ 44A4h
       DW 0000h
       DB 00h
;_ErrSyntax		equ 44A7h
       DW 0000h
       DB 00h
;_ErrDataType		equ 44AAh
       DW 0000h
       DB 00h
;_ErrArgument		equ 44ADh
       DW 0000h
       DB 00h
;_ErrDimMismatch		equ 44B0h
       DW 0000h
       DB 00h
;_ErrDimension		equ 44B3h
       DW 0000h
       DB 00h
;_ErrUndefined		equ 44B6h
       DW 0000h
       DB 00h
;_ErrMemory		equ 44B9h
       DW 0000h
       DB 00h
;_ErrInvalid		equ 44BCh
       DW 0000h
       DB 00h
;_ErrBreak		equ 44BFh
       DW 0000h
       DB 00h
;_ErrStat		equ 44C2h
       DW 0000h
       DB 00h
;_ErrSignChange		equ 44C5h
       DW 0000h
       DB 00h
;_ErrIterations		equ 44C8h
       DW 0000h
       DB 00h
;_ErrBadGuess		equ 44CBh
       DW 0000h
       DB 00h
;_ErrTolTooSmall		equ 44CEh
       DW 0000h
       DB 00h
;_ErrStatPlot		equ 44D1h
       DW 0000h
       DB 00h
;_ErrLinkXmit		equ 44D4h
       DW 0000h
       DB 00h
;_JError			equ 44D7h
       DW 0000h
       DB 00h
;_noErrorEntry		equ 44DAh
       DW 0000h
       DB 00h
;_pushErrorHandleR	equ 44DDh
       DW 0000h
       DB 00h
;_popErrorHandleR	equ 44E0h
       DW 0000h
       DB 00h
;_strcopy		equ 44E3h
       DW 0000h
       DB 00h
;_strCat			equ 44E6h
       DW 0000h
       DB 00h
;_isInSet		equ 44E9h
       DW 0000h
       DB 00h
;_sDone			equ 44ECh ;this should actually be called _SetEquToOP1
       DW 0000h
       DB 00h
;_serrort		equ 44EFh
       DW 0000h
       DB 00h
;_sNameEq		equ 44F2h
       DW 0000h
       DB 00h
;_sUnderScr		equ 44F5h
       DW 0000h
       DB 00h
;_sFAIL			equ 44F8h
       DW 0000h
       DB 00h
;_sName			equ 44FBh
       DW 0000h
       DB 00h
;_sOK			equ 44FEh
       DW 0000h
       DB 00h
;_PutMap			equ 4501h
       DW 0000h
       DB 00h
;_PutC			equ 4504h
       DW 0000h
       DB 00h
;_DispHL			equ 4507h
       DW 0000h
       DB 00h
;_PutS			equ 450Ah
_PutS:
       DW PutS
       DB 00h
;_putpsb			equ 450Dh
       DW 0000h
       DB 00h
;_PutPS			equ 4510h
       DW 0000h
       DB 00h
;_wputps			equ 4513h
       DW 0000h
       DB 00h
;_putbuf			equ 4516h
       DW 0000h
       DB 00h
;_putbuf1		equ 4519h
       DW 0000h
       DB 00h
;_wputc			equ 451Ch
       DW 0000h
       DB 00h
;_wputs			equ 451Fh
       DW 0000h
       DB 00h
;_wputsEOL		equ 4522h ;displays string in HL in big font, and uses ... if too long
       DW 0000h
       DB 00h
;_wdispEOL		equ 4525h
       DW 0000h
       DB 00h
;_whomeup		equ 4528h
       DW 0000h
       DB 00h
;_setNumWindow		equ 452Bh ;based on current cursor position, sets winleft and similar (for input prompts)
       DW 0000h
       DB 00h
;_newline		equ 452Eh
       DW 0000h
       DB 00h
;_moveDown		equ 4531h
       DW 0000h
       DB 00h
;_scrollUp		equ 4534h
       DW 0000h
       DB 00h
;_shrinkWindow		equ 4537h
       DW 0000h
       DB 00h
;_moveUp			equ 453Ah
       DW 0000h
       DB 00h
;_scrollDown		equ 453Dh
       DW 0000h
       DB 00h
_ClrLCDFull:
       DW ClrLCDFull
       DB 00h
;_ClrLCD			equ 4543h
       DW 0000h
       DB 00h
_ClrScrnFull:
       DW ClrScrnFull
       DB 00h
;_ClrScrn		equ 4549h
       DW 0000h
       DB 00h
_ClrTxtShd:
       DW ClrTxtShd
       DB 00h
;_ClrWindow		equ 454Fh
       DW 0000h
       DB 00h
;_EraseEOL		equ 4552h
       DW 0000h
       DB 00h
;_EraseEOW		equ 4555h
       DW 0000h
       DB 00h
;_HomeUp			equ 4558h
       DW 0000h
       DB 00h
;_getcurloc		equ 455Bh
       DW 0000h
       DB 00h
;_VPutMap		equ 455Eh
       DW 0000h
       DB 00h
;_VPutS			equ 4561h
       DW 0000h
       DB 00h
;_VPutSN			equ 4564h
       DW 0000h
       DB 00h
;_vputsnG		equ 4567h
       DW 0000h
       DB 00h
;_vputsnT		equ 456Ah
       DW 0000h
       DB 00h
;_RunIndicOn		equ 456Dh
       DW 0000h
       DB 00h
;_RunIndicOff		equ 4570h
       DW 0000h
       DB 00h
;_saveCmdShadow		equ 4573h
       DW 0000h
       DB 00h
;_saveShadow		equ 4576h
       DW 0000h
       DB 00h
;_rstrShadow		equ 4579h
       DW 0000h
       DB 00h
;_rstrpartial		equ 457Ch
       DW 0000h
       DB 00h
;_rstrCurRow		equ 457Fh
       DW 0000h
       DB 00h
;_rstrUnderMenu		equ 4582h
       DW 0000h
       DB 00h
;_rstrbotrow		equ 4585h
       DW 0000h
       DB 00h
;_saveTR			equ 4588h ;save top right corner of LCD so 2nd arrow can be displayed, indicinuse flag must be set
       DW 0000h
       DB 00h
;_restoreTR		equ 458Bh ;restore top right corner of LCD destroyed by an arrow. indicinuse flag must be set
       DW 0000h
       DB 00h
;_GetKeyPress		equ 458Eh
       DW 0000h
       DB 00h
;_GetTokLen		equ 4591h ;input: hl=pointer to token. output: a=lenght of string, hl=pointer to string on page 1
       DW 0000h
       DB 00h
;_GET_TOK_STRNG		equ 4594h ;input: hl=pointer to token. output: op3=string of the token, a=length of string
       DW 0000h
       DB 00h
;_GETTOKSTRING		equ 4597h ;input: DE=token. output: hl=pointer to the string on page 1
       DW 0000h
       DB 00h
;_PUTBPATBUF2		equ 459Ah
       DW 0000h
       DB 00h
;_PUTBPATBUF		equ 459Dh
       DW 0000h
       DB 00h
;_putbPAT		equ 45A0h
       DW 0000h
       DB 00h
;_putcCheckScrolL	equ 45A3h
       DW 0000h
       DB 00h
;_DispEOL		equ 45A6h
       DW 0000h
       DB 00h
;_fdispEOL		equ 45A9h
       DW 0000h
       DB 00h
;_MAKEROWCMD		equ 45ACh
       DW 0000h
       DB 00h
;_TOTOSTRP		equ 45AFh
       DW 0000h
       DB 00h
;_SETVARNAME		equ 45B2h
       DW 0000h
       DB 00h
;_DispDone		equ 45B5h
       DW 0000h
       DB 00h
;_finishoutput		equ 45B8h
       DW 0000h
       DB 00h
;_curBlink		equ 45BBh
       DW 0000h
       DB 00h
_CursorOff:
       DW CursorOff
       DB 00h
_hideCursor:
       DW hideCursor
       DB 00h
_CursorOn:
       DW CursorOn
       DB 00h
_showCursor:
       DW showCursor
       DB 00h
_KeyToString:
       DW KeyToString
       DB 01h
;_PULLDOWNCHK		equ 45CDh ;something wrong here
       DW 0000h
       DB 00h
;_MenuCatCommon		equ 45D0h
       DW 0000h
       DB 00h
;_ZIfCatalog		equ 45D3h
       DW 0000h
       DB 00h
;_ZIfMatrixMenu		equ 45D6h ;_loadCurCat
       DW 0000h
       DB 00h
;_LoadMenuNum		equ 45D9h
       DW 0000h
       DB 00h
;_LoadMenuNumL		equ 45DCh
       DW 0000h
       DB 00h
;_MenCatRet		equ 45DFh ;restores display as though a menu were just cleared (restores some flags too)
       DW 0000h
       DB 00h
;_MenuSwitchContext	equ 45E2h ;switches to context in A, calls menu hook with A=3, set 5,(iy+16h) for some sort of override to not make switch
       DW 0000h
       DB 00h
;_MenuEdKey		equ 45E5h
       DW 0000h
       DB 00h
;_BackUpGraphSettings	equ 45E8h
       DW 0000h
       DB 00h
;_notalphnum		equ 45EBh
       DW 0000h
       DB 00h
;_SaveSavedFlags		equ 45EEh
       DW 0000h
       DB 00h
;_SetMenuFlags		equ 45F1h
       DW 0000h
       DB 00h
;_RstrSomeFlags		equ 45F4h
       DW 0000h
       DB 00h
;_RstrOScreen		equ 45F7h ;restores saveSScreen to the display
       DW 0000h
       DB 00h
;_SaveOScreen		equ 45FAh ;stores display in saveSScreen
       DW 0000h
       DB 00h
;_dispListName		equ 45FDh ;_SeeIfErrorCx
       DW 0000h
       DB 00h
;_PrevContext		equ 4600h
       DW 0000h
       DB 00h
;_CompareContext		equ 4603h
       DW 0000h
       DB 00h
;_AdrMRow		equ 4606h
       DW 0000h
       DB 00h
;_AdrMEle		equ 4609h
       DW 0000h
       DB 00h
;_GETMATOP1A		equ 460Ch
       DW 0000h
       DB 00h
;_GETM1TOOP1		equ 460Fh
       DW 0000h
       DB 00h
;_GETM1TOP1A		equ 4612h
       DW 0000h
       DB 00h
;_GetMToOP1		equ 4615h
       DW 0000h
       DB 00h
;_PUTTOM1A		equ 4618h
       DW 0000h
       DB 00h
;_PUTTOMA1		equ 461Bh
       DW 0000h
       DB 00h
;_PutToMat		equ 461Eh
       DW 0000h
       DB 00h
;_MAT_EL_DIV		equ 4621h
       DW 0000h
       DB 00h
;_CMATFUN		equ 4624h
       DW 0000h
       DB 00h
;_ROWECH_POLY		equ 4627h
       DW 0000h
       DB 00h
;_ROWECHELON		equ 462Ah
       DW 0000h
       DB 00h
;_AdrLEle		equ 462Dh
       DW 0000h
       DB 00h
;_GETL1TOOP1		equ 4630h
       DW 0000h
       DB 00h
;_GETL1TOP1A		equ 4633h
       DW 0000h
       DB 00h
;_GetLToOP1		equ 4636h
       DW 0000h
       DB 00h
;_GETL1TOOP2		equ 4639h
       DW 0000h
       DB 00h
;_GETL1TOP2A		equ 463Ch
       DW 0000h
       DB 00h
;_GETL2TOP1A		equ 463Fh
       DW 0000h
       DB 00h
;_PUTTOLA1		equ 4642h
       DW 0000h
       DB 00h
;_PutToL			equ 4645h
       DW 0000h
       DB 00h
;_MAXMINLST		equ 4648h
       DW 0000h
       DB 00h
;_LLOW			equ 464Bh
       DW 0000h
       DB 00h
;_LHIGH			equ 464Eh
       DW 0000h
       DB 00h
;_LSUM			equ 4651h
       DW 0000h
       DB 00h
;CUMSUM			equ 4654h
       DW 0000h
       DB 00h
;_ToFrac			equ 4657h
       DW 0000h
       DB 00h
;_SEQSET			equ 465Ah
       DW 0000h
       DB 00h
;_SEQSOLVE		equ 465Dh
       DW 0000h
       DB 00h
;_CMP_NUM_INIT		equ 4660h
       DW 0000h
       DB 00h
;_BinOPExec		equ 4663h
       DW 0000h
       DB 00h
;_EXMEAN1		equ 4666h
       DW 0000h
       DB 00h
;_SET2MVLPTRS		equ 4669h
       DW 0000h
       DB 00h
;_SETMAT1		equ 466Ch
       DW 0000h
       DB 00h
;_CREATETLIST		equ 466Fh
       DW 0000h
       DB 00h
;_UnOPExec		equ 4672h
       DW 0000h
       DB 00h
;_ThreeExec		equ 4675h
       DW 0000h
       DB 00h
;_RESTOREERRNO		equ 4678h
       DW 0000h
       DB 00h
;_FourExec		equ 467Bh
       DW 0000h
       DB 00h
;_FiveExec		equ 467Eh
       DW 0000h
       DB 00h
;_CPYTO2ES1		equ 4681h
       DW 0000h
       DB 00h
;_CPYTO6ES1		equ 4684h
       DW 0000h
       DB 00h
;_CPYTO1ES1		equ 4687h
       DW 0000h
       DB 00h
;_CPYTO3ES1		equ 468Ah
       DW 0000h
       DB 00h
;_CPYTO3ES2		equ 468Dh
       DW 0000h
       DB 00h
;_CPYTO2ES2		equ 4690h
       DW 0000h
       DB 00h
;_CPYTO1ES2		equ 4693h
       DW 0000h
       DB 00h
;_CPYTO2ES3		equ 4696h
       DW 0000h
       DB 00h
;_CPYTO1ES3		equ 4699h
       DW 0000h
       DB 00h
;_CPYTO3ES4		equ 469Ch
       DW 0000h
       DB 00h
;_CPYTO6ES3		equ 469Fh
       DW 0000h
       DB 00h
;_CPYTO2ES4		equ 46A2h
       DW 0000h
       DB 00h
;_CPYTO1ES4		equ 46A5h
       DW 0000h
       DB 00h
;_CPYTO2ES5		equ 46A8h
       DW 0000h
       DB 00h
;_CPYTO1ES5		equ 46ABh
       DW 0000h
       DB 00h
;_CPYTO4EST		equ 46AEh
       DW 0000h
       DB 00h
;_CPYTO2EST		equ 46B1h
       DW 0000h
       DB 00h
;_CPYTO1EST		equ 46B4h
       DW 0000h
       DB 00h
;_CPYTO2ES6		equ 46B7h
       DW 0000h
       DB 00h
;_CPYTO1ES6		equ 46BAh
       DW 0000h
       DB 00h
;_CPYTO2ES7		equ 46BDh
       DW 0000h
       DB 00h
;_CPYTO1ES7		equ 46C0h
       DW 0000h
       DB 00h
;_CPYTO2ES8		equ 46C3h
       DW 0000h
       DB 00h
;_CPYTO1ES8		equ 46C6h
       DW 0000h
       DB 00h
;_CPYTO1ES9		equ 46C9h
       DW 0000h
       DB 00h
;_CPYTO2ES9		equ 46CCh
       DW 0000h
       DB 00h
;_CPYTO2ES10		equ 46CFh
       DW 0000h
       DB 00h
;_CPYTO1ES10		equ 46D2h
       DW 0000h
       DB 00h
;_CPYTO2ES11		equ 46D5h
       DW 0000h
       DB 00h
;_CPYTO1ES11		equ 46D8h
       DW 0000h
       DB 00h
;_CPYTO2ES12		equ 46DBh
       DW 0000h
       DB 00h
;_CPYTO1ES12		equ 46DEh
       DW 0000h
       DB 00h
;_CPYTO2ES13		equ 46E1h
       DW 0000h
       DB 00h
;_CPYTO1ES13		equ 46E4h
       DW 0000h
       DB 00h
;_CPYTO1ES14		equ 46E7h
       DW 0000h
       DB 00h
;_CPYTO1ES16		equ 46EAh
       DW 0000h
       DB 00h
;_CPYTO1ES17		equ 46EDh
       DW 0000h
       DB 00h
;_CPYTO1ES18		equ 46F0h
       DW 0000h
       DB 00h
;_CPYTO1ES15		equ 46F3h
       DW 0000h
       DB 00h
;_CPYTO2ES15		equ 46F6h
       DW 0000h
       DB 00h
;_CPYO1TOEST		equ 46F9h
       DW 0000h
       DB 00h
;_CPYO1TOES1		equ 46FCh
       DW 0000h
       DB 00h
;_CPYO6TOES1		equ 46FFh
       DW 0000h
       DB 00h
;_CPYO6TOES3		equ 4702h
       DW 0000h
       DB 00h
;_CPYO1TOES2		equ 4705h
       DW 0000h
       DB 00h
;_CPYO2TOES2		equ 4708h
       DW 0000h
       DB 00h
;_CPYO1TOES3		equ 470Bh
       DW 0000h
       DB 00h
;_CPYO1TOES4		equ 470Eh
       DW 0000h
       DB 00h
;_CPYO1TOES5		equ 4711h
       DW 0000h
       DB 00h
;_CPYO1TOES6		equ 4714h
       DW 0000h
       DB 00h
;_CPYO1TOES7		equ 4717h
       DW 0000h
       DB 00h
;_CPYO2TOES4		equ 471Ah
       DW 0000h
       DB 00h
;_CPYO2TOES5		equ 471Dh
       DW 0000h
       DB 00h
;_CPYO2TOES6		equ 4720h
       DW 0000h
       DB 00h
;_CPYO2TOES7		equ 4723h
       DW 0000h
       DB 00h
;_CPYO2TOES8		equ 4726h
       DW 0000h
       DB 00h
;_CPYO2TOES9		equ 4729h
       DW 0000h
       DB 00h
;_CPYO1TOES8		equ 472Ch
       DW 0000h
       DB 00h
;_CPYO1TOES9		equ 472Fh
       DW 0000h
       DB 00h
;_CPYO1TOES10		equ 4732h
       DW 0000h
       DB 00h
;_CPYO1TOES11		equ 4735h
       DW 0000h
       DB 00h
;_CPYO1TOES12		equ 4738h
       DW 0000h
       DB 00h
;_CPYO1TOES13		equ 473Bh
       DW 0000h
       DB 00h
;_CPYO1TOES14		equ 473Eh
       DW 0000h
       DB 00h
;_CPYO1TOES15		equ 4741h
       DW 0000h
       DB 00h
;_EVALF3A		equ 4744h
       DW 0000h
       DB 00h
;_GetK			equ 4747h ;?
       DW 0000h
       DB 00h
;_setTitle               equ 474Ah
       DW 0000h
       DB 00h
;_dispVarVal		equ 474Dh
       DW 0000h
       DB 00h
;_RecallEd		equ 4750h ;_setupBuffer
       DW 0000h
       DB 00h
;_createNumEditBuf	equ 4753h
       DW 0000h
       DB 00h
;_ProcessBufKeys		equ 4756h ;may be default key processing like [CLEAR], etc. especially for an edit buffer.
       DW 0000h
       DB 00h
;_CallCommon		equ 4759h
       DW 0000h
       DB 00h
;_CommonKeys		equ 475Ch
       DW 0000h
       DB 00h
;_Leftmore		equ 475Fh
       DW 0000h
       DB 00h
;_fDel			equ 4762h
       DW 0000h
       DB 00h
;_fClear			equ 4765h
       DW 0000h
       DB 00h
;_finsDisp		equ 4768h ;Michael says _FinsDisp02 equ 4768h (something's not right)
       DW 0000h
       DB 00h
;_FinsDisp02		equ 476Bh ;_setIndicator
       DW 0000h
       DB 00h
;_closeeditbufnor	equ 476Eh
       DW 0000h
       DB 00h
;_releaseBuffer		equ 4771h
       DW 0000h
       DB 00h
;_varnameToOP1hl		equ 4774h
       DW 0000h
       DB 00h
;_nameToOP1		equ 4777h
       DW 0000h
       DB 00h
;_numPPutAway		equ 477Ah
       DW 0000h
       DB 00h
;_numRedisp		equ 477Dh
       DW 0000h
       DB 00h
;_numError02		equ 4780h
       DW 0000h
       DB 00h
;_Load_SFont		equ 4783h
       DW 0000h
       DB 00h
;_SFont_Len		equ 4786h
       DW 0000h
       DB 00h
;_InitNumVec		equ 4789h ;inits window settings/table setup/finance solver context (dialog-like)
       DW 0000h
       DB 00h
;_SetXXOP1		equ 478Ch
       DW 0000h
       DB 00h
;_SetXXOP2		equ 478Fh
       DW 0000h
       DB 00h
;_SetXXXXOP2		equ 4792h
       DW 0000h
       DB 00h
;_UCLineS		equ 4795h
       DW 0000h
       DB 00h
;_CLine			equ 4798h
       DW 0000h
       DB 00h
;_CLineS			equ 479Bh
       DW 0000h
       DB 00h
;_XRootY			equ 479Eh
       DW 0000h
       DB 00h
;_YToX			equ 47A1h
       DW 0000h
       DB 00h
;_ZmStats		equ 47A4h
       DW 0000h
       DB 00h
;_POINT_STAT_HLP		equ 47A7h
       DW 0000h
       DB 00h
;_DRAWSPLOT		equ 47AAh
       DW 0000h
       DB 00h
;_INITNEWTRACEP		equ 47ADh ;A is input here, goes to (8E63h)
       DW 0000h
       DB 00h
;_SPLOTCOORD		equ 47B0h
       DW 0000h
       DB 00h
;_SPLOTRIGHT		equ 47B3h
       DW 0000h
       DB 00h
;_SPLOTLEFT		equ 47B6h
       DW 0000h
       DB 00h
;_CMPBOXINFO		equ 47B9h
       DW 0000h
       DB 00h
;_NEXTPLOT		equ 47BCh
       DW 0000h
       DB 00h
;_PREVPLOT		equ 47BFh
       DW 0000h
       DB 00h
;_CLRPREVPLOT		equ 47C2h
       DW 0000h
       DB 00h
;_PUT_INDEX_LST		equ 47C5h
       DW 0000h
       DB 00h
;_GET_INDEX_LST		equ 47C8h
       DW 0000h
       DB 00h
;_HEAP_SORT		equ 47CBh
       DW 0000h
       DB 00h
;_StoGDB2		equ 47CEh
       DW 0000h
       DB 00h
;_RclGDB2		equ 47D1h
       DW 0000h
       DB 00h
;_CircCmd		equ 47D4h
       DW 0000h
       DB 00h
;_GrphCirc		equ 47D7h
       DW 0000h
       DB 00h
;_Mov18B			equ 47DAh
       DW 0000h
       DB 00h
;_DarkLine		equ 47DDh
       DW 0000h
       DB 00h
;_ILine			equ 47E0h
       DW 0000h
       DB 00h
;_IPoint			equ 47E3h
       DW 0000h
       DB 00h
;_XYRNDBOTH		equ 47E6h
       DW 0000h
       DB 00h
;_XYRND			equ 47E9h
       DW 0000h
       DB 00h
;_CheckTOP		equ 47ECh
       DW 0000h
       DB 00h
;_CheckXY		equ 47EFh
       DW 0000h
       DB 00h
;_DarkPnt		equ 47F2h
       DW 0000h
       DB 00h
;_CPointS		equ 47F5h
       DW 0000h
       DB 00h
;_WTOV			equ 47F8h
       DW 0000h
       DB 00h
;_VtoWHLDE		equ 47FBh
       DW 0000h
       DB 00h
;_Xitof			equ 47FEh
       DW 0000h
       DB 00h
;_YftoI			equ 4801h
       DW 0000h
       DB 00h
;_XftoI			equ 4804h
       DW 0000h
       DB 00h
;_TraceOff		equ 4807h
       DW 0000h
       DB 00h
;_GrRedisp		equ 480Ah
       DW 0000h
       DB 00h
;_GDISPTOKEN		equ 480Dh
       DW 0000h
       DB 00h
;_GRDECODA		equ 4810h
       DW 0000h
       DB 00h
;_LABCOOR		equ 4813h ;draws labels with _GRLABELS and X/Y/whatever coordinates, including stat plot stuff
       DW 0000h
       DB 00h
;_COORDISP		equ 4816h ;draws X & Y coordinates (or R and theta if PolarGC)
       DW 0000h
       DB 00h
;_TMPEQUNOSRC		equ 4819h
       DW 0000h
       DB 00h
;_GRLABELS		equ 481Ch
       DW 0000h
       DB 00h
;_YPIXSET		equ 481Fh
       DW 0000h
       DB 00h
;_XPIXSET		equ 4822h
       DW 0000h
       DB 00h
;_COPYRNG		equ 4825h
       DW 0000h
       DB 00h
;_VALCUR			equ 4828h ;just sets/resets three flags, enables graph cursor
       DW 0000h
       DB 00h
;_GRPUTAWAY		equ 482Bh
       DW 0000h
       DB 00h
;_RSTGFLAGS		equ 482Eh
       DW 0000h
       DB 00h
;_GRReset		equ 4831h
       DW 0000h
       DB 00h
;_XYCENT			equ 4834h
       DW 0000h
       DB 00h
;_ZOOMXYCMD		equ 4837h
       DW 0000h
       DB 00h
;_CPTDELY		equ 483Ah
       DW 0000h
       DB 00h
;_CPTDELX		equ 483Dh
       DW 0000h
       DB 00h
;_SetFuncM		equ 4840h
       DW 0000h
       DB 00h
;_SetSeqM		equ 4843h
       DW 0000h
       DB 00h
;_SetPolM		equ 4846h
       DW 0000h
       DB 00h
;_SetParM		equ 4849h
       DW 0000h
       DB 00h
;_ZmInt			equ 484Ch
       DW 0000h
       DB 00h
;_ZmDecml		equ 484Fh
       DW 0000h
       DB 00h
;_ZmPrev			equ 4852h
       DW 0000h
       DB 00h
;_ZmUsr			equ 4855h
       DW 0000h
       DB 00h
;_SETUZM			equ 4858h
       DW 0000h
       DB 00h
;_ZmFit			equ 485Bh
       DW 0000h
       DB 00h
;_ZmSquare		equ 485Eh
       DW 0000h
       DB 00h
;_ZmTrig			equ 4861h
       DW 0000h
       DB 00h
;_SetXMinMax		equ 4864h
       DW 0000h
       DB 00h
;_ZooDefault		equ 4867h
       DW 0000h
       DB 00h
;_GrBufCpy		equ 486Ah
       DW 0000h
       DB 00h
;_DRAWSPLITLINE		equ 486Dh
       DW 0000h
       DB 00h
;_RestoreDisp		equ 4870h
       DW 0000h
       DB 00h
;_FNDDB			equ 4873h
       DW 0000h
       DB 00h
;_AllEq			equ 4876h
       DW 0000h
       DB 00h
;_fndallseleq		equ 4879h
       DW 0000h
       DB 00h
;_NEXTEQ			equ 487Ch
       DW 0000h
       DB 00h
;_PREVEQ			equ 487Fh
       DW 0000h
       DB 00h
;_BLINKGCUR		equ 4882h
       DW 0000h
       DB 00h
;_NBCURSOR		equ 4885h
       DW 0000h
       DB 00h
;_STATMARK		equ 4888h
       DW 0000h
       DB 00h
;_CHKTEXTCURS		equ 488Bh
       DW 0000h
       DB 00h
;_Regraph		equ 488Eh
       DW 0000h
       DB 00h
;_DOREFFLAGS02		equ 4891h ;something wrong here
       DW 0000h
       DB 00h
;INITNSEQ		equ 4894h
       DW 0000h
       DB 00h
;_YRES			equ 4897h ;_PLOTPTXY2
       DW 0000h
       DB 00h
;_Ceiling		equ 489Ah ;ceil(OP1)
       DW 0000h
       DB 00h
;_PutXY			equ 489Dh ;draws X & Y coordinates (regardless of PolarGC)
       DW 0000h
       DB 00h
;_PUTEQUNO		equ 48A0h
       DW 0000h
       DB 00h
;_PDspGrph		equ 48A3h
       DW 0000h
       DB 00h
;_HorizCmd		equ 48A6h
       DW 0000h
       DB 00h
;_VertCmd		equ 48A9h
       DW 0000h
       DB 00h
;_LineCmd		equ 48ACh
       DW 0000h
       DB 00h
;_UnLineCmd		equ 48AFh
       DW 0000h
       DB 00h
;_PointCmd		equ 48B2h
       DW 0000h
       DB 00h
;_PixelTest		equ 48B5h
       DW 0000h
       DB 00h
;_PixelCmd  		equ 48B8h
       DW 0000h
       DB 00h
;_TanLnF			equ 48BBh
       DW 0000h
       DB 00h
;_DRAWCMD_INIT		equ 48BEh
       DW 0000h
       DB 00h
;_DrawCmd		equ 48C1h
       DW 0000h
       DB 00h
;_SHADECMD		equ 48C4h
       DW 0000h
       DB 00h
;_InvCmd			equ 48C7h
       DW 0000h
       DB 00h
;_STATSHADE		equ 48CAh
       DW 0000h
       DB 00h
;_dspmattable		equ 48CDh
       DW 0000h
       DB 00h
;_dsplsts		equ 48D0h
       DW 0000h
       DB 00h
;_closeEditBuf		equ 48D3h
       DW 0000h
       DB 00h
;_parseEditBuf		equ 48D6h
       DW 0000h
       DB 00h
;_putsm			equ 48D9h
       DW 0000h
       DB 00h
;_DspCurTbl		equ 48DCh
       DW 0000h
       DB 00h
;_DSPGRTBL		equ 48DFh
       DW 0000h
       DB 00h
;_zeroTemplate		equ 48E2h
       DW 0000h
       DB 00h
;_settblrefs		equ 48E5h
       DW 0000h
       DB 00h
;_dispTblBot		equ 48E8h
       DW 0000h
       DB 00h
;_DispTblTop		equ 48EBh
       DW 0000h
       DB 00h
;_dispTblbody		equ 48EEh
       DW 0000h
       DB 00h
;_VPUTBLANK		equ 48F1h
       DW 0000h
       DB 00h
;_TBLTRACE		equ 48F4h
       DW 0000h
       DB 00h
;_dispListNameY		equ 48F7h
       DW 0000h
       DB 00h
;_CurNameLength		equ 48FAh
       DW 0000h
       DB 00h
;_NameToBuf		equ 48FDh
       DW 0000h
       DB 00h
;_jpromptcursor		equ 4900h
       DW 0000h
       DB 00h
;_BufLeft		equ 4903h
       DW 0000h
       DB 00h
;_BufRight		equ 4906h
       DW 0000h
       DB 00h
;_bufInsert		equ 4909h
       DW 0000h
       DB 00h
;_bufQueueChar		equ 490Ch
       DW 0000h
       DB 00h
;_BufReplace		equ 490Fh
       DW 0000h
       DB 00h
;_BufDelete		equ 4912h
       DW 0000h
       DB 00h
;_BUFPEEK		equ 4915h
       DW 0000h
       DB 00h
;_BUFPEEK1		equ 4918h
       DW 0000h
       DB 00h
;_BUFPEEK2		equ 491Bh
       DW 0000h
       DB 00h
;_BUFPEEK3		equ 491Eh
       DW 0000h
       DB 00h
;_BufToBtm		equ 4921h
       DW 0000h
       DB 00h
;_setupEditEqu		equ 4924h
       DW 0000h
       DB 00h
;_BufToTop		equ 4927h
       DW 0000h
       DB 00h
;_isEditFull		equ 492Ah
       DW 0000h
       DB 00h
;_IsEditEmpty		equ 492Dh
       DW 0000h
       DB 00h
;_IsAtTop		equ 4930h
       DW 0000h
       DB 00h
;_IsAtBtm		equ 4933h
       DW 0000h
       DB 00h
;_BufClear		equ 4936h
       DW 0000h
       DB 00h
;_JcursorFirst		equ 4939h
       DW 0000h
       DB 00h
;_JcursorLast		equ 493Ch
       DW 0000h
       DB 00h
;_CursorLeft		equ 493Fh
       DW 0000h
       DB 00h
;_cursorRight		equ 4942h
       DW 0000h
       DB 00h
;_cursorUp		equ 4945h
       DW 0000h
       DB 00h
;_CursorDown		equ 4948h
       DW 0000h
       DB 00h
;_cursorToOffset		equ 494Bh
       DW 0000h
       DB 00h
;_InsDisp		equ 494Eh
       DW 0000h
       DB 00h
;_FDISPBOL1		equ 4951h
       DW 0000h
       DB 00h
;_FDISPBOL		equ 4954h
       DW 0000h
       DB 00h
;_DispEOW		equ 4957h
       DW 0000h
       DB 00h
;_DispHead		equ 495Ah
       DW 0000h
       DB 00h
;_DispTail		equ 495Dh
       DW 0000h
       DB 00h
;_PutTokString		equ 4960h
       DW 0000h
       DB 00h
;_setupEditCmd		equ 4963h
       DW 0000h
       DB 00h
;_setEmptyEditEqu	equ 4966h
       DW 0000h
       DB 00h
;_SetEmptyEditPtr	equ 4969h
       DW 0000h
       DB 00h
;_CloseEditEqu		equ 496Ch
       DW 0000h
       DB 00h
;_GetPrevTok		equ 496Fh
       DW 0000h
       DB 00h
_GetKey:
       DW GetKey
       DB 01h
;_canIndic		equ 4975h
       DW 0000h
       DB 00h
_LCD_DRIVERON:
       DW LCD_DRIVERON
       DB 00h
;_DFMIN2			equ 497Bh
       DW 0000h
       DB 00h
;_formDisp		equ 497Eh ;this is directly what the OS calls on the homescreen to display a result
       DW 0000h
       DB 00h
;_formMatrix		equ 4981h
       DW 0000h
       DB 00h
;_wscrollLeft		equ 4984h
       DW 0000h
       DB 00h
;_wscrollUp		equ 4987h
       DW 0000h
       DB 00h
;_wscrollDown		equ 498Ah
       DW 0000h
       DB 00h
;_wscrollRight		equ 498Dh
       DW 0000h
       DB 00h
;_FormEReal		equ 4990h
       DW 0000h
       DB 00h
;_formERealTOK		equ 4993h
       DW 0000h
       DB 00h
;_FormDCplx		equ 4996h
       DW 0000h
       DB 00h
;_FormReal		equ 4999h
       DW 0000h
       DB 00h
;_formScrollUp		equ 499Ch
       DW 0000h
       DB 00h
;_setwinabove		equ 499Fh
       DW 0000h
       DB 00h
;_disarmScroll		equ 49A2h
       DW 0000h
       DB 00h
;_OP1toEdit		equ 49A5h
       DW 0000h
       DB 00h
;_MinToEdit		equ 49A8h
       DW 0000h
       DB 00h
;_rclVarToEdit		equ 49ABh
       DW 0000h
       DB 00h
;_rclVarToEditPtR	equ 49AEh
       DW 0000h
       DB 00h
;_RCLENTRYTOEDIT		equ 49B1h
       DW 0000h
       DB 00h
;_rclToQueue		equ 49B4h ;recalls bytes at OP1 into edit buffer
       DW 0000h
       DB 00h
;_FORMTOTOK		equ 49B7h
       DW 0000h
       DB 00h
;_DISP_INTERVAL		equ 49BAh
       DW 0000h
       DB 00h
;_DisplstName		equ 49BDh
       DW 0000h
       DB 00h
;_dispSLstNameHL		equ 49C0h
       DW 0000h
       DB 00h
;_EditEqu		equ 49C3h
       DW 0000h
       DB 00h
;_closeEquField		equ 49C6h
       DW 0000h
       DB 00h
;_AutoSelect		equ 49C9h
       DW 0000h
       DB 00h
;_DISPYEOS		equ 49CCh
       DW 0000h
       DB 00h
;_dispNumEOS		equ 49CFh
       DW 0000h
       DB 00h
;_setupdispeq		equ 49D2h
       DW 0000h
       DB 00h
;_DispForward		equ 49D5h
       DW 0000h
       DB 00h
;_DispYPrompt2		equ 49D8h
       DW 0000h
       DB 00h
;_stringwidth		equ 49DBh
       DW 0000h
       DB 00h
;_dispErrorScreen	equ 49DEh ;displays top row of error screen (error message)
       DW 0000h
       DB 00h
;_POPCX			equ 49E1h ;moves 14 bytes at cxPrev to cxMain, 15th byte goes to replace appflags
       DW 0000h
       DB 00h
;_loadnoeentry		equ 49E4h
       DW 0000h
       DB 00h
;_SaveScreen		equ 49E7h
       DW 0000h
       DB 00h
;_RETSCREEN		equ 49EAh
       DW 0000h
       DB 00h
;_RetScreenErr		equ 49EDh
       DW 0000h
       DB 00h
;_CheckSplitFlag		equ 49F0h
       DW 0000h
       DB 00h
;_SolveRedisp		equ 49F3h
       DW 0000h
       DB 00h
;_SolveDisp		equ 49F6h
       DW 0000h
       DB 00h
;_itemName		equ 49F9h
       DW 0000h
       DB 00h
;_SetNorm_Vals		equ 49FCh
       DW 0000h
       DB 00h
;_SetYOffset		equ 49FFh ;sets up YOffset and next 4 bytes (possibly specialized for the table editor)
       DW 0000h
       DB 00h
;_ConvKeyToTok		equ 4A02h
       DW 0000h
       DB 00h
;_ConvFCKeyToTok		equ 4A05h
       DW 0000h
       DB 00h
;_ConvFEKeyToTok		equ 4A08h
       DW 0000h
       DB 00h
;_TokToKey		equ 4A0Bh
       DW 0000h
       DB 00h
;_SendSkipExitPacket	equ 4A0Eh
       DW 0000h
       DB 00h
;_GETVARCMD		equ 4A11h
       DW 0000h
       DB 00h
;_SendVarCmd		equ 4A14h
       DW 0000h
       DB 00h
;_SendScreenshot		equ 4A17h
       DW 0000h
       DB 00h
_keyscnlnk:
       DW keyscnlnk
       DB 7Ch
;_DeselectAllVars	equ 4A1Dh
       DW 0000h
       DB 00h
;_DelRes			equ 4A20h
       DW 0000h
       DB 00h
;_ConvLcToLr		equ 4A23h
       DW 0000h
       DB 00h
;_RedimMat		equ 4A26h
       DW 0000h
       DB 00h
;_IncLstSize		equ 4A29h
       DW 0000h
       DB 00h
;_InsertList		equ 4A2Ch
       DW 0000h
       DB 00h
;_dellistel		equ 4A2Fh
       DW 0000h
       DB 00h
;_EditProg		equ 4A32h
       DW 0000h
       DB 00h
;_CloseProg		equ 4A35h
       DW 0000h
       DB 00h
;_ClrGraphRef		equ 4A38h
       DW 0000h
       DB 00h
;_FixTempCnt		equ 4A3Bh
       DW 0000h
       DB 00h
;_SAVEDATA		equ 4A3Eh
       DW 0000h
       DB 00h
;_RESTOREDATA		equ 4A41h
       DW 0000h
       DB 00h
;_FindAlphaUp		equ 4A44h
       DW 0000h
       DB 00h
;_FindAlphaDn		equ 4A47h
       DW 0000h
       DB 00h
;_CmpSyms		equ 4A4Ah
       DW 0000h
       DB 00h
;_CREATETEMP		equ 4A4Dh
       DW 0000h
       DB 00h
;_CleanAll		equ 4A50h
       DW 0000h
       DB 00h
;_MoveToNextSym		equ 4A53h ;input: hl=pointer to type byte of VAT entry. output: hl = pointer to next entry's type byte
       DW 0000h
       DB 00h
;_ConvLrToLc		equ 4A56h
       DW 0000h
       DB 00h
;_TblScreenDn		equ 4A59h ;something is not right here
       DW 0000h
       DB 00h
;_TblScreenUp		equ 4A5Ch
       DW 0000h
       DB 00h
;_SCREENUP		equ 4A5Fh
       DW 0000h
       DB 00h
;_ScreenUpDown		equ 4A62h
       DW 0000h
       DB 00h
;_ZifRclHandler		equ 4A65h
       DW 0000h
       DB 00h
;_zifrclkapp		equ 4A68h
       DW 0000h
       DB 00h
;_RCLKEY			equ 4A6Bh
       DW 0000h
       DB 00h
;_RCLREGEQ_CALL		equ 4A6Eh
       DW 0000h
       DB 00h
;_RCLREGEQ		equ 4A71h
       DW 0000h
       DB 00h
;_initNamePrompt		equ 4A74h
       DW 0000h
       DB 00h
;_NamePrompt2		equ 4A77h
       DW 0000h
       DB 00h
;_CATALOGCHK		equ 4A7Ah
       DW 0000h
       DB 00h
;_clrTR			equ 4A7Dh
       DW 0000h
       DB 00h
;_QUAD			equ 4A80h
       DW 0000h
       DB 00h
;_GRAPHQUAD		equ 4A83h
       DW 0000h
       DB 00h
;_BC2NOREAL		equ 4A86h
       DW 0000h
       DB 00h
;_ErrNonReal_FPST_FPS1	equ 4A89h
       DW 0000h
       DB 00h
;_ErrNonReal		equ 4A8Ch ;ERR:DATA TYPE if top B numers from FPS are non-real
       DW 0000h
       DB 00h
;_WRITE_TEXT		equ 4A8Fh
       DW 0000h
       DB 00h
;_FORSEQINIT		equ 4A92h
       DW 0000h
       DB 00h
;_GRPHPARS		equ 4A95h
       DW 0000h
       DB 00h
;_PLOTPARS		equ 4A98h
       DW 0000h
       DB 00h
;_ParseInp		equ 4A9Bh
       DW 0000h
       DB 00h
;_PARSEOFF		equ 4A9Eh
       DW 0000h
       DB 00h
;_PARSESCAN		equ 4AA1h
       DW 0000h
       DB 00h
;_GETPARSE		equ 4AA4h
       DW 0000h
       DB 00h
;_SAVEPARSE		equ 4AA7h
       DW 0000h
       DB 00h
;_InitPFlgs		equ 4AAAh
       DW 0000h
       DB 00h
;_CKENDLINERR		equ 4AADh
       DW 0000h
       DB 00h
;_OP2Set60		equ 4AB0h
       DW 0000h
       DB 00h
;_GETSTATPTR		equ 4AB3h
       DW 0000h
       DB 00h
;_CMP_STATPTR		equ 4AB6h
       DW 0000h
       DB 00h
;_VARSYSADR		equ 4AB9h
       DW 0000h
       DB 00h
;_StoSysTok		equ 4ABCh
       DW 0000h
       DB 00h
;_StoAns			equ 4ABFh
       DW 0000h
       DB 00h
;_StoTheta		equ 4AC2h
       DW 0000h
       DB 00h
;_StoR			equ 4AC5h
       DW 0000h
       DB 00h
;_StoY			equ 4AC8h
       DW 0000h
       DB 00h
;_StoN			equ 4ACBh
       DW 0000h
       DB 00h
;_StoT			equ 4ACEh
       DW 0000h
       DB 00h
;_StoX			equ 4AD1h
       DW 0000h
       DB 00h
;_StoOther		equ 4AD4h
       DW 0000h
       DB 00h
;_RclAns			equ 4AD7h
       DW 0000h
       DB 00h
;_RclY			equ 4ADAh
       DW 0000h
       DB 00h
;_RclN			equ 4ADDh
       DW 0000h
       DB 00h
;_RclX			equ 4AE0h
       DW 0000h
       DB 00h
;_RclVarSym		equ 4AE3h
       DW 0000h
       DB 00h
;_RclSysTok		equ 4AE6h
       DW 0000h
       DB 00h
;_StMatEl		equ 4AE9h
       DW 0000h
       DB 00h
;_STLSTVECEL		equ 4AECh
       DW 0000h
       DB 00h
;_ConvOP1		equ 4AEFh
       DW 0000h
       DB 00h
;_Find_Parse_Formula	equ 4AF2h
       DW 0000h
       DB 00h
;_PARSE_FORMULA		equ 4AF5h
       DW 0000h
       DB 00h
;_FetchQuotedString	equ 4AF8h
       DW 0000h
       DB 00h
;_FetchNumLine		equ 4AFBh
       DW 0000h
       DB 00h
;_ParseNameTokens	equ 4AFEh
       DW 0000h
       DB 00h
;_ParseInpGraph		equ 4B01h ;same as _ParseInp except 3,(iy+1Fh) is graph/split screen override, or something
       DW 0000h
       DB 00h
;_ParseInpGraphReset	equ 4B04h ;_ParseInpGraph except zeroes out iy+6/7, resets 3,(iy+1Ah) & 0,(iy+1Fh), fmtFlags->fmtOverride, parse within ParseInp?
       DW 0000h
       DB 00h
;_ParseInpLastEnt	equ 4B07h ;ParseInp on program 05h,23h,00h
       DW 0000h
       DB 00h
;_ErrOnCertainTypes	equ 4B0Ah ;ERR:DATA TYPE if A is one of a couple of values...subroutine in ParseInp, somehow
       DW 0000h
       DB 00h
;_CreatePair		equ 4B0Dh
       DW 0000h
       DB 00h
;_PUSHNUM		equ 4B10h
       DW 0000h
       DB 00h
;_INCCURPCERREND		equ 4B13h
       DW 0000h
       DB 00h
;_ERREND			equ 4B16h
       DW 0000h
       DB 00h
;_COMMAERRF		equ 4B19h
       DW 0000h
       DB 00h
;_COMMAERR		equ 4B1Ch
       DW 0000h
       DB 00h
;_STEQARG2		equ 4B1Fh
       DW 0000h
       DB 00h
;_STEQARG		equ 4B22h
       DW 0000h
       DB 00h
;_INPARG			equ 4B25h
       DW 0000h
       DB 00h
;_STEQARG3		equ 4B28h
       DW 0000h
       DB 00h
;_NXTFETCH		equ 4B2Bh
       DW 0000h
       DB 00h
;_CKFETCHVAR		equ 4B2Eh
       DW 0000h
       DB 00h
;_FETCHVARA		equ 4B31h
       DW 0000h
       DB 00h
;_FETCHVAR		equ 4B34h
       DW 0000h
       DB 00h
;_CKENDLIN		equ 4B37h ;gets parse byte in A and then _CKENDEXP
       DW 0000h
       DB 00h
;_CKENDEXP		equ 4B3Ah ;checks A for 3Eh or 3Fh
       DW 0000h
       DB 00h
;_CKPARSEND		equ 4B3Dh
       DW 0000h
       DB 00h
;_STOTYPEARG		equ 4B40h
       DW 0000h
       DB 00h
;_ConvDim		equ 4B43h
       DW 0000h
       DB 00h
;_ConvDim00		equ 4B46h
       DW 0000h
       DB 00h
;_AHEADEQUAL		equ 4B49h
       DW 0000h
       DB 00h
;_PARSAHEADS		equ 4B4Ch
       DW 0000h
       DB 00h
;_PARSAHEAD              equ 4B4Fh
       DW 0000h
       DB 00h
;_AnsName		equ 4B52h
       DW 0000h
       DB 00h
;_STOCMPREALS		equ 4B55h
       DW 0000h
       DB 00h
;_GETDEPTR		equ 4B58h
       DW 0000h
       DB 00h
;_PUSH2BOPER		equ 4B5Bh ;push the value in bc onto the operator stack
       DW 0000h
       DB 00h
;_POP2BOPER		equ 4B5Eh ;pop 2 bytes on the operator stack to bc
       DW 0000h
       DB 00h
;_PUSHOPER		equ 4B61h ;push the value in a onto the operator stack
       DW 0000h
       DB 00h
;_POPOPER		equ 4B64h ;pop 1 byte on the operator stack to a
       DW 0000h
       DB 00h
;_FIND_E_UNDEF		equ 4B67h
       DW 0000h
       DB 00h
;_STTMPEQ		equ 4B6Ah
       DW 0000h
       DB 00h
;_FINDEOL		equ 4B6Dh
       DW 0000h
       DB 00h
;_BRKINC			equ 4B70h
       DW 0000h
       DB 00h
;_INCFETCH		equ 4B73h
       DW 0000h
       DB 00h
;_CURFETCH		equ 4B76h
       DW 0000h
       DB 00h
;_Random			equ 4B79h
       DW 0000h
       DB 00h
;_StoRand		equ 4B7Ch
       DW 0000h
       DB 00h
;_RandInit		equ 4B7Fh
       DW 0000h
       DB 00h
;_resetStacks		equ 4B82h ;(onsp)->(errsp), (fpbase)->(fps), (opbase)->(ops)
       DW 0000h
       DB 00h
;_Factorial		equ 4B85h
       DW 0000h
       DB 00h
;_YONOFF			equ 4B88h
       DW 0000h
       DB 00h
;_EQSELUNSEL		equ 4B8Bh
       DW 0000h
       DB 00h
;_ITSOLVER		equ 4B8Eh
       DW 0000h
       DB 00h
;_GRITSOLVER		equ 4B91h
       DW 0000h
       DB 00h
;_ITSOLVERB		equ 4B94h
       DW 0000h
       DB 00h
;_ITSOLVERNB		equ 4B97h
       DW 0000h
       DB 00h
;_ExTest_INT		equ 4B9Ah
       DW 0000h
       DB 00h
;_DIST_FUN		equ 4BADh
       DW 0000h
       DB 00h
;_LogGamma		equ 4BA0h
       DW 0000h
       DB 00h
;_OneVar			equ 4BA3h
       DW 0000h
       DB 00h
;_ONEVARS_0		equ 4BA6h
       DW 0000h
       DB 00h
;_ORDSTAT		equ 4BA9h
       DW 0000h
       DB 00h
;_INITSTATANS2		equ 4BACh
       DW 0000h
       DB 00h
;_ANOVA_SPEC		equ 4BAFh
       DW 0000h
       DB 00h
;_OutputExpr		equ 4BB2h
       DW 0000h
       DB 00h
;_CentCursor		equ 4BB5h
       DW 0000h
       DB 00h
;_TEXT			equ 4BB8h
       DW 0000h
       DB 00h
;_FINISHSPEC		equ 4BBBh
       DW 0000h
       DB 00h
;_TRCYFUNC		equ 4BBEh
       DW 0000h
       DB 00h
;_RCL_SEQ_X		equ 4BC1h
       DW 0000h
       DB 00h
;_RCLSEQ2		equ 4BC4h
       DW 0000h
       DB 00h
;_GRPPutAway		equ 4BC7h
       DW 0000h
       DB 00h
;_CKVALDELX		equ 4BCAh
       DW 0000h
       DB 00h
;_CKVALDELTA		equ 4BCDh
       DW 0000h
       DB 00h
;_GrBufClr		equ 4BD0h
       DW 0000h
       DB 00h
;_GRBUFCPY_V		equ 4BD3h
       DW 0000h
       DB 00h
;_FNDSELEQ		equ 4BD6h
       DW 0000h
       DB 00h
;_CLRGRAPHXY		equ 4BD9h
       DW 0000h
       DB 00h
;_NEDXT_Y_STYLE		equ 4BDCh
       DW 0000h
       DB 00h
;_PLOTPT			equ 4BDFh
       DW 0000h
       DB 00h
;_NEWINDEP		equ 4BE2h
       DW 0000h
       DB 00h
;_Axes			equ 4BE5h
       DW 0000h
       DB 00h
;_setPenX		equ 4BE8h
       DW 0000h
       DB 00h
;_setPenY		equ 4BEBh
       DW 0000h
       DB 00h
;_setPenT		equ 4BEEh
       DW 0000h
       DB 00h
;_TAN_EQU_DISP		equ 4BF1h
       DW 0000h
       DB 00h
;_PutAns			equ 4BF4h
       DW 0000h
       DB 00h
;_DispOP1A		equ 4BF7h
       DW 0000h
       DB 00h
;_MATHTANLN		equ 4BFAh
       DW 0000h
       DB 00h
;_ENDDRAW		equ 4BFDh
       DW 0000h
       DB 00h
;_SetTblGraphDraw	equ 4C00h
       DW 0000h
       DB 00h
;_StartDialog		equ 4C03h
       DW 0000h
       DB 00h
;_DialogInit		equ 4C06h
       DW 0000h
       DB 00h
;_GetDialogNumOP1	equ 4C09h
       DW 0000h
       DB 00h
;_SetDialogNumOP1	equ 4C0Ch
       DW 0000h
       DB 00h
;_GetDialogNumHL		equ 4C0Fh
       DW 0000h
       DB 00h
;_ErrArgumentO123	equ 4C12h ;ERR:ARGUMENT if OP2>OP1 or OP1>OP3
       DW 0000h
       DB 00h
;_SetDialogKeyOverride	equ 4C15h
       DW 0000h
       DB 00h
;_ResDialogKeyOverride	equ 4C18h
       DW 0000h
       DB 00h
;_ForceDialogKeypress	equ 4C1Bh
       DW 0000h
       DB 00h
;_DialogStartGetKey	equ 4C1Eh
       DW 0000h
       DB 00h
;_StartDialog_Override	equ 4C21h
       DW 0000h
       DB 00h
;_CallDialogCallback	equ 4C24h
       DW 0000h
       DB 00h
;_SetDialogCallback	equ 4C27h
       DW 0000h
       DB 00h
;_ResDialogCallback	equ 4C2Ah
       DW 0000h
       DB 00h
;_CopyDialogNum		equ 4C2Dh
       DW 0000h
       DB 00h
_MemClear:
       DW MemClear
       DB 00h
;_MemSet			equ 4C33h
       DW 0000h
       DB 00h
;_ReloadAppEntryVecs	equ 4C36h
       DW 0000h
       DB 00h
;_PointOn		equ 4C39h
       DW 0000h
       DB 00h
;_ExecuteNewPrgm		equ 4C3Ch
       DW 0000h
       DB 00h
;_StrLength		equ 4C3Fh
       DW 0000h
       DB 00h
;_VPutMapRec		equ 4C42h
       DW 0000h
       DB 00h
;_getRomPage		equ 4C45h
       DW 0000h
       DB 00h
;_FindAppUp		equ 4C48h
       DW 0000h
       DB 00h
;_FindAppDn		equ 4C4Bh
       DW 0000h
       DB 00h
;_FindApp		equ 4C4Eh
       DW 0000h
       DB 00h
;_ExecuteApp		equ 4C51h
       DW 0000h
       DB 00h
;_MonReset		equ 4C54h
       DW 0000h
       DB 00h
;_ClearParseVar		equ 4C57h
       DW 0000h
       DB 00h
;_SetParseVarProg	equ 4C5Ah
       DW 0000h
       DB 00h
;_isContextKey		equ 4C5Dh
       DW 0000h
       DB 00h
;_IBounds		equ 4C60h
       DW 0000h
       DB 00h
;_IOffset		equ 4C63h
       DW 0000h
       DB 00h
;_DrawCirc2		equ 4C66h
       DW 0000h
       DB 00h
;_CanAlphIns		equ 4C69h
       DW 0000h
       DB 00h
;cxRedisp		equ 4C6Ch
       DW 0000h
       DB 00h
;_GetBaseVer		equ 4C6Fh
       DW 0000h
       DB 00h
;_OPSet0DE		equ 4C72h ;loads a floating point 0 to location de 
       DW 0000h
       DB 00h
;_AppGetCbl		equ 4C75h
       DW 0000h
       DB 00h
;_AppGetCalc		equ 4C78h
       DW 0000h
       DB 00h
;_SaveDisp		equ 4C7Bh
       DW 0000h
       DB 00h
;_SetIgnoreKey  		equ 4C7Eh ;set 1,(iy+28h) / ret
       DW 0000h
       DB 00h
;_SetSendThisKeyBack	equ 4C81h ;set 2,(iy+28h) / ld (kbdKey),a / ret
       DW 0000h
       DB 00h
;_DisableApd		equ 4C84h
       DW 0000h
       DB 00h
;_EnableApd		equ 4C87h ;set apdable,(iy+apdflags)
       DW 0000h
       DB 00h
;_JForceCmdNoChar2	equ 4C8Ah ;2.41 at least
       DW 0000h
       DB 00h
;_set2IY34		equ 4C8Dh ;set 2,(iy+34) / ret
       DW 0000h
       DB 00h
;_forcecmd		equ 4C90h
       DW 0000h
       DB 00h
;_ApdSetup		equ 4C93h
       DW 0000h
       DB 00h
;_Get_NumKey		equ 4C96h
       DW 0000h
       DB 00h
;_AppSetup		equ 4C99h ;or _AppCleanup, or something
       DW 0000h
       DB 00h
;_HandleLinkKeyActivity	equ 4C9Ch
       DW 0000h
       DB 00h
;_JForceCmdNoChar3	equ 4C9Fh ;2.41 at least
       DW 0000h
       DB 00h
;_ReleaseSedit		equ 4CA2h
       DW 0000h
       DB 00h
;_initsmalleditline	equ 4CA5h
       DW 0000h
       DB 00h
;_startsmalledit		equ 4CA8h
       DW 0000h
       DB 00h
;;4CABh
       DW 0000h
       DB 00h
;_SGetTokString		equ 4CAEh
       DW 0000h
       DB 00h
;_LoadPattern	 	equ 4CB1h
       DW 0000h
       DB 00h
;_SStringLength		equ 4CB4h
       DW 0000h
       DB 00h
;_RestorePenCol		equ 4CB7h
       DW 0000h
       DB 00h
;;4CBAh
       DW 0000h
       DB 00h
;_DoNothing		equ 4CBDh
       DW 0000h
       DB 00h
;_ForceSmallEditReturn	equ 4CC0h
       DW 0000h
       DB 00h
;;4CC3h ;saves context
       DW 0000h
       DB 00h
;;4CC6h
       DW 0000h
       DB 00h
;;4CC9h
       DW 0000h
       DB 00h
;;4CCCh
       DW 0000h
       DB 00h
;_VEraseEOL		equ 4CCFh
       DW 0000h
       DB 00h
;;4CD2h
       DW 0000h
       DB 00h
;;4CD5h
       DW 0000h
       DB 00h
;_GoToErr		equ 4CD8h
       DW 0000h
       DB 00h
;_initsmalleditBox	equ 4CDBh
       DW 0000h
       DB 00h
;;4CDEh
       DW 0000h
       DB 00h
;_EmptyHook		equ 4CE1h
       DW 0000h
       DB 00h
;_ForceSmallEditReturn2	equ 4CE4h
       DW 0000h
       DB 00h
;;4CE7h ;same as 4CC3h
       DW 0000h
       DB 00h
;;4CEAh
       DW 0000h
       DB 00h
;_ClearRow		equ 4CEDh
       DW 0000h
       DB 00h
;;4CF0h
       DW 0000h
       DB 00h
;;4CF3h
       DW 0000h
       DB 00h
;;4CF6h
       DW 0000h
       DB 00h
;;4CF9h
       DW 0000h
       DB 00h
;;4CFCh
       DW 0000h
       DB 00h
;;4CFFh
       DW 0000h
       DB 00h
;;4D02h
       DW 0000h
       DB 00h
;;4D05h
       DW 0000h
       DB 00h
;;4D08h
       DW 0000h
       DB 00h
;;4D0Bh
       DW 0000h
       DB 00h
;;4D0Eh
       DW 0000h
       DB 00h
;;4D11h
       DW 0000h
       DB 00h
;;4D14h
       DW 0000h
       DB 00h
;;4D17h
       DW 0000h
       DB 00h
;;4D1Ah
       DW 0000h
       DB 00h
;;4D1Dh
       DW 0000h
       DB 00h
;;4D20h
       DW 0000h
       DB 00h
;;4D23h
       DW 0000h
       DB 00h
;_AppScreenUpDown	equ 4D26h ;shifts screen up/down, A is LCD row, H is number of lines to shift, (OP1)-(OP1+3) are something
       DW 0000h
       DB 00h
;_AppScreenUpDown1	equ 4D29h ;shifts screen up/down, but really no clue what the inputs are (all registers and (OP1)-(OP1+3))
       DW 0000h
       DB 00h
;;4D2Ch
       DW 0000h
       DB 00h
;_initsmalleditlinevar	equ 4D2Fh
       DW 0000h
       DB 00h
;_initsmalleditlineop1	equ 4D32h
       DW 0000h
       DB 00h
;_initsmalleditboxvar	equ 4D35h
       DW 0000h
       DB 00h
;_initsmalleditboxop1	equ 4D38h
       DW 0000h
       DB 00h
;;4D3Bh
       DW 0000h
       DB 00h
;_RestartDialog		equ 4D3Eh
       DW 0000h
       DB 00h
;_ErrCustom1		equ 4D41h
       DW 0000h
       DB 00h
;_ErrCustom2		equ 4D44h
       DW 0000h
       DB 00h
;_AppStartMouse		equ 4D47h
       DW 0000h
       DB 00h
;_AppStartMouseNoSetup	equ 4D4Ah
       DW 0000h
       DB 00h
;_AppMouseGetKey		equ 4D4Dh
       DW 0000h
       DB 00h
;_AppDispMouse		equ 4D50h
       DW 0000h
       DB 00h
;_AppEraseMouse		equ 4D53h
       DW 0000h
       DB 00h
;_AppSetupMouseMem	equ 4D56h
       DW 0000h
       DB 00h
;_GetDispRowOffset	equ 4D59h ;HL=A*12 (intended for A to be row and HL becomes offset into plotSScreen)
       DW 0000h
       DB 00h
;_ClearRect		equ 4D5Ch
       DW 0000h
       DB 00h
;_InvertRect		equ 4D5Fh
       DW 0000h
       DB 00h
;_FillRect		equ 4D62h
       DW 0000h
       DB 00h
;_AppUpdateMouse		equ 4D65h
       DW 0000h
       DB 00h
;_AppDispPrevMouse	equ 4D68h ;might bring previous keypress's movement to current coordinates with flags to not display
       DW 0000h
       DB 00h
;;4D6Bh ;restores some cursor flags and stuff
       DW 0000h
       DB 00h
;_initcellbox		equ 4D6Eh
       DW 0000h
       DB 00h
;_drawcell		equ 4D71h
       DW 0000h
       DB 00h
;;4D74h
       DW 0000h
       DB 00h
;_invertcell		equ 4D77h
       DW 0000h
       DB 00h
;_setcelloverride	equ 4D7Ah
       DW 0000h
       DB 00h
;_DrawRectBorder		equ 4D7Dh
       DW 0000h
       DB 00h
;_ClearCell		equ 4D80h
       DW 0000h
       DB 00h
;_covercell		equ 4D83h
       DW 0000h
       DB 00h
;_EraseRectBorder	equ 4D86h
       DW 0000h
       DB 00h
;_FillRectPattern	equ 4D89h
       DW 0000h
       DB 00h
;_DrawRectBorderClear	equ 4D8Ch
       DW 0000h
       DB 00h
;;4D8Fh ;mouse subroutine
       DW 0000h
       DB 00h
;;4D92h
       DW 0000h
       DB 00h
;_VerticalLine		equ 4D95h
       DW 0000h
       DB 00h
;_IBoundsFull		equ 4D98h
       DW 0000h
       DB 00h
;_DisplayImage		equ 4D9Bh
       DW 0000h
       DB 00h
;:4D9Eh ;does something dumb with ports 10h/11h
       DW 0000h
       DB 00h
;;4DA1h ;mouse subroutine
       DW 0000h
       DB 00h
;_AppUpdateMouseCoords	equ 4DA4h
       DW 0000h
       DB 00h
;_ShiftBitsLeft		equ 4DA7h ;mouse subroutine, shifts B bits left from DE sprite to HL one
       DW 0000h
       DB 00h
;;4DAAh ;mouse subroutine
       DW 0000h
       DB 00h
;;4DADh ;mouse subroutine
       DW 0000h
       DB 00h
;;4DB0h ;mouse subroutine
       DW 0000h
       DB 00h
;;4DB3h ;mouse subroutine
       DW 0000h
       DB 00h
;;4DB6h ;mouse subroutine
       DW 0000h
       DB 00h
;;4DB9h ;mouse subroutine
       DW 0000h
       DB 00h
;;4DBCh ;mouse subroutine
       DW 0000h
       DB 00h
;_AppUpdateMouseRow	equ 4DBFh
       DW 0000h
       DB 00h
;_AppDrawMouse		equ 4DC2h ;set 2,(iy+2Ch) for AppEraseMouse, reset for AppDispMouse
       DW 0000h
       DB 00h
;_AppDrawMouseDirect	equ 4DC5h ;pretty much _AppDrawMouse, but you pass LCD column in A
       DW 0000h
       DB 00h
;_CPoint			equ 4DC8h
       DW 0000h
       DB 00h
;_DeleteApp		equ 4DCBh
       DW 0000h
       DB 00h
;_AppUpdateMouseXY	equ 4DCEh
       DW 0000h
       DB 00h
;_setmodecellflag	equ 4DD1h
       DW 0000h
       DB 00h
;_resetmodecellflag	equ 4DD4h
       DW 0000h
       DB 00h
;_ismodecellset		equ 4DD7h
       DW 0000h
       DB 00h
;_getmodecellflag	equ 4DDAh
       DW 0000h
       DB 00h
;;4DDDh
       DW 0000h
       DB 00h
;_CellBoxManager		equ 4DE0h
       DW 0000h
       DB 00h
;_startnewcell		equ 4DE3h
       DW 0000h
       DB 00h
;;4DE6h
       DW 0000h
       DB 00h
;_CellCursorHandle	equ 4DE9h
       DW 0000h
       DB 00h
;;4DECh
       DW 0000h
       DB 00h
;;4DEFh
       DW 0000h
       DB 00h
;_ClearCurCell		equ 4DF2h
       DW 0000h
       DB 00h
;_drawcurcell		equ 4DF5h
       DW 0000h
       DB 00h
;_invertcurcell		equ 4DF8h
       DW 0000h
       DB 00h
;_covercurcell		equ 4DFBh
       DW 0000h
       DB 00h
;_BlinkCell		equ 4DFEh
       DW 0000h
       DB 00h
;_BlinkCellNoLookUp	equ 4E01h
       DW 0000h
       DB 00h
;_BlinkCurCell		equ 4E04h
       DW 0000h
       DB 00h
;_BlinkCellToOn		equ 4E07h
       DW 0000h
       DB 00h
;_BlinkCellToOnNoLookUp	equ 4E0Ah
       DW 0000h
       DB 00h
;_BlinkCurCellToOn	equ 4E0Dh
       DW 0000h
       DB 00h
;_BlinkCellToOff		equ 4E10h
       DW 0000h
       DB 00h
;_BlinkCellToOffNoLookUp equ 4E13h
       DW 0000h
       DB 00h
;_BlinkCurCellToOff	equ 4E16h
       DW 0000h
       DB 00h
;_getcurmodecellflag	equ 4E19h
       DW 0000h
       DB 00h
;;4E1Ch
       DW 0000h
       DB 00h
;_startsmalleditreturn	equ 4E1Fh
       DW 0000h
       DB 00h
;;4E22h
       DW 0000h
       DB 00h
;;4E25h
       DW 0000h
       DB 00h
;_CellkHandle		equ 4E28h
       DW 0000h
       DB 00h
;_errchkalphabox		equ 4E2Bh
       DW 0000h
       DB 00h
;;4E2Eh
       DW 0000h
       DB 00h
;;4E31h
       DW 0000h
       DB 00h
;;4E34h
       DW 0000h
       DB 00h
;;4E37h
       DW 0000h
       DB 00h
;_eraseallcells		equ 4E3Ah
       DW 0000h
       DB 00h
;_iscurmodecellset	equ 4E3Dh
       DW 0000h
       DB 00h
;;4E40h
       DW 0000h
       DB 00h
;_initalphabox		equ 4E43h
       DW 0000h
       DB 00h
;;4E46h
       DW 0000h
       DB 00h
;;4E49h
       DW 0000h
       DB 00h
;_drawblnkcell		equ 4E4Ch
       DW 0000h
       DB 00h
;_ClearBlnkCell		equ 4E4Fh
       DW 0000h
       DB 00h
;_invertblnkcell		equ 4E52h
       DW 0000h
       DB 00h
;_AppMouseForceKey	equ 4E55h
       DW 0000h
       DB 00h
;_AppSetupMouseMemCoords	equ 4E58h ;this is _AppSetupMouseMem except you pass starting coordinates in HL
       DW 0000h
       DB 00h
;_AppMoveMouse		equ 4E5Bh ;this is _AppMouseForceKey and then updating coordinates
       DW 0000h
       DB 00h
;_GetStringInput		equ 4E5Eh
       DW 0000h
       DB 00h
;_GetStringInput2	equ 4E61h
       DW 0000h
       DB 00h
;_WaitEnterKeyValue	equ 4E64h
       DW 0000h
       DB 00h
;_HorizontalLine		equ 4E67h
       DW 0000h
       DB 00h
;_CreateAppVar		equ 4E6Ah
       DW 0000h
       DB 00h
;_CreateProtProg		equ 4E6Dh
       DW 0000h
       DB 00h
;_CreateVar		equ 4E70h
       DW 0000h
       DB 00h
;_AsmComp		equ 4E73h
       DW 0000h
       DB 00h
;_GetAsmSize		equ 4E76h
       DW 0000h
       DB 00h
;_SquishPrgm		equ 4E79h
       DW 0000h
       DB 00h
;_ExecutePrgm		equ 4E7Ch
       DW 0000h
       DB 00h
;_ChkFindSymAsm		equ 4E7Fh
       DW 0000h
       DB 00h
;_ParsePrgmName		equ 4E82h
       DW 0000h
       DB 00h
;_CSub			equ 4E85h
       DW 0000h
       DB 00h
;_CAdd			equ 4E88h
       DW 0000h
       DB 00h
;_CSqaure		equ 4E8Bh
       DW 0000h
       DB 00h
;_CMult			equ 4E8Eh
       DW 0000h
       DB 00h
;_CRecip			equ 4E91h
       DW 0000h
       DB 00h
;_CDiv			equ 4E94h
       DW 0000h
       DB 00h
;_CAbs			equ 4E97h
       DW 0000h
       DB 00h
;_AddSquares		equ 4E9Ah
       DW 0000h
       DB 00h
;_CSqRoot		equ 4E9Dh
       DW 0000h
       DB 00h
;_CLN			equ 4EA0h
       DW 0000h
       DB 00h
;_CLog			equ 4EA3h
       DW 0000h
       DB 00h
;_CTenX			equ 4EA6h
       DW 0000h
       DB 00h
;_CEtoX			equ 4EA9h
       DW 0000h
       DB 00h
;_CXrootY		equ 4EACh
       DW 0000h
       DB 00h
;;4EAFh
       DW 0000h
       DB 00h
;_CYtoX			equ 4EB2h
       DW 0000h
       DB 00h
;_InvertNonReal		equ 4EB5h
       DW 0000h
       DB 00h
;_CplxMult		equ 4EB8h
       DW 0000h
       DB 00h
;_CplxDiv		equ 4EBBh
       DW 0000h
       DB 00h
;_CplxTrunc		equ 4EBEh
       DW 0000h
       DB 00h
;_CplxFrac		equ 4EC1h
       DW 0000h
       DB 00h
;_CplxFloor		equ 4EC4h
       DW 0000h
       DB 00h
;_SendHeaderPacket	equ 4EC7h
       DW 0000h
       DB 00h
;_CancelTransmission	equ 4ECAh
       DW 0000h
       DB 00h
;_SendScreenContents	equ 4ECDh
       DW 0000h
       DB 00h
;_SendRAMVarData		equ 4ED0h
       DW 0000h
       DB 00h
;_SendRAMCmd		equ 4ED3h
       DW 0000h
       DB 00h
;_SendPacket		equ 4ED6h
       DW 0000h
       DB 00h
;_ReceiveAck		equ 4ED9h
       DW 0000h
       DB 00h
;_Send4BytePacket	equ 4EDCh
       DW 0000h
       DB 00h
;_SendDataByte		equ 4EDFh
       DW 0000h
       DB 00h
;_Send4Bytes		equ 4EE2h
       DW 0000h
       DB 00h
;_SendAByte		equ 4EE5h
       DW 0000h
       DB 00h
;_SendCByte		equ 4EE8h
       DW 0000h
       DB 00h
;_GetSmallPacket		equ 4EEBh
       DW 0000h
       DB 00h
;_GetDataPacket		equ 4EEEh
       DW 0000h
       DB 00h
;_SendAck		equ 4EF1h
       DW 0000h
       DB 00h
;_Get4Bytes		equ 4EF4h
       DW 0000h
       DB 00h
;_Get3Bytes		equ 4EF7h
       DW 0000h
       DB 00h
;_Rec1stByte		equ 4EFAh
       DW 0000h
       DB 00h
_Rec1stByteNC:
       DW Rec1stByteNC
       DB 7Ch
;_ContinueGetByte	equ 4F00h
       DW 0000h
       DB 00h
_RecAByteIO:
       DW RecAByteIO
       DB 7Ch
;_ReceiveVar		equ 4F06h
       DW 0000h
       DB 00h
;_ReceiveVarDataExists	equ 4F09h
       DW 0000h
       DB 00h
;_ReceiveVarData		equ 4F0Ch
       DW 0000h
       DB 00h
;_SrchVLstUp		equ 4F0Fh
       DW 0000h
       DB 00h
;_SrchVLstDn		equ 4F12h
       DW 0000h
       DB 00h
;_SendVariable		equ 4F15h
       DW 0000h
       DB 00h
;_Get4BytesCursor	equ 4F18h
       DW 0000h
       DB 00h
;_Get4BytesNC		equ 4F1Bh
       DW 0000h
       DB 00h
;_Convert85List		equ 4F1Eh
       DW 0000h
       DB 00h
;_SendDirectoryContents	equ 4F21h
       DW 0000h
       DB 00h
;_SendReadyPacket	equ 4F24h
       DW 0000h
       DB 00h
;_Convert85Real		equ 4F27h
       DW 0000h
       DB 00h
;_ret_6			equ 4F2Ah
       DW 0000h
       DB 00h
;_SendCertificate	equ 4F2Dh ;sends certificate in header/data packets, Flash must be unlocked, used with sending an application in LINK menu
       DW 0000h
       DB 00h
;_SendApplication	equ 4F30h
       DW 0000h
       DB 00h
;_SendOSHeader		equ 4F33h
       DW 0000h
       DB 00h
;_SendOSPage		equ 4F36h
       DW 0000h
       DB 00h
;_SendOS			equ 4F39h
       DW 0000h
       DB 00h
;_FlashWriteDisable	equ 4F3Ch
       DW 0000h
       DB 00h
;_SendCmd		equ 4F3Fh
       DW 0000h
       DB 00h
;_SendOSValidationData	equ 4F42h
       DW 0000h
       DB 00h
;_Disp			equ 4F45h
       DW 0000h
       DB 00h
;_SendGetkeyPress	equ 4F48h
       DW 0000h
       DB 00h
;_RejectCommand		equ 4F4Bh
       DW 0000h
       DB 00h
;_CheckLinkLines		equ 4F4Eh
       DW 0000h
       DB 00h
;_GetHookByte		equ 4F51h
       DW 0000h
       DB 00h
;_GetBytePaged		equ 4F54h
       DW 0000h
       DB 00h
;_cursorhook		equ 4F57h
       DW 0000h
       DB 00h
;_call_library_hook	equ 4F5Ah
       DW 0000h
       DB 00h
;_call_rawkey_hook	equ 4F5Dh
       DW 0000h
       DB 00h
;_setCursorHook		equ 4F60h ;enable cursor hook
       DW 0000h
       DB 00h
;_EnableLibraryHook	equ 4F63h
       DW 0000h
       DB 00h
;_SetGetKeyHook		equ 4F66h
       DW 0000h
       DB 00h
;_ClrCursorHook		equ 4F69h
       DW 0000h
       DB 00h
;_DisableLibraryHook	equ 4F6Ch
       DW 0000h
       DB 00h
;_ClrRawKeyHook  	equ 4F6Fh
       DW 0000h
       DB 00h
;_ResetHookBytes		equ 4F72h
       DW 0000h
       DB 00h
;_AdjustAllHooks		equ 4F75h
       DW 0000h
       DB 00h
;_getkeyhook		equ 4F78h
       DW 0000h
       DB 00h
;_SetGetcscHook		equ 4F7Bh
       DW 0000h
       DB 00h
;_ClrGetKeyHook		equ 4F7Eh
       DW 0000h
       DB 00h
;_call_linkactivity_hook	equ 4F81h
       DW 0000h
       DB 00h
;_EnableLinkActivityHook	equ 4F84h
       DW 0000h
       DB 00h
;_DisableLinkHook	equ 4F87h
       DW 0000h
       DB 00h
;_GetSmallPacket2	equ 4F8Ah
       DW 0000h
       DB 00h
;_EnableCatalog2Hook	equ 4F8Dh
       DW 0000h
       DB 00h
;_DisableCatalog2Hook	equ 4F90h
       DW 0000h
       DB 00h
;_EnableLocalizeHook	equ 4F93h
       DW 0000h
       DB 00h
;_DisableLocalizeHook	equ 4F96h
       DW 0000h
       DB 00h
;_SetTokenHook		equ 4F99h
       DW 0000h
       DB 00h
;_ClearTokenHook		equ 4F9Ch
       DW 0000h
       DB 00h
;;4F9Fh ld hl,92c6 / ld a,(92c5) / res 2,a / cp (hl) / ret
       DW 0000h
       DB 00h
;;4FA2h hl=11*(92fc)+92c9 / ld a,(hl) / and Fh / cp 2 / ret ; I can almost guarantee this is stat plot related
       DW 0000h
       DB 00h
;_DispListElementOffLA	equ 4FA5h
       DW 0000h
       DB 00h
;_Bit_VertSplit		equ 4FA8h
       DW 0000h
       DB 00h
;_SetHomescreenHook	equ 4FABh
       DW 0000h
       DB 00h
;_ClrHomeScreenHook	equ 4FAEh
       DW 0000h
       DB 00h
;_SetWindowHook		equ 4FB1h
       DW 0000h
       DB 00h
;_DisableWindowHook	equ 4FB4h
       DW 0000h
       DB 00h
;_SetGraphModeHook	equ 4FB7h
       DW 0000h
       DB 00h
;_DisableGraphHook	equ 4FBAh
       DW 0000h
       DB 00h
;_ParseAndStoreSysVar	equ 4FBDh
       DW 0000h
       DB 00h
;_DisplayEditSysVar	equ 4FC0h
       DW 0000h
       DB 00h
;_JForceWindowSettings	equ 4FC3h
       DW 0000h
       DB 00h
;_DelVarArc		equ 4FC6h
       DW 0000h
       DB 00h
;_DelVarNoArc		equ 4FC9h
       DW 0000h
       DB 00h
;_SetAllPlots		equ 4FCCh
       DW 0000h
       DB 00h
;_SetYeditHook     	equ 4FCFh
       DW 0000h
       DB 00h
;_DisableYEquHook	equ 4FD2h
       DW 0000h
       DB 00h
;_JForceYEqu		equ 4FD5h
       DW 0000h
       DB 00h
;_Arc_Unarc		equ 4FD8h ;checks for low battery
       DW 0000h
       DB 00h
;_ArchiveVar		equ 4FDBh ;set 0,(iy+24h) to check for low battery first
       DW 0000h
       DB 00h
;_UnarchiveVar		equ 4FDEh
       DW 0000h
       DB 00h
;_DialogKeyHook		equ 4FE1h ;rawkey hook used by OS for dialog context
       DW 0000h
       DB 00h
;_SetFontHook		equ 4FE4h
       DW 0000h
       DB 00h
;_ClrFontHook		equ 4FE7h
       DW 0000h
       DB 00h
;_SetRegraphHook		equ 4FEAh
       DW 0000h
       DB 00h
;_DisableRegraphHook	equ 4FEDh
       DW 0000h
       DB 00h
;_RunGraphingHook	equ 4FF0h
       DW 0000h
       DB 00h
;_SetTraceHook		equ 4FF3h
       DW 0000h
       DB 00h
;_DisableTraceHook	equ 4FF6h
       DW 0000h
       DB 00h
;_RunTraceHook		equ 4FF9h
       DW 0000h
       DB 00h
;_NDeriv			equ 4FFCh
       DW 0000h
       DB 00h
;_PolarDerivative	equ 4FFFh
       DW 0000h
       DB 00h
;_JForceGraphNoKey	equ 5002h
       DW 0000h
       DB 00h
;_JForceGraphKey		equ 5005h
       DW 0000h
       DB 00h
_PowerOff:
       DW PowerOff
       DB 00h
;_GetKeyRetOff		equ 500Bh ;same as getkey, only returns kOff if 2nd+on is pressed 
       DW 0000h
       DB 00h
;_FindGroupSym		equ 500Eh
       DW 0000h
       DB 00h
;_FillBasePageTable	equ 5011h
       DW 0000h
       DB 00h
;_ArcChk			equ 5014h
       DW 0000h
       DB 00h
;_FlashToRam		equ 5017h
       DW 0000h
       DB 00h
;_LoadDEIndPaged		equ 501Ah
       DW 0000h
       DB 00h
;_LoadCIndPaged		equ 501Dh
       DW 0000h
       DB 00h
;_SetupPagedPtr		equ 5020h
       DW 0000h
       DB 00h
;_PagedGet		equ 5023h
       DW 0000h
       DB 00h
;_SetParserHook		equ 5026h
       DW 0000h
       DB 00h
;_ClearParserHook	equ 5029h
       DW 0000h
       DB 00h
;_SetAppChangeHook 	equ 502Ch
       DW 0000h
       DB 00h
;_ClearAppChangeHook	equ 502Fh
       DW 0000h
       DB 00h
;_EnableGraphicsHook	equ 5032h
       DW 0000h
       DB 00h
;_DisableGraphicsHook	equ 5035h
       DW 0000h
       DB 00h
;_IPointNoGraphicsHook	equ 5038h
       DW 0000h
       DB 00h
;_ILineNoHook		equ 503Bh
       DW 0000h
       DB 00h
;;503Eh
       DW 0000h
       DB 00h
;_DeleteTempPrograms	equ 5041h
       DW 0000h
       DB 00h
;_EnableCatalog1Hook	equ 5044h
       DW 0000h
       DB 00h
;_DisableCatalog1Hook	equ 5047h
       DW 0000h
       DB 00h
;_EnableHelpHook		equ 504Ah
       DW 0000h
       DB 00h
;_DisableHelpHook	equ 504Dh
       DW 0000h
       DB 00h
;_DispCatalogEnd		equ 5050h
       DW 0000h
       DB 00h
;_GetMenuKeypress	equ 5053h
       DW 0000h
       DB 00h
;_GetCatalogItem		equ 5056h
       DW 0000h
       DB 00h
;_RunCatalog2Hook	equ 5059h
       DW 0000h
       DB 00h
;_RunCatalog1Hook	equ 505Ch
       DW 0000h
       DB 00h
;;505Fh
       DW 0000h
       DB 00h
;;5062h
       DW 0000h
       DB 00h
;_dispMenuTitle		equ 5065h
       DW 0000h
       DB 00h
;;5068h
       DW 0000h
       DB 00h
;_EnablecxRedispHook	equ 506Bh
       DW 0000h
       DB 00h
;_DisablecxRedispHook	equ 506Eh
       DW 0000h
       DB 00h
;_BufCpy			equ 5071h
       DW 0000h
       DB 00h
;_BufClr			equ 5074h
       DW 0000h
       DB 00h
;_UnOPExec2		equ 5077h
       DW 0000h
       DB 00h
;_BinOPExec2		equ 507Ah
       DW 0000h
       DB 00h
;_LoadMenuB		equ 507Dh ;clears screen and loads menu from B, plus a couple flag changes
       DW 0000h
       DB 00h
;_DisplayVarInfo		equ 5080h
       DW 0000h
       DB 00h
;_SetMenuHook		equ 5083h
       DW 0000h
       DB 00h
;_ClearMenuHook		equ 5086h
       DW 0000h
       DB 00h
;_getBCOffsetIX		equ 5089h
       DW 0000h
       DB 00h
;_GetBCOffsetIX2		equ 508Ch
       DW 0000h
       DB 00h
;_ForceFullScreen	equ 508Fh
       DW 0000h
       DB 00h
;_GetVariableData	equ 5092h
       DW 0000h
       DB 00h
;_FindSwapSector		equ 5095h
       DW 0000h
       DB 00h
;_CopyFlashPage		equ 5098h
       DW 0000h
       DB 00h
;_FindAppNumPages	equ 509Bh
       DW 0000h
       DB 00h
;_HLMinus5		equ 509Eh
       DW 0000h
       DB 00h
;_SendArcPacket		equ 50A1h
       DW 0000h
       DB 00h
;_ForceGraphKeypress	equ 50A4h
       DW 0000h
       DB 00h
;_DoNothing3		equ 50A7h
       DW 0000h
       DB 00h
;_FormBase		equ 50AAh
       DW 0000h
       DB 00h
;;50ADh
       DW 0000h
       DB 00h
;_IsFragmented		equ 50B0h
       DW 0000h
       DB 00h
;_Chk_Batt_Low		equ 50B3h
       DW 0000h
       DB 00h
;_Chk_Batt_Low_2		equ 50B6h
       DW 0000h
       DB 00h
;_Arc_Unarc2		equ 50B9h ;identical to _Arc_Unarc, except you can choose to res 0,(iy+24h) to skip low battery check
       DW 0000h
       DB 00h
;_GetAppBasePage		equ 50BCh ;input: a=one of an app's pages. output: a=app's first page
       DW 0000h
       DB 00h
;_SetExSpeed		equ 50BFh
       DW 0000h
       DB 00h
;_RclExit		equ 50C2h
       DW 0000h
       DB 00h
;_GroupAllVars		equ 50C5h
       DW 0000h
       DB 00h
;_UngroupVar		equ 50C8h
       DW 0000h
       DB 00h
;_WriteToFlash		equ 50CBh ;ReceiveApplication or something like that on OSes below 2.40
       DW 0000h
       DB 00h
;_SetSilentLinkHook	equ 50CEh
       DW 0000h
       DB 00h
;_DisableSilentLinkHook	equ 50D1h
       DW 0000h
       DB 00h
;_TwoVarSet		equ 50D4h
       DW 0000h
       DB 00h
;_ExecClassCToken	equ 50D7h
       DW 0000h
       DB 00h
;_ExecClass3Token	equ 50DAh
       DW 0000h
       DB 00h
;_GetSysInfo		equ 50DDh
       DW 0000h
       DB 00h
;_NZIf83Plus		equ 50E0h
       DW 0000h
       DB 00h
;_LinkStatus		equ 50E3h
       DW 0000h
       DB 00h
;_DoNothing2		equ 50E6h ;originally for TI-Navigator
       DW 0000h
       DB 00h
;_KeyboardGetKey		equ 50E9h
       DW 0000h
       DB 00h
;_RunAppLib		equ 50ECh
       DW 0000h
       DB 00h
;_FindSpecialAppHeader	equ 50EFh
       DW 0000h
       DB 00h
;_SendUSBData		equ 50F2h
       DW 0000h
       DB 00h
;_AppGetCBLUSB		equ 50F5h
       DW 0000h
       DB 00h
;_AppGetCalcUSB		equ 50F8h
       DW 0000h
       DB 00h
;_GetVarCmdUSB		equ 50FBh
       DW 0000h
       DB 00h
;;50FEh
       DW 0000h
       DB 00h
;_TenX2			equ 5101h
       DW 0000h
       DB 00h
;;5104h
       DW 0000h
       DB 00h
;;5107h
       DW 0000h
       DB 00h
;_GetVarVersion		equ 510Ah
       DW 0000h
       DB 00h
;;510Dh
       DW 0000h
       DB 00h
;;5110h
       DW 0000h
       DB 00h
;_DeleteTempEditEqu	equ 5113h
       DW 0000h
       DB 00h
;_JcursorFirst2		equ 5116h
       DW 0000h
       DB 00h
;;5119h
       DW 0000h
       DB 00h
;_PromptMoveBackLeft	equ 511Ch
       DW 0000h
       DB 00h
;_wputsEOL2		equ 511Fh ;same except res 0,(iy+0Eh) first
       DW 0000h
       DB 00h
;_InvertTextInsMode	equ 5122h
       DW 0000h
       DB 00h
;;5125h
       DW 0000h
       DB 00h
;_ResetDefaults		equ 5128h
       DW 0000h
       DB 00h
;_ZeroFinanceVars	equ 512Bh
       DW 0000h
       DB 00h
;_DispHeader		equ 512Eh
       DW 0000h
       DB 00h
;_JForceGroup		equ 5131h
       DW 0000h
       DB 00h
;;5134h
       DW 0000h
       DB 00h
;;5137h
       DW 0000h
       DB 00h
;_DispCoords		equ 513Ah
       DW 0000h
       DB 00h
;;513Dh
       DW 0000h
       DB 00h
;;5140h
       DW 0000h
       DB 00h
;_chkTmr			equ 5143h
       DW 0000h
       DB 00h
;;5146h
       DW 0000h
       DB 00h
;;5149h
       DW 0000h
       DB 00h
;;514Ch
       DW 0000h
       DB 00h
;_getDate		equ 514Fh
       DW 0000h
       DB 00h
;_GetDateString		equ 5152h
       DW 0000h
       DB 00h
;_getDtFmt		equ 5155h
       DW 0000h
       DB 00h
;_getDtStr		equ 5158h
       DW 0000h
       DB 00h
;_getTime		equ 515Bh
       DW 0000h
       DB 00h
;_GetTimeString		equ 515Eh
       DW 0000h
       DB 00h
;_getTmFmt		equ 5161h
       DW 0000h
       DB 00h
;_getTmStr		equ 5164h
       DW 0000h
       DB 00h
;_SetZeroOne		equ 5167h
       DW 0000h
       DB 00h
;_setDate		equ 516Ah
       DW 0000h
       DB 00h
;_IsOneTwoThree		equ 516Dh
       DW 0000h
       DB 00h
;_setTime		equ 5170h
       DW 0000h
       DB 00h
;_IsOP112or24		equ 5173h
       DW 0000h
       DB 00h
;_chkTimer0		equ 5176h
       DW 0000h
       DB 00h
;_timeCnv		equ 5179h
       DW 0000h
       DB 00h
;_GetLToOP1Extra		equ 517Ch
       DW 0000h
       DB 00h
;_ClrWindowAndFlags	equ 517Fh
       DW 0000h
       DB 00h
;_SetMachineID		equ 5182h
       DW 0000h
       DB 00h
;_ResetLists		equ 5185h
       DW 0000h
       DB 00h
;_DispValue		equ 5188h
       DW 0000h
       DB 00h
;;518Bh
       DW 0000h
       DB 00h
;;518Eh
       DW 0000h
       DB 00h
;_ExecLib		equ 5191h
       DW 0000h
       DB 00h
;;5194h
       DW 0000h
       DB 00h
;_CPOP1OP2Rounded	equ 5197h
       DW 0000h
       DB 00h
;_CPOP1OP2Rounded2	equ 519Ah
       DW 0000h
       DB 00h
;_OpenLib		equ 519Dh
       DW 0000h
       DB 00h
;;51A0h
       DW 0000h
       DB 00h
;;51A3h
       DW 0000h
       DB 00h
;_ResetIOPrompt		equ 51A6h
       DW 0000h
       DB 00h
;_StrCpyVarData		equ 51A9h
       DW 0000h
       DB 00h
;_SetUpEditor		equ 51ACh
       DW 0000h
       DB 00h
;_SortA			equ 51AFh
       DW 0000h
       DB 00h
;_SortD			equ 51B2h
       DW 0000h
       DB 00h
;;51B5h
       DW 0000h
       DB 00h
;_IsOP1ResID		equ 51B8h
       DW 0000h
       DB 00h
;_ListEdNameCxMain	equ 51BBh
       DW 0000h
       DB 00h
;_ListEdEnterNewName	equ 51BEh
       DW 0000h
       DB 0FDh
;;51C1h
       DW 0000h
       DB 00h
;_ForceModeKeypress	equ 51C4h ;forces a keypress (and calls help hook) on any of several mode-setting contexts
       DW 0000h
       DB 00h
;_DispAboutScreen	equ 51C7h
       DW 0000h
       DB 00h
;_ChkHelpHookVer		equ 51CAh
       DW 0000h
       DB 00h
;_Disp32			equ 51CDh
       DW 0000h
       DB 00h
;;51D0h
       DW 0000h
       DB 00h
;;51D3h
       DW 0000h
       DB 00h
;;51D6h
       DW 0000h
       DB 00h
;;51D9h
       DW 0000h
       DB 00h
;_DrawTableEditor	equ 51DCh ;draws table editor lines
       DW 0000h
       DB 00h
;_DisplayListNameEquals	equ 51DFh
       DW 0000h
       DB 00h
;_DisplayListHeader	equ 51E2h
       DW 0000h
       DB 00h
;_DispMatrixDimensions	equ 51E5h
       DW 0000h
       DB 00h
;_HighlightListEdItem	equ 51E8h
       DW 0000h
       DB 00h
;;51EBh
       DW 0000h
       DB 00h
;;51EEh
       DW 0000h
       DB 00h
;_MatrixName		equ 51F1h
       DW 0000h
       DB 00h
;;51F4h
       DW 0000h
       DB 00h
;;51F7h
       DW 0000h
       DB 00h
;;51FAh
       DW 0000h
       DB 00h
;;51FDh
       DW 0000h
       DB 00h
;;5200h
       DW 0000h
       DB 00h
;;5203h
       DW 0000h
       DB 00h
;;5206h
       DW 0000h
       DB 00h
;;5209h
       DW 0000h
       DB 00h
;;520Ch
       DW 0000h
       DB 00h
;;520Fh
       DW 0000h
       DB 00h
;_SetupEmptyEditTempEqu	equ 5212h
       DW 0000h
       DB 00h
;_ExecClass1Token	equ 5215h
       DW 0000h
       DB 00h
;_HandleMathTokenParse	equ 5218h
       DW 0000h
       DB 00h
;_MaybePushMultiplyOp	equ 521Bh
       DW 0000h
       DB 00h
;_RestartParseOP1Result	equ 521Eh
       DW 0000h
       DB 00h
;_Chk_Batt_Level		equ 5221h
       DW 0000h
       DB 00h
;;5224h
       DW 0000h
       DB 00h
;;5227h
       DW 0000h
       DB 00h
;;522Ah
       DW 0000h
       DB 00h
;_DisplayListEquals	equ 522Dh
       DW 0000h
       DB 00h
;_GetCurPlotListOffset	equ 5230h
       DW 0000h
       DB 00h
;_GoToLastRow		equ 5233h
       DW 0000h
       DB 00h
;_RectBorder		equ 5236h
       DW 0000h
       DB 00h
;;5239h
       DW 0000h
       DB 00h
;;523Ch
       DW 0000h
       DB 00h
;;523Fh
       DW 0000h
       DB 00h
;_LoadA5			equ 5242h
       DW 0000h
       DB 00h
;;5245h
       DW 0000h
       DB 00h
;_NamedListToOP1		equ 5248h
       DW 0000h
       DB 00h
;;524Bh
       DW 0000h
       DB 00h
;;524Eh
       DW 0000h
       DB 00h
;;5251h
       DW 0000h
       DB 00h
;_InitUSBDeviceCallback	equ 5254h
       DW 0000h
       DB 00h
;_KillUSBDevice		equ 5257h ;this actually recycles the USB connection and re-inits it (I think)
       DW 0000h
       DB 00h
;_SetUSBConfiguration	equ 525Ah
       DW 0000h
       DB 00h
;_RequestUSBData		equ 525Dh
       DW 0000h
       DB 00h
;_StopReceivingUSBData	equ 5260h
       DW 0000h
       DB 00h
;_FindAppHeaderByPage	equ 5263h
       DW 0000h
       DB 00h
;_FindNextHeaderByPage	equ 5266h
       DW 0000h
       DB 00h
;_IsMatchingLaunchApp	equ 5269h
       DW 0000h
       DB 00h
;_InitTimer		equ 526Ch
       DW 0000h
       DB 00h
;_KillTimer		equ 526Fh
       DW 0000h
       DB 00h
;_StartTimer		equ 5272h
       DW 0000h
       DB 00h
;_RestartTimer		equ 5275h
       DW 0000h
       DB 00h
;_StopTimer		equ 5278h
       DW 0000h
       DB 00h
;_WaitTimer		equ 527Bh
       DW 0000h
       DB 00h
;_CheckTimer		equ 527Eh
       DW 0000h
       DB 00h
;_CheckTimerRestart	equ 5281h
       DW 0000h
       DB 00h
;_SetVertGraphActive	equ 5284h
       DW 0000h
       DB 00h
;_ClearVertGraphActive	equ 5287h
       DW 0000h
       DB 00h
;_EnableUSBHook		equ 528Ah
       DW 0000h
       DB 00h
;_DisableUSBHook		equ 528Dh
       DW 0000h
       DB 00h
;_InitUSBDevice		equ 5290h
       DW 0000h
       DB 00h
;_KillUSBPeripheral	equ 5293h
       DW 0000h
       DB 00h
;_GetCurPlotListOffset	equ 5296h
       DW 0000h
       DB 00h
;;5299h
       DW 0000h
       DB 00h
;_GraphLine		equ 529Ch
       DW 0000h
       DB 00h
;;529Fh
       DW 0000h
       DB 00h
;;52A2h
       DW 0000h
       DB 00h
;;52A5h
       DW 0000h
       DB 00h
;;52A8h
       DW 0000h
       DB 00h
;;52ABh
       DW 0000h
       DB 00h
;;52AEh
       DW 0000h
       DB 00h
;_ZifTableEditor		equ 52B1h
       DW 0000h
       DB 00h
;;52B4h
       DW 0000h
       DB 00h
;_GetCurPlotOffset	equ 52B7h
       DW 0000h
       DB 00h
;;52BAh
       DW 0000h
       DB 00h
;_FindAppName		equ 52BDh
       DW 0000h
       DB 00h
;;52C0h
       DW 0000h
       DB 00h
;;52C3h
       DW 0000h
       DB 00h
;_UpdateStatPlotLists	equ 52C6h
       DW 0000h
       DB 00h
;_GrBufCpyCustom		equ 52C9h
       DW 0000h
       DB 00h
;;52CCh
       DW 0000h
       DB 00h
;;52CFh
       DW 0000h
       DB 00h
;;52D2h
       DW 0000h
       DB 00h
;_VDispRealOP1		equ 52D5h
       DW 0000h
       DB 00h
;_DispXEqualsNum		equ 52D8h
       DW 0000h
       DB 00h
;_ResetGraphSettings	equ 52DBh
       DW 0000h
       DB 00h
;_InitializeVariables	equ 52DEh
       DW 0000h
       DB 00h
;;52E1h ;bit 4,(9C75h) (this is DEFINITELY returning the status of something when acting as a TI-SmartView Input Pad...this bit is bit 1 of the data byte from a PC HID Set Report request)
       DW 0000h
       DB 00h
;_DelVarSym		equ 52E4h
       DW 0000h
       DB 00h
;_FindAppUpNoCase	equ 52E7h
       DW 0000h
       DB 00h
;_FindAppDnNoCase	equ 52EAh
       DW 0000h
       DB 00h
;_DeleteInvalidApps	equ 52EDh
       DW 0000h
       DB 00h
;_DeleteApp_Link		equ 52F0h
       DW 0000h
       DB 0FEh
;_CmpSymsNoCase		equ 52F3h
       DW 0000h
       DB 00h
;_SetAppRestrictions	equ 52F6h
       DW 0000h
       DB 00h
;_RemoveAppRestrictions	equ 52F9h
       DW 0000h
       DB 00h
;_QueryAppRestrictions	equ 52FCh
       DW 0000h
       DB 00h
;_DispAppRestrictions	equ 52FFh
       DW 0000h
       DB 00h
;_SetupHome		equ 5302h
       DW 0000h
       DB 00h
;_GRPUTAWAYFull		equ 5305h ;same as _GRPUTAWAY except it assumes no split screen
       DW 0000h
       DB 00h
;_SendSmartPadKeypress	equ 5308h ;B and A are the inputs
       DW 0000h
       DB 00h
;_ToggleUSBSmartPadInput	equ 530Bh ;A is input, 0 or 1 to enable/disable
       DW 0000h
       DB 00h
;_IsUSBDeviceConnected	equ 530Eh ;bit 4,(81h) \ ret, this is just a guess on its purpose but it seems to work
       DW 0000h
       DB 00h
;_RecycleUSB		equ 5311h ;identical to 5257h
       DW 0000h
       DB 00h
;_PolarEquToOP1		equ 5314h
       DW 0000h
       DB 00h
;_ParamXEquToOP1		equ 5317h
       DW 0000h
       DB 00h
;_ParamYEquToOP1		equ 531Ah
       DW 0000h
       DB 00h
;_GetRestrictionsOptions	equ 531Dh
       DW 0000h
       DB 00h
;_DispResetComplete	equ 5320h
       DW 0000h
       DB 00h
;_PTTReset		equ 5323h
       DW 0000h
       DB 00h
;_FindAppCustom		equ 5326h
       DW 0000h
       DB 00h
;_ClearGraphStyles	equ 5329h
       DW 0000h
       DB 00h

       DB 00h,00h,00h,00h
;5330h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5350h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5370h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5390h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;53B0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;53D0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;53F0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5410h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5430h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5450h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5470h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5490h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;54B0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;54D0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;54F0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5510h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5530h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5550h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5570h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5590h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;55B0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;55D0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;55F0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5610h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5630h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5650h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5670h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5690h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;56B0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;56D0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;56F0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5710h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5730h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5750h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5770h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5790h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;57B0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;57D0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;57F0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5810h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5830h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5850h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5870h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5890h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;58B0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;58D0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;58F0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5910h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5930h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5950h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5970h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5990h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;59B0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;59D0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;59F0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5A10h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5A30h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5A50h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5A70h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5A90h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5AB0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5AD0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5AF0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5B10h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5B30h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5B50h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5B70h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5B90h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5BB0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5BD0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5BF0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5C10h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5C30h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5C50h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5C70h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5C90h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5CB0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5CD0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5CF0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5D10h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5D30h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5D50h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5D70h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5D90h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5DB0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5DD0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5DF0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5E10h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5E30h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5E50h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5E70h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5E90h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5EB0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5ED0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5EF0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5F10h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5F30h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5F50h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5F70h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5F90h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5FB0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5FD0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;5FF0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6010h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6030h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6050h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6070h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6090h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;60B0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;60D0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;60F0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6110h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6130h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6150h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6170h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6190h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;61B0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;61D0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;61F0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6210h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6230h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6250h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6270h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6290h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;62B0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;62D0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;62F0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6310h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6330h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6350h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6370h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6390h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;63B0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;63D0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;63F0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6410h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6430h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6450h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6470h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6490h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;64B0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;64D0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;64F0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6510h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6530h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6550h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6570h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6590h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;65B0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;65D0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;65F0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6610h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6630h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6650h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6670h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6690h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;66B0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;66D0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;66F0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6710h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6730h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6750h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6770h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6790h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;67B0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;67D0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;67F0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6810h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6830h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6850h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6870h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6890h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;68B0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;68D0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;68F0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6910h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6930h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6950h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6970h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6990h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;69B0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;69D0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;69F0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6A10h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6A30h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6A50h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6A70h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6A90h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6AB0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6AD0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6AF0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6B10h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6B30h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6B50h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6B70h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6B90h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6BB0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6BD0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6BF0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6C10h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6C30h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6C50h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6C70h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6C90h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6CB0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6CD0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6CF0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6D10h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6D30h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6D50h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6D70h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6D90h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6DB0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6DD0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6DF0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6E10h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6E30h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6E50h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6E70h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6E90h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6EB0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6ED0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6EF0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6F10h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6F30h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6F50h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6F70h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6F90h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6FB0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6FD0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;6FF0h
       DB 00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h,00h
;7000h
;This is the start of the OS2 BCALL jump table.
_UnlockFlash:
       DW UnlockFlash
       DB 7Ch

