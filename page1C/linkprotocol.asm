 DEFINE PAGE1C, SPACE=ROM
 SEGMENT PAGE1C

 include "includes\os2.inc"
 include "includes\internal.inc"

 PUBLIC SendCommandA,ReceiveHeaderPacket,SendSkipExitPacket,SendCAndAddToChecksum,SendContinue,ReceiveDataPacket
 PUBLIC receiveRestOfDataPacket,SendHCommand,SendChecksumGetAcknowledge
 EXTERN SetupPagedPtr,EnableLinkAssist,Send4Bytes,PagedGet,SendAByte,Get4BytesNC,JErrorNo
 EXTERN SendCByte,RecAByteIO,SaveFlashBytes,SendAcknowledge

SendContinue:
       ld hl,0973h
       ld (header),hl
       ld hl,0
       ld (header+2),hl
       call Send4Bytes
       call Get4BytesNC
       ld a,(header+1)
       cp 56h
       ret z
       jp JErrorNo

SendCommandA:
       ex de,hl
       ld hl,ioData
SendACommand:
       ld b,0
       ex de,hl
       call SetupPagedPtr
       ex de,hl
       ld (iMathPtr5),hl
       ld (header+2),de
       ld h,a
       call EnableLinkAssist
       set indicOnly,(iy+indicFlags)
       ld l,73h
       ld (header),hl
       call Send4Bytes
       ld hl,(iMathPtr5)
       ld de,(header+2)
       ld bc,0
       ld (header+4),bc
       jr startSendCmdLoop
sendCmdLoop:
       push de
       ld a,(pagedPN)
       or a
       jr z,$F
       call PagedGet
       ld c,a
       jr sendCmdByte
$$:    ld c,(hl)
sendCmdByte:
       call SendCAndAddToChecksum
       pop de
       dec de
       inc hl
startSendCmdLoop:
       ld a,d
       or e
       jr nz,sendCmdLoop
SendChecksumGetAcknowledge:
       ld hl,(header+4)
       ld a,l
       push hl
       call SendAByte
       pop af
       call SendAByte
ReceiveAcknowledge:
       call Get4BytesNC
       ld a,(header+1)
       cp 56h
       ret z
       jp JErrorNo
SendCAndAddToChecksum:
       ld b,0
       ex de,hl
       ld hl,(header+4)
       add hl,bc
       ld (header+4),hl
       ex de,hl
       jr SendCByte

SendSkipExitPacket:
       ld hl,header+8
       ld (hl),a
       ld a,36h
       jr SendACommand

ReceiveHeaderPacket:
       xor a
       ld (header+8),a
       ld (ioNewData),a
       ld hl,ioData
       jr $F
ReceiveDataPacket:
       ld (iMathPtr5),de
       push bc
       call Get4BytesNC
       pop bc
       ld hl,ioFlag
       ld a,(header+1)
       cp 15h
       jp nz,JErrorNo
       ld hl,(header+2)
       or a
       sbc hl,bc
       jp nz,JErrorNo
       jr receiveRestOfDataPacket
GetSmallDataPacket:
       ld hl,header+8
$$:    ld (iMathPtr5),hl
       ld hl,(header+2)
       ld de,15
       or a
       sbc hl,de
       jr c,receiveRestOfDataPacket
       jp JErrorNo
receiveRestOfDataPacket:
       ld bc,(header+2)
       ld de,0
       ld (header+4),de
       ld a,b
       or c
       jr z,receiveDataDone
       ld hl,pagedBuf
       ld (pagedGetPtr),hl
       xor a
       ld (pagedCount),a
       ld hl,(iMathPtr5)
receiveDataLoop:
       in a,(interruptStatusPort)
       bit INT_STATUS_ON,a
       jr z,JErrorNo
       push bc
       call RecAByteIO
       bit 7,h
       jr nz,$F
       push hl
       ld hl,(pagedGetPtr)
       ld (hl),a
       inc hl
       ld (pagedGetPtr),hl
       pop hl
       ld a,(pagedCount)
       inc a
       ld (pagedCount),a
       cp 16                       ;buffer 16 bytes at a time
       call z,SaveFlashBytes
       jr receiveDataContinue
$$:    ld (hl),a
       inc hl
receiveDataContinue:
       ex de,hl
       ld b,0
       ld hl,(header+4)
       add hl,bc
       ld (header+4),hl
       ex de,hl
       pop bc
       dec bc
       ld a,b
       or c
       jr nz,receiveDataLoop
       ld a,(pagedCount)
       or a
       call nz,SaveFlashBytes      ;flush out the buffer
receiveDataDone:
       call RecAByteIO
       push af
       call RecAByteIO
       ld b,a
       pop af
       call ValidateChecksum
       jr SendAcknowledge
ValidateChecksum:
       ld c,a
       in a,(statusPort)
       and STATUS_NON_83P_MASK
       jr z,vcNotSE
       in a,(speedPort)
       and 1
       jr z,vcNotSE
       xor a
       out (speedPort),a
       ld de,0FEh
$$:    dec de
       ld a,d
       or e
       jr nz,$B
       ld a,1
       out (speedPort),a
       jr $F
vcNotSE:
       ld de,100h
$$:    dec de
       ld a,d
       or e
       jr nz,$B
$$:    ld hl,(header+4)
       or a
       sbc hl,bc
       jr nz,$F
       ret
$$:    ld hl,(header+2)
       push hl
       ld h,5Ah
       call SendHCommand
       jp JErrorNo
SendHCommand:
       set indicOnly,(iy+indicFlags)
       ld l,73h
       ld (header),hl
       di
       call Send4Bytes
       ei
       jr ReceiveAcknowledge

