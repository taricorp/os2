 DEFINE PAGE1C, SPACE=ROM
 SEGMENT PAGE1C

 include "includes\os2.inc"
 include "includes\internal.inc"

 PUBLIC RecAByteIO,ContinueGetByte,SendAByte,SendCByte,Send4Bytes,Rec1stByteNC
 EXTERN NZIf83Plus,JErrorNo,EnableLinkAssist,ResetLATimeout,NZIfTimeout,HandleLinkKeyActivity

RecAByteIO:
       di
       call ReceiveByteToC
       ld a,c
       ret
ReceiveByteToC:
       ld b,8
receiveByteLoop:
       call NZIf83Plus
       jr z,ReceiveLAByteToC
       ld de,0FFFFh
       jr startReceiveLoop
receiveLoop:
       in a,(0)
       and 3
       jp z,JErrorNo
       cp 3
       jr nz,ReceiveByteToC_Ready
       in a,(0)
       and 3
       jr z,JErrorNo
       cp 3
       jr nz,ReceiveByteToC_Ready
startReceiveLoop:
       dec de
       ld a,d
       or e
       jr nz,receiveLoop
       jp JErrorNo
ReceiveByteToC_Ready:
       cp 2
       jr z,RBTC_1
       ld a,1
       out (0),a
       rr c
       ld de,0FFFFh
$$:    in a,(0)
       and 3
       cp 2
       jr z,RBTC_2
       dec de
       ld a,d
       or e
       jr nz,$B
       jp JErrorNo
RBTC_2:xor a
       out (0),a
       ld d,4
$$:    dec d
       jr z,$F
       in a,(0)
       and 3
       cp 3
       jr nz,$B
$$:    djnz receiveByteLoop
       xor a
       ret
RBTC_1:ld a,2
       out (0),a
       rr c
       ld de,0FFFFh
$$:    in a,(0)
       and 3
       cp 1
       jr z,RBTC_2
       dec de
       ld a,d
       or e
       jr nz,$B
       jp JErrorNo
ReceiveLAByteToC:
       call EnableLinkAssist
       call ResetLATimeout
$$:    xor a
       ld (laTimer),a
       call NZIfTimeout
       jp nz,JErrorNo
       in a,(laStatusPort)
       bit 6,a
       jr nz,$F
       and 11h
       jr z,$B
       in a,(laReceivePort)
       ld c,a
       ld a,0
       ret
$$:    and 11h
       jp z,JErrorNo
       in a,(laReceivePort)
       ld c,a
       ld a,1
       ret

Rec1stByteNC:
       di
       res indicOnly,(iy+indicFlags)
       ld a,lcdXAutoIncrementMode
       out (LCDinstPort),a
       ei
       call NZIf83Plus
       jr nz,$F
       call EnableLinkAssist
       call LAReceiveByteToC
       ld a,c
       ret
$$:    bit onInterrupt,(iy+onFlags)
       jp nz,JErrorNo
       call HandleLinkKeyActivity
       in a,(0)
       and 3
       cp 3
       jr z,$B
       set indicOnly,(iy+indicFlags)
       call ReceiveByteToC_Ready
       ld a,c
       ret
LAReceiveByteToC:
       bit onInterrupt,(iy+onFlags)
       jp nz,JErrorNo
       call HandleLinkKeyActivity
       in a,(laStatusPort)
       and 58h
       jr z,LAReceiveByteToC
       set indicOnly,(iy+indicFlags)
       jr ReceiveLAByteToC

ContinueGetByte:
       set indicOnly,(iy+indicFlags)
       di
       call NZIf83Plus
       jr nz,cgbNoAssist
       di
       call ReceiveLAByteToC
       ld a,c
       ret
cgbNoAssist:
       ld b,8
       jr ReceiveByteToC_Ready

Send4Bytes:
       call EnableLinkAssist
       ld hl,250
$$:    dec hl
       ld a,h
       or l
       jr nz,$B
send4Loop:
       call NZIf83Plus
       jr z,send4LA
       xor a
       out (0),a
       in a,(0)
       and 3
       cp 3
       jr z,sendBytes
send4ReceivedByte:
       call ReceiveByteToC
       jr send4Loop
send4LA:
       in a,(9)
       and 19h
       jr nz,send4ReceivedByte
sendBytes:
       ld hl,0
       ld (header+4),hl
       ld a,(header)
       call SendAByte
       ld a,(header+1)
       call SendAByte
       ld a,(header+2)
       call SendAByte
       ld a,(header+3)
SendAByte:
       ld c,a
SendCByte:
       call NZIf83Plus
       jr z,sendByteLA
       ld b,8
sendLoop:
       ld de,0FFFFh
       rr c
       jr nc,$F
       ld a,2
       jr sendContinue
$$:    ld a,1
sendContinue:
       out (0),a
bitLoop:
       in a,(0)
       and 3
       jr z,bitDone
       in a,(0)
       and 3
       jr z,bitDone
       dec de
       ld a,d
       or e
       jr nz,bitLoop
       jp JErrorNo
bitDone:
       xor a
       out (0),a
       ld de,0FFFFh
bitReallyDone:
       dec de
       ld a,d
       or e
       jp z,JErrorNo
       in a,(0)
       and 3
       cp 3
       jr nz,bitReallyDone
       djnz sendLoop
       ret
sendByteLA:
       call EnableLinkAssist
       call ResetLATimeout
sendByteLA_1:
       ld a,0FAh
       ld (laTimer),a
       in a,(laStatusPort)
       bit 5,a
       jr z,$F
       ld a,c
       out (laSendPort),a
       ret
$$:    call NZIfTimeout
       jr z,sendByteLA_1
       jp JErrorNo

