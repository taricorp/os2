 DEFINE PAGE1C, SPACE=ROM
 SEGMENT PAGE1C

 include "includes\os2.inc"
 include "includes\internal.inc"

 PUBLIC ResetLATimeout,NZIfTimeout,Get3Bytes,Get4BytesNC,SaveFlashBytes
 PUBLIC CheckLinkLines,SendAcknowledge,EnableLinkAssist,DisableLinkAssist
 EXTERN NZIf83Plus,Send4Bytes,Rec1stByteNC,RecAByteIO,LockFlash

CheckLinkLines:
       ld bc,15
       ld hl,ioData
       ld (hl),a
       call NZIf83Plus
       jr nz,$F
       bit linkAssistEnabled,(iy+linkAssistFlags)
       jr nz,xorAret
       in a,(speedPort)
       and 1
       jr z,$F
       ld bc,32
$$:    in a,(0)
       and 3
       cp (hl)
       ret nz
       dec bc
       ld a,b
       or c
       jr nz,$B
       ret
xorAret:
       xor a
       ret

SendAcknowledge:
       ld hl,0
SendAcknowledgeHL:
       ld (header+2),hl
       ld hl,5673h
       ld (header),hl
       jr Send4Bytes

Get4BytesNC:
       call Rec1stByteNC
Get3Bytes:
       ld (header),a
Get3From4BytePacket:
       call RecAByteIO
       ld (header+1),a
       call RecAByteIO
       ld (header+2),a
       call RecAByteIO
       ld (header+3),a
       ld a,(header+1)
       ret

EnableLinkAssist:
       call NZIf83Plus
       ret nz
       bit linkAssistEnabled,(iy+linkAssistFlags)
       ret nz
       set linkAssistEnabled,(iy+linkAssistFlags)
       in a,(laStatusPort)
       xor a
       out (laEnPort),a
       ret

DisableLinkAssist:
       in a,(statusPort)
       and STATUS_NON_83P_MASK
       ret z
       in a,(laStatusPort)
       and 99h
       ret nz
       res linkAssistEnabled,(iy+linkAssistFlags)
       ld a,80h
       out (laEnPort),a
       ret

NZIfTimeout:
       push hl
       ld h,2
$$:    dec h
       jr nz,$B
       ld hl,(linkDelay)
       dec hl
       ld (linkDelay),hl
       ld a,h
       or l
       jr nz,$F
       ld a,1
       jr NZIT_1
$$:    ld a,0
NZIT_1:or a
       pop hl
       ret

ResetLATimeout:
       push hl
       ld hl,0FFFFh
       in a,(speedPort)
       and 1
       jr nz,$F
       ld hl,6800h
$$:    ld (linkDelay),hl
       pop hl
       ret

SaveFlashBytes:
       push bc
       push de
       push hl
       ld a,(pagedCount)
       ld c,a
       xor a
       ld (pagedCount),a
       ld hl,pagedBuf
       ld (pagedGetPtr),hl
       ld de,(iMathPtr5)
       ld a,(arcInfo)
       push de
       ld (Abackup),a
       ld a,i
       jp pe,$F
       ld a,i
$$:    di
       push af
       ld a,(Abackup)
       push af
       ld a,1
       nop
       di
       nop
       nop
       im 1
       di
       out (14h),a
       di
       pop af
       ld b,0
       B_CALL WriteFlash
       call LockFlash
       ld (Abackup),a
       pop af
       jp po,$F
       ei
$$:    ld a,(83EBh)
       ld (iMathPtr5),de
       pop hl
       or a
       sbc hl,de
       jr c,$F
       ld a,(arcInfo)
       inc a
       ld (arcInfo),a
$$:    pop hl
       pop de
       pop bc
       ret

