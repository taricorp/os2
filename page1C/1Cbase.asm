 DEFINE PAGE1C, SPACE=ROM
 SEGMENT PAGE1C

 include "includes\os2.inc"
 include "includes\internal.inc"

 PUBLIC UnlockFlash,LockFlash,OutputProtectedPort
 EXTERN DispHexA,NZIf83Plus,PutC,DispHexHL,HandleLinkKeyActivity,SetupPagedPtr,PagedGet
 ;You can execute the below routines only because they rely on page 0.
 EXTERN JErrorNo,LdHLInd

LockFlash:
       push bc
       ld b,0
       jr $F
UnlockFlash:
       di
       push af
       ld a,1
       nop
       nop
       im 1
       di
       out (14h),a
       pop af
       ret
       push bc
       ld b,1
$$:    ld c,14h
       push af
       call OutputProtectedPort
       pop af
       pop bc
       ret

OutputProtectedPort:
;Outputs value B to protected port C
       ld a,i
       jp pe,$F
       ld a,i
$$:    push af
       di
       nop
       nop
       im 1
       di
       out (c),b
       pop af
       ret po
       ei
       ret

