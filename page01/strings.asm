 DEFINE PAGE01, SPACE=ROM
 SEGMENT PAGE01

 include "includes\os2.inc"

 PUBLIC KeyToString
 EXTERN LdHLInd

KeyToString:
       ld a,e
       ld l,a
       ld h,0
       add hl,hl
       ld de,keyToStringTable-2
       add hl,de
       call LdHLInd
       dec hl
       ld de,keyForStr
       ld bc,18
       ldir
       ld hl,keyForStrRam
       ret
keyToStringTable:
       DW 0000h
       DW 0000h
       DW 0000h
       DW sTools
sTools:
       DB sEnd-$F
$$:    DB "Tools",0CEh
sEnd:
