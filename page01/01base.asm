 DEFINE PAGE01, SPACE=ROM
 SEGMENT PAGE01

 include "includes\os2.inc"

 PUBLIC GetKey
 EXTERN GetCSC,HandleLinkKeyActivity,EraseEOL,resetAPDTimer,LCDDelay,PutMap,HandleLinkKeyActivity
 EXTERN saveTR,restoreTR,CanAlph,ResAlph,showCursor

GetKey:
       xor a
GetKeyMainLoop:
       call DisplayShift
       call showCursor
       set apdRunning,(iy+apdFlags)
restartKeyLoop:
       in a,(statusPort)
       and STATUS_NON_83P_MASK
       jr z,skipLinkAssistDisable
       bit 0,(iy+3Eh)
       jr z,disableLA
       in a,(9)
       and 99h
       jr z,disableLATotal
       and 19h
       jr nz,receivingData
       jr $F
disableLATotal:
       res 0,(iy+3Eh)
disableLA:
       ld a,80h
       out (8),a
skipLinkAssistDisable:
       set enableHW2Timer,(iy+interruptFlags)
       bit onInterrupt,(iy+onFlags)
       res onInterrupt,(iy+onFlags)
       jr z,$F
       xor a
       ret
$$:    call GetCSC
GetKeyHandleScanCode:
       or a
       jr nz,scanCodeFound
$$:    call HandleLinkKeyActivity
       bit hw2TimerSkipped,(iy+linkKeyFlags)
       jr nz,$B
       in a,(2)
       and 80h
       jr z,$F
       bit 0,(iy+3Eh)
       jr z,$F
       in a,(laStatusPort)
       bit 6,a
       jr z,noError
       set 7,(iy+3Dh)
noError:
       and 19h
       jr isReceivingData
$$:    in a,(0)
       and 3
       cp 3
isReceivingData:
       jr nz,receivingData
       ld a,(9CB1h)
       or a
       jr z,restartKeyLoop
       jr GetKeyMainLoop
receivingData:
       ld hl,kbdKey
       ld (hl),0
       B_CALL keyscnlnk
;       res indicOnly,(iy+indicFlags)      ;***TESTING
       ld a,(kbdKey)
       or a
       jr z,GetKeyMainLoop
GetKeyReturn:
       ;Say what?
       ret
scanCodeFound:
       push af
       xor a
       ld (9CB1h),a
       ld (9CB0h),a
       pop af
       bit kbdKeyPress,(iy+kbdFlags)
       jr z,$F
       call resetAPDTimer
       res apdRunning,(iy+apdFlags)
       res apdWarmStart,(iy+apdFlags)
$$:    bit shift2nd,(iy+shiftFlags)
       jr nz,keyPressedWith2nd
       bit shiftAlpha,(iy+shiftFlags)
       jr nz,keyPressedWithAlpha
       cp sk2nd
       jr nz,$F
       set shift2nd,(iy+shiftFlags)
       jr GetKeyMainLoop
$$:    cp skAlpha
       jr nz,TranslateKeyCode
       set shiftAlpha,(iy+shiftFlags)
       res shiftLwrAlph,(iy+shiftFlags)
       jr GetKeyMainLoop
Translate2ndKeyCode:
       add a,38h
TranslateKeyCode:
       ld l,a
       ld h,0
       ld de,scanCodeTable-1
       add hl,de
TranslateKeyCodeHL:
       ld a,(hl)
       or a
       jr nz,GetKeyReturn
       jr GetKeyMainLoop
keyPressedWith2nd:
       res shift2nd,(iy+shiftFlags)
       bit saIndic,(iy+newIndicFlags)
       jr z,$F
       push af
       call restoreTR
       res saIndic,(iy+newIndicFlags)
       pop af
$$:    cp sk2nd
       jr z,$F
       cp skAlpha
       jr nz,handle2ndPressed
       set shiftALock,(iy+shiftFlags)
       set shiftAlpha,(iy+shiftFlags)
$$:    jr GetKeyMainLoop
handle2ndPressed:
       ld b,32h
AdjustContrast:
       ld hl,contrast
       cp 4
       jr nz,$F
       ld a,(hl)
       cp 27h
       jr nc,skipContrastAdjustment
       inc a
       jr setContrast
$$:    cp 1
       jr nz,Translate2ndKeyCode
       ld a,(hl)
       or a
       jr z,skipContrastAdjustment
       dec a
setContrast:
       ld (hl),a
       add a,18h
       or 0C0h
       di
       call LCDDelay
       out (LCDinstPort),a
       ei
       ld hl,8442h
       ld (hl),b
skipContrastAdjustment:
       set apdRunning,(iy+apdFlags)
       call saveTR
       ld hl,(curRow)
       push hl
       ld hl,0F00h
       ld (curRow),hl
       ld l,(iy+textFlags)
       ld h,(iy+appFlags)
       push hl
       ld a,(flags+plotFlags)
       push af
       set textInverse,(iy+textFlags)
       res appTextSave,(iy+appFlags)
       ld a,(contrast)
       srl a
       srl a
       add a,'0'
       di
       call PutMap
       ei
       pop af
       ld (flags+plotFlags),a
       pop hl
       ld (iy+appFlags),h
       ld (iy+textFlags),l
       pop hl
       ld (curRow),hl
$$:    call HandleLinkKeyActivity
       bit kbdSCR,(iy+kbdFlags)
       jr z,$B
       call GetCSC
       ld b,0Ah
       cp 1
       jr z,AdjustContrast
       cp 4
       jr z,AdjustContrast
       call restoreTR
       jr GetKeyHandleScanCode
keyPressedWithAlpha:
       cp skAlpha
       jr nz,handleAlphaNotAlphaPress
       bit lwrCaseActive,(iy+appLwrCaseFlag)
       jr z,$F
       bit shiftLwrAlph,(iy+shiftFlags)
       jr nz,$F
       set shiftLwrAlph,(iy+shiftFlags)
       jr GetKeyMainLoop
$$:    call CanAlph
       jr GetKeyMainLoop
handleAlphaNotAlphaPress:
       cp sk2nd
       jr nz,$F
       set shift2nd,(iy+shiftFlags)
       jr GetKeyMainLoop
$$:    call ResAlph
       bit shiftAlpha,(iy+shiftFlags)
       jr nz,$F
       bit saIndic,(iy+newIndicFlags)
       jr z,$F
       push af
       call restoreTR
       res saIndic,(iy+newIndicFlags)
       pop af
$$:    add a,70h
       bit shiftLwrAlph,(iy+shiftFlags)
       jr z,TranslateKeyCode
       add a,38h
       ld l,a
       ld h,0
       ld de,scanCodeTable-1
       add hl,de
       ld a,(hl)
       cp 0E2h
       jr c,TranslateKeyCodeHL
       cp 0FCh
       jr nc,TranslateKeyCodeHL
       ld (8446h),a
       ld a,0FCh
       jr GetKeyReturn

scanCodeTable:
       DB kDown      ;skDown
       DB kLeft      ;skLeft
       DB kRight     ;skRight
       DB kUp        ;skUp
       DB 00h
       DB 00h
       DB 00h
       DB 00h
       DB kEnter     ;skEnter
       DB kAdd       ;skAdd
       DB kSub       ;skSub
       DB kMul       ;skMul
       DB kDiv       ;skDiv
       DB kExpon     ;skPower
       DB kClear     ;skClear
       DB 00h
       DB kChs       ;skChs
       DB k3         ;sk3
       DB k6         ;sk6
       DB k9         ;sk9
       DB kRParen    ;skRParen
       DB kTan       ;skTan
       DB kVars      ;skVars
       DB 00h
       DB kDecPnt    ;skDecPnt
       DB k2         ;sk2
       DB k5         ;sk5
       DB k8         ;sk8
       DB kLParen    ;skLParen
       DB kCos       ;skCos
       DB kPrgm      ;skPrgm
       DB kStat      ;skStat
       DB k0         ;sk0
       DB k1         ;sk1
       DB k4         ;sk4
       DB k7         ;sk7
       DB kComma     ;skComma
       DB kSin       ;skSin
       DB kAppsMenu  ;skMatrix
       DB kVarx      ;skGraphvar
       DB 00h
       DB kStore     ;skStore
       DB kLn        ;skLn
       DB kLog       ;skLog
       DB kSquare    ;skSquare
       DB kInv       ;skRecip
       DB kMath      ;skMath
       DB 00h
       DB kGraph     ;skGraph
       DB kTrace     ;skTrace
       DB kZoom      ;skZoom
       DB kWindow    ;skWindow
       DB kYequ      ;skYEqu
       DB 00h
       DB kMode      ;skMode
       DB kDel       ;skDel
       DB kDown
       DB kBOL
       DB kEOL
       DB kUp
       DB 00h
       DB 00h
       DB 00h
       DB 00h
       DB kLastEnt
       DB kMem
       DB kRBrack
       DB kLBrack
       DB kCONSTeA
       DB kPi
       DB kClear
       DB 00h
       DB kAns
       DB kL3A
       DB kL6A
       DB kwnA
       DB kRBrace
       DB kATan
       DB kDist
       DB 00h
       DB kI
       DB kL2A
       DB kL5A
       DB kvnA
       DB kLBrace
       DB kACos
       DB kDraw
       DB kList
       DB kCatalog
       DB kL1A
       DB kL4A
       DB kunA
       DB kEE
       DB kASin
       DB kAngle
       DB kLinkIO
       DB 00h
       DB kRecall
       DB kExp
       DB kALog
       DB kSqrt
       DB kMatrix
       DB kTest
       DB 00h
       DB kTable
       DB kCalc
       DB kFormat
       DB kTblSet
       DB kSPlot
       DB 00h
       DB kQuit
       DB kIns
       DB kAlphaDown
       DB kLeft
       DB kRight
       DB kAlphaUp
       DB 00h
       DB 00h
       DB 00h
       DB 00h
       DB kAlphaEnter
       DB kQuote
       DB kCapW
       DB kCapR
       DB kCapM
       DB kCapH
       DB kClear
       DB 00h
       DB kQuest
       DB kTheta
       DB kCapV
       DB kCapQ
       DB kCapL
       DB kCapG
       DB kVars
       DB 00h
       DB kColon
       DB kCapZ
       DB kCapU
       DB kCapP
       DB kCapK
       DB kCapF
       DB kCapC
       DB kStat
       DB kSpace
       DB kCapY
       DB kCapT
       DB kCapO
       DB kCapJ
       DB kCapE
       DB kCapB
       DB kVarx
       DB 00h
       DB kCapX
       DB kCapS
       DB kCapN
       DB kCapI
       DB kCapD
       DB kCapA
       DB 00h
       DB kGraph
       DB kTrace
       DB kZoom
       DB kWindow
       DB kYequ
       DB 00h
       DB kMode
       DB kDel
       DB kAlphaDown
       DB kLeft
       DB kRight
       DB kAlphaUp
       DB 00h
       DB 00h
       DB 00h
       DB 00h
       DB kAlphaEnter
       DB kQuote
       DB kL6A
       DB kL1A
       DB kI
       DB kASinH
       DB kClear
       DB 00h
       DB kQuest
       DB kTheta
       DB kL5A
       DB kFMax
       DB kRBrace
       DB kTanH
       DB kVars
       DB 00h
       DB kColon
       DB kwnA
       DB kL4A
       DB kFMin
       DB kLBrace
       DB kCosH
       DB kClrHome
       DB kStat
       DB kSpace
       DB kvnA
       DB kL3A
       DB kPlot3
       DB kATanH
       DB kSinH
       DB kGetKey
       DB kVarx
       DB 00h
       DB kunA
       DB kL2A
       DB kCONSTeA
       DB kACosH
       DB kPrtScr
       DB kOutput

DisplayShift:
       bit extraIndic,(iy+newIndicFlags)
       ret nz
       bit 2,(iy+3Eh)
       jr nz,$F
       bit curAble,(iy+curFlags)
       jr z,$F
       bit appCurGraphic,(iy+appFlags)
       jr nz,$F
       bit appCurWord,(iy+appFlags)
       jr nz,$F
       bit saIndic,(iy+newIndicFlags)
       ret z
       call restoreTR
       res saIndic,(iy+newIndicFlags)
       ret
$$:    bit saIndic,(iy+newIndicFlags)
       jr z,$F
       call GetShiftCharacter
       jr nz,ds1
       call restoreTR
       res saIndic,(iy+newIndicFlags)
       ret
$$:    call GetShiftCharacter
       ret z
       set saIndic,(iy+newIndicFlags)
       call saveTR
ds1:   ld hl,(curRow)
       push hl
       ld hl,0F00h
       ld (curRow),hl
       ld l,(iy+textFlags)
       ld h,(iy+appFlags)
       push hl
       ld a,(flags+plotFlags)
       push af
       res appTextSave,(iy+appFlags)
       call GetShiftCharacter
       di
       call PutMap
       ei
       pop af
       ld (flags+plotFlags),a
       pop hl
       ld (iy+appFlags),h
       ld (iy+textFlags),l
       pop hl
       ld (curRow),hl
       ret
GetShiftCharacter:
       ld a,0E1h
       ld h,(iy+shiftFlags)
       bit 3,h
       ret nz
       bit 4,h
       ret z
       add a,2
       bit 5,h
       ret nz
       dec a
       ret

