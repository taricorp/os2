@echo off
Hex2ROM 83P OS2.hex OS2.rom
Build8XU -k05 -m02 -n2B -h03 -o05_OS2.8xu 00:0000:OS2.rom 01:4000:OS2.rom 02:8000:OS2.rom 7C:70000:OS2.rom 7D:74000:OS2.rom
Build8XU -k04 -f04.key -m02 -n2B -h03 -o83P_OS2.8xu 00:0000:OS2.rom 01:4000:OS2.rom 02:8000:OS2.rom 7C:70000:OS2.rom 7D:74000:OS2.rom
Build8XU -k0A -f0A.key -m02 -n2B -h03 -o84P_OS2.8xu 00:0000:OS2.rom 01:4000:OS2.rom 02:8000:OS2.rom 7C:70000:OS2.rom 7D:74000:OS2.rom
Build8XU -t73 -k02 -f02.key -m02 -n2B -h03 -o73_OS2.73u 00:0000:OS2.rom 01:4000:OS2.rom 02:8000:OS2.rom 7C:70000:OS2.rom 7D:74000:OS2.rom
