 DEFINE PAGE02, SPACE=ROM
 SEGMENT PAGE02

 include "includes\os2.inc"

 PUBLIC HandleUSBInterruptInitialize,HandleDefaultUSBInterrupt,ReadUSBInterruptData
 PUBLIC HandleUSBACablePluggedIn,HandleUSBACableUnplugged,HandleUSBBCablePluggedIn,HandleUSBBCableUnplugged
 EXTERN PutC

;TODO: deal with all these USB events, somehow
HandleUSBACablePluggedIn:
HandleUSBACableUnplugged:
       ret

HandleUSBBCablePluggedIn:
       set useUSB,(iy+linkFlags)
       ld a,2
       ld (9C28h),a
       ld a,80h
       out (57h),a
       xor a
       out (4Ch),a
       ld a,1
       out (5Bh),a
       xor a
       in a,(4Ch)
       ld a,2
       out (54h),a
       ld a,20h
       out (4Ah),a
       xor a
       out (4Bh),a
       in a,(3Ah)
       bit 3,a
       jr z,$F
       ld a,20h
       out (4Bh),a
$$:    xor a
       out (54h),a
       ld b,1
       call WaitTimerBms
       in a,(3Ah)
       bit 3,a
       jr z,$F
       ld a,44h
       out (54h),a
$$:    ld a,0C4h
       out (54h),a
       ld a,8
       out (4Ch),a
       ld de,0FFFFh
$$:    call DecreaseCounter
       jr z,killUSBscfRet
       in a,(4Ch)
       cp 1Ah
       jr z,$F
       cp 5Ah
       jr z,$B
$$:    ld a,0FFh
       out (87h),a
       xor a
       out (92h),a
       in a,(87h)
       ld a,0Eh
       out (89h),a
       xor a
       ld (9C26h),a
       ld (9C27h),a
       ld a,5
       out (8Bh),a
       ld a,5
       ld (9C28h),a
       in a,(8Fh)
       bit 2,a
       jr z,$F
       in a,(81h)
       or 2
       out (81h),a
       call WaitTimer20ms
       jr continue
$$:    in a,(81h)
       or 1
       out (81h),a
continue:
       in a,(54h)
       or 1
       out (54h),a
       ret

HandleUSBBCableUnplugged:
       ret

HandleUSBInterruptInitialize:
       in a,(4Dh)
       bit 5,a
       jr nz,handleFinalPeriphInit
       ld a,20h
       out (57h),a
       in a,(4Ch)
       cp 5Ah
       ret z
       cp 1Ah
       ret z
       ld a,4
       ld (9C28h),a
       xor a
       out (4Ch),a
       ld a,1
       out (5Bh),a
       xor a
       in a,(4Ch)
       ld a,2
       out (54h),a
       ld a,20h
       out (4Ah),a
       ;TODO: finish this...it's 7AM and I've been at this all night...
       ret
handleFinalPeriphInit:
       set 5,(iy+1Bh)
       ld a,2
       ld (9C28h),a
       ld a,80h
       out (57h),a
       xor a
       out (4Ch),a
       ld a,1
       out (5Bh),a
       ld a,2
       out (54h),a
       ld a,20h
       out (4Ah),a
       call ScrewWithUSBPorts
       ld a,8
       out (4Ch),a
       ld de,0FFFFh
$$:    call DecreaseCounter
       jr z,die
       in a,(4Ch)
       cp 1Ah
       jr z,$F
       cp 5Ah
       jr nz,$B
$$:    ld a,0FFh
       out (87h),a
       xor a
       out (92h),a
       ld a,0Eh
       out (89h),a
       xor a
       ld (9C26h),a
       ld (9C27h),a
       ld a,5
       out (8Bh),a
       ;TODO: finish this...it's 7:30AM and I've been at this all night...
       ret
die:
ScrewWithUSBPorts:
       ret

HandleDefaultUSBInterrupt:
       di
       bit 3,(iy+41h)
       ret nz
       in a,(8Fh)
       bit 7,a
       jr nz,killUSBInterruptsError
       xor a
       out (5Bh),a
       ld a,20h
       out (57h),a
       ld a,(9C28h)
       cp 4
       jr nz,setUSBErrorStateAndStuff
       ld a,1
       ld (9C28h),a
       ld b,50
       call WaitTimerBms
       ld a,1
       out (5Bh),a
       res 0,(iy+41h)
       ret

killUSBInterruptsError:
       ld a,6
       ld (9C28h),a
       xor a
       out (5Bh),a
       jr setUSBErrorStateAndStuff

ReadUSBInterruptData:
       ret

DecreaseCounter:
       dec de
       ld a,d
       or e
       ret

WaitTimer20ms:
       ld b,2
WaitTimerBms:
       push hl
       ld a,42h
       out (36h),a
       xor a
       out (37h),a
       ld a,b
       out (38h),a
       ld hl,0
$$:    inc hl
       in a,(4)
       bit 7,a
       jr z,$B
       pop hl
       ret

killUSBscfRet:
       xor a
       out (5Bh),a
setUSBErrorStateAndStuff:
       in a,(4Dh)
       bit 5,a
       jr nz,$F
       xor a
       out (4Ch),a
       res 6,(iy+41h)
       jr continueFail
$$:    ld b,8
       in a,(4Dh)
       bit 6,a
       jr nz,$F
       ld b,0
$$:    ld a,b
       out (4Ch),a
continueFail:
       ld a,2
       out (54h),a
       in a,(39h)
       and 0F8h
       out (39h),a
       ld a,6
       ld (9C28h),a
       in a,(4Dh)
       bit 5,a
       jr nz,fail2
       ld de,0FFFFh
$$:    call DecreaseCounter
       jr z,failUSBGood
       in a,(4Dh)
       bit 7,a
       jr z,$B
       in a,(4Dh)
       bit 0,a
       jr z,$B
       ld a,22h
       out (57h),a
       scf
       ret
fail2: in a,(4Dh)
       bit 6,a
       jr nz,$F
       xor a
       out (4Ch),a
       ld a,50h
       jr fail3
$$:    ld a,93h
fail3: out (57h),a
       scf
       ret
failUSBGood:
       scf
       ei
       ld a,1
       out (5Bh),a
       res 0,(iy+41h)
       ret

