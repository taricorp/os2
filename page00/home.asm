 DEFINE PAGE00, SPACE=ROM
 SEGMENT PAGE00

 include "includes\os2.inc"

 PUBLIC homescreenContextVectors,InitHomescreenContext
 EXTERN DispHexA,DispHexHL,GetHexA,GetHexHL,AppInit,RestoreTextShadow

InitHomescreenContext:
       ld hl,8000h
       ld (editTop),hl
       inc hl
       ld (editCursor),hl
       inc hl
       ld (editTail),hl
       inc hl
       ld (editBtm),hl
       ld a,' '
       ld (curUnder),a
       ld hl,0
       ld (curRow),hl
       ld hl,homescreenContextVectors
       call AppInit
       call RestoreTextShadow
       set appTextSave,(iy+appFlags)
       set curAble,(iy+curFlags)
       set curOn,(iy+curFlags)
       ld hl,menuCurrent
       ld (hl),0
       ret

homescreenContextVectors:
       DW cxMainHandler
       DW DummyRet
       DW DummyRet
       DW DummyRet
       DW DummyRet
       DW DummyRet
       DB 00h

DummyRet:
       ret
cxMainHandler:
       ld hl,(curRow)
       push hl
       ld hl,7
       ld (curRow),hl
       call DispHexA
       pop hl
       ld (curRow),hl
       cp kGraph
       jr z,killOtherOS
       cp kWindow
       jr z,writeByte
       cp kYequ
       ret nz
       ld hl,(curRow)
       push hl
       in a,(6)
       push af
       call GetHexA
       call GetHexHL
       ld de,appData
       ld bc,32
       out (6),a
$$:    ld a,(hl)
       ld (de),a
       inc hl
       inc de
       dec bc
       ld a,b
       or c
       jr nz,$B
       ld de,2
       ld (curRow),de
       ld hl,appData
       ld b,32
dispLoop:
       ld a,(hl)
       call DispHexA
       inc hl
       djnz dispLoop
       pop af
       out (6),a
       pop de
       ld (curRow),de
       set curAble,(iy+curFlags)
       ret
killOtherOS:
       B_CALL UnlockFlash
       ld a,70h
       B_CALL EraseFlashPage
       ret
writeByte:
       B_CALL UnlockFlash
       ld hl,(curRow)
       push hl
       ld hl,0
       ld (curRow),hl
       call GetHexA
       push af
       call GetHexHL
       push hl
       call GetHexA
       ld b,a
       pop de
       pop af
       B_CALL WriteAByte
       pop hl
       ld (curRow),hl
       ret

