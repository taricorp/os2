 DEFINE PAGE00, SPACE=ROM
 SEGMENT PAGE00

 include "includes\os2.inc"

 PUBLIC ReadKeyboardKey,GetCSC,HandleLinkKeyActivity,ReadKeypad
 EXTERN NZIf83Plus

NoIGetCSC:
       call ReadKeyboardKey
       jr $F
GetCSC:call $F
       ei
       ret
$$:    ld hl,kbdScanCode
       di
       ld a,(hl)
       ld (hl),0
       res kbdSCR,(iy+kbdFlags)
       ret

HandleLinkKeyActivity:
       nop
       nop
       di
       push af
       call NZIf83Plus
       jr nz,eiHaltRet
       in a,(laStatusPort)
       and 11011001b ;in the middle of any transmissions?
       jr z,eiHaltRet
       bit 6,a
       call nz,setLinkAssistError
       pop af
       ei
       ret
eiHaltRet:
       pop af
       ei
       halt
       ret
$$:    pop af
       ei
       ret
setLinkAssistError:
       set linkAssistError,(iy+linkAssistStatusFlags)
       ret

ReadKeyboardKey:
;8440h-8443h are used to debounce up/down/left/right/DEL
       call ReadKeypad
       jr nc,$F
       ld a,0FFh
       ld (8441h),a
       ld a,5
       ld (8443h),a
       ret
$$:    ld hl,8441h
       cp (hl)
       ld hl,8443h
       jr z,$F
       ld (8441h),a
       ld (hl),5
$$:    or a
       jr nz,$F
       dec (hl)
       ret nz
       inc (hl)
$$:    ld hl,8440h
       cp (hl)
       jr nz,rkk1
       or a
       ret z
       cp 0F3h
       jr nc,$F
       cp 38h
       jr z,$F
       cp 5
       ret nc
$$:    ld hl,8442h
       dec (hl)
       ret nz
       ld (hl),0Ah
       jr setKbdKey
rkk1:  call setKbdKey
       or a
       jr z,$F
       set kbdKeyPress,(iy+kbdFlags)
$$:    ld (hl),a
       ld a,32h
       ld (8442h),a
       ret
setKbdKey:
       ld (kbdScanCode),a
       set kbdSCR,(iy+kbdFlags)
       or a
       ret z
       ld (kbdGetKy),a
       ret
ReadKeypad:
       sub a
       call ReadPort1
       xor 0FFh
       ret z
       ld hl,0
       ld c,0FEh
       ld d,1
rk1:   ld a,c
       call ReadPort1
rk3:   ld e,0
       ld b,8
rk2:   rla
       jr c,$F
       inc e
       ld l,b
$$:    djnz rk2
       ld a,e
       or a
       jr z,$F
       cp 2
       jr nc,scfRet
       ld a,h
       or a
       jr nz,scfRet
       ld h,d
$$:    inc d
       rlc c
       jr c,rk1
       ld a,h
       or a
       ret z
       dec a
       rla
       rla
       rla
       add a,l
       ret
scfRet:scf
       ret
ReadPort1:
       push af
       in a,(statusPort)
       and STATUS_NON_83P_MASK
       jr nz,rp1SE
rp1_1: pop af
       out (1),a
rp1_2: nop
       nop
       nop
       nop
       in a,(1)
       ld b,a
       ld a,0FFh
       out (1),a
       ld a,b
       ret
rp1SE: in a,(speedPort)
       and 1
       jr z,rp1_1
       pop af
       out (1),a
       nop
       nop
       nop
       jr rp1_2

