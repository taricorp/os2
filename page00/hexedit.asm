 DEFINE PAGE00, SPACE=ROM
 SEGMENT PAGE00

 include "includes\os2.inc"

 PUBLIC JForceHexEditor
 EXTERN PutS

JForceHexEditor:
       res curAble,(iy+curFlags)
       res curOn,(iy+curFlags)
       res appTextSave,(iy+appFlags)
       B_CALL ClrLCDFull
       ld hl,0
       ld (curRow),hl
       ld hl,sWelcome
       call PutS
       B_CALL GetKey
       B_JUMP JForceCmdNoChar
sWelcome:
       DB "Welcome to the  "
       DB "hex editor!",0

