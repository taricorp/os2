 DEFINE PAGE00, SPACE=ROM
 SEGMENT PAGE00

 PUBLIC _APP_PUSH_ERRORH,_APP_POP_ERRORH,PushRealO1,Mov9ToOP1,FPAdd,LCDDelay
 PUBLIC CopyFlashPage,ATimes16,CalculateOSChecksum,Is84P,NZIf84PlusSeries
 PUBLIC Placeholder005Fh,Placeholder0006h,Placeholder004Eh,Placeholder0003h,Placeholder0035h
 PUBLIC outputPage,NZIf83Plus,ZIfSlowSpeed,GetBytePaged,MakeOffPageCall,CopyToRAMPage
 PUBLIC CopyRAMToFlashPage,CanAlphIns,CanAlph,ResAlph,GetHexA,GetHexHL,SetFastSpeed,cphlde
 PUBLIC BCALL,BJUMP,JErrorNo,SetupPagedPtr,PagedGet,EraseRAMPage,SetContrast,MemClear
 EXTERN Page0Call,UpdateAPD,PowerOff,GetCSC,PutC,DispHexHL,PutMap,BCALLRoutine,OS2Marker

 include "includes\os2.inc"

PushRealO1:
Mov9ToOP1:
FPAdd:
Placeholder0003h:
Placeholder0006h:
Placeholder0035h:
Placeholder004Eh:
Placeholder005Fh:
       ret

cphlde:
       push hl
       or a
       sbc hl,de
       pop hl
       ret

ATimes16:
       add a,a
       add a,a
       add a,a
       add a,a
       ret

CalculateOSChecksum:
       push ix
       ld ix,userMem
       ld bc,0FE70h-userMem
       ld hl,0011h
       ld d,h
$$:    ld e,(ix+0)
       inc ix
       add hl,de
       dec bc
       ld a,b
       or c
       jr nz,$B
       ld e,d
       ex de,hl
       sbc hl,de
       pop ix
       ret

CopyFlashPage:
       ld hl,4000h
$$:    push af
       push bc
       ld de,tempSwapArea
       ld bc,128
       push hl
       ld ix,_FlashToRam2-4000h
       call BCALLRoutine
       pop de
       pop af
       push af
       ld hl,tempSwapArea
       ld bc,128
       push de
       ld ix,_WriteFlash-4000h
       call BCALLRoutine
       pop hl
       ld bc,128
       add hl,bc
       pop bc
       pop af
       bit 7,h
       jr z,$B
       ret


MemClear:
       ld a,b
       or c
       ret z
       push hl
       pop de
       inc de
       ld (hl),0
       dec bc
       ld a,b
       or c
       ret z
       ldir
       ret

SetContrast:
       ld a,(contrast)
       add a,18h
       or 0C0h
       call LCDDelay
       out (LCDinstPort),a
       ret

IGetKey:
       push de
       push hl
       push bc
       push ix
       B_CALL GetKey
       pop ix
       pop bc
       pop hl
       pop de
       ret

IPutC:
       push de
       push hl
       push bc
       push ix
       push af
       call PutC
       pop af
       pop ix
       pop bc
       pop hl
       pop de
       ret

GetHexHL:
       push af
       call GetHexA
       ld h,a
       call GetHexA
       ld l,a
       pop af
       ret

;lets user input an 8 bit number in hexadecimal
;prompt is at currow,curcol
;number is returned in a
TempNum              EQU    0FFFEh
GetHexA:
       push bc
       push de
       push hl
       push ix
       call GetHexA_sub
       pop ix
       pop hl
       pop de
       pop bc
       ret
GetHexA_sub:
       set curAble,(iy+curFlags)
       ld b,2
       ld hl,TempNum
getnumhloop:
       call IGetKey
       cp 2
       jp nz,gnhnotback
       ld a,b
       cp 2
       jp z,gnhnotback
       ld a,' '
       call PutMap
       ld hl,curCol
       dec (hl)
       jp GetHexA_sub
gnhnotback:
       sub 142
       cp 10
       jp c,gnhnumpressed
       sub 12
       cp 6
       jp c,gnhletpressed
       jp getnumhloop
gnhnumpressed:
       ld (hl),a
       inc hl
       add a,48
       call IPutC
       djnz getnumhloop
       jp gnhdone
gnhletpressed:
       add a,10
       ld (hl),a
       inc hl
       add a,55
       call IPutC
       djnz getnumhloop
gnhdone:
       dec hl
       ld b,(hl)
       dec hl
       ld a,(hl)
       rlca
       rlca
       rlca
       rlca
       or b
       res curAble,(iy+curFlags)
       ret

ResAlph:
       bit shiftALock,(iy+shiftFlags)
       ret nz
       jr $F
CanAlphIns:
       res textInsMode,(iy+textFlags)
CanAlph:
       res shiftALock,(iy+shiftFlags)
$$:    bit shiftKeepAlph,(iy+shiftFlags)
       ret nz
       res shiftAlpha,(iy+shiftFlags)
       ret

CopyToRAMPage:
       di
       out (7),a
       ld a,b
       out (5),a
       ld hl,8000h
       ld de,0C000h
       ld bc,4000h
       ldir
       ld a,81h
       out (7),a
       xor a
       out (5),a
       ret

CopyRAMToFlashPage:
       di
       ld hl,4000h
$$:    push af
       push bc
       push hl
       ;Copy from extra RAM page A to Flash page B
       out (5),a
       ld de,8000h
       add hl,de
       ld de,tempSwapArea
       ld bc,128
       ldir
       xor a
       out (5),a
       pop de
       pop af
       push af
       ld hl,tempSwapArea
       ld bc,128
       push de
       B_CALL WriteFlash
       pop hl
       ld bc,128
       add hl,bc
       pop bc
       pop af
       bit 7,h
       jr z,$B
       ret

EraseRAMPage:
       di
       out (7),a
       ld hl,8000h
       ld (hl),0
       ld de,8001h
       ld bc,4000h-1
       ldir
       ld a,81h
       out (7),a
       ret

LCDDelay:
       push af
       call NZIf83Plus
       jr nz,LCDDelayDone
$$:    in a,(statusPort)
       and 2
       jr z,$B
LCDDelayDone:
       pop af
       ret

SetupPagedPtr:
       push af
       ld a,b
       ld (pagedPN),a
       ld (pagedGetPtr),de
       xor a
       ld (pagedCount),a
       pop af
       ret

PagedGet:
       push hl
PagedGet_1:
       ld a,(pagedCount)
       or a
       jr z,$F
       dec a
       ld (pagedCount),a
       ld hl,(pagedPutPtr)
       ld a,(hl)
       inc hl
       ld (pagedPutPtr),hl
       pop hl
       ret
$$:    push bc
       push de
       in a,(memPageAPort)
       push af
       ld a,(pagedPN)
       out (6),a
       ld bc,16
       ld a,c
       ld (pagedCount),a
       ld de,pagedBuf
       ld (pagedPutPtr),de
       ld hl,(pagedGetPtr)
       bit 7,h
       jr nz,getAllRAM
getPagedByte:
       ldi
       bit 7,h
       jr z,$F
       in a,(memPageAPort)
       inc a
       ld (pagedPN),a
       out (memPageAPort),a
       ld hl,4000h
$$:    ld a,b
       or c
       jr nz,getPagedByte
       jr $F
getAllRAM:
       ldir
$$:    ld (pagedGetPtr),hl
       pop af
       out (memPageAPort),a
       pop de
       pop bc
       jr PagedGet_1

_APP_PUSH_ERRORH:
       pop de
       push hl
       ld b,a
       in a,(6)
       push af
       ld a,b
       ld hl,(errSP)
       push hl
       ld bc,(fpBase)
       ld hl,(FPS)
       or a
       sbc hl,bc
       push hl
       ld bc,(OPBase)
       ld hl,(OPS)
       sbc hl,bc
       push hl
       ld (errSP),sp
       ex de,hl
       jp (hl)

_APP_POP_ERRORH:
       pop bc
       ld sp,(errSP)
       pop af
       pop af
       ex (sp),hl
       ld (errSP),hl
       pop hl
       pop af
       pop af
       push bc
       ret

BCALL:
       push hl
       push hl
       push hl
       push af
       push bc
       push de
       push hl
       ld hl,15
       add hl,sp
       ld d,(hl)
       dec hl
       ld e,(hl)
       inc de
       inc de
       ld (hl),e
       inc hl
       ld (hl),d
       in a,(memPageAPort)
       ld c,a
       dec hl
       dec hl
       dec hl
       ld (hl),a
       dec hl
       ;HACK: until I can figure out how to shift bits of addresses in ZDS...
       ld (hl),0
       dec hl
       ld (hl),75h
       dec de
       ex de,hl
       ld d,(hl)
       dec hl
       ld e,(hl)
       ld b,0
       bit 7,d
       jr nz,bootCodeCall
       bit 6,d
       jr z,multipageAppCall
       res 6,d
       ld a,(OS2Marker)
       or a
       ld a,7Dh
       jr z,$F
       ld a,7Bh
       jr $F
bootCodeCall:
       res 7,d
       ld a,7Fh
       jr $F
multipageAppCall:
       call getBasePage
$$:    call outputPage
       ld hl,4000h
       add hl,de
       ld e,l
       ld d,h
       ld hl,8
       add hl,sp
       ld a,(de)
       ld (hl),a
       inc de
       inc hl
       ld a,(de)
       ld (hl),a
       inc de
       ld a,b
       or a
       jr z,$F
       ld a,(de)
       neg
       add a,b
       jr BCALL_done
$$:    ld a,(de)
       or a
       jr nz,BCALL_done
       ld a,c
BCALL_done:
       call outputPage
       pop hl
       pop de
       pop bc
       pop af
       ret
getBasePage:
       ;TODO: come back to this...
       ret
       
BJUMP:
       ex (sp),hl
       push af
       push bc
       push de
       in a,(6)
       ld c,a
       ld e,(hl)
       inc hl
       ld d,(hl)
       ld b,0
       bit 7,d
       jr nz,bootCodeJump
       bit 6,d
       jr z,multipageAppJump
       res 6,d
       ld a,7Dh
       jr $F
bootCodeJump:
       res 7,d
       ld a,7Fh
       jr $F
multipageAppJump:
       call getBasePage
$$:    call outputPage
       ld hl,4000h
       add hl,de
       ld e,(hl)
       inc hl
       ld d,(hl)
       inc hl
       ld a,b
       or a
       ld a,(hl)
       jr z,$F
       neg
$$:    add a,b
       jr nz,$F
       ld a,c
$$:    call outputPage
       ex de,hl
       pop de
       pop bc
       pop af
       ex (sp),hl
       ret

JErrorNo:
       ld (errNo),a
       call NZIf83Plus
       jr z,$F
       ld a,0C0h
       out (bport),a
       jr JErrorNo_1
$$:    ld a,80h
       out (laEnPort),a
       res linkAssistEnabled,(iy+linkAssistFlags)
JErrorNo_1:
       res indicOnly,(iy+indicFlags)
       ;res 4,(iy+24h)
       ld sp,(errSP)
       ld hl,(OPBase)
       pop de
       add hl,de
       ld (OPS),hl
       pop de
       ld hl,(fpBase)
       add hl,de
       ld (FPS),hl
       pop hl
       ld (errSP),hl
       pop af
       out (6),a
       ld a,(errNo)
       ret
       
ZIfSlowSpeed:
       jr z,ForceSlowSpeed
SetFastSpeed:
       push af
       call NZIf83Plus
       jr nz,$F
       ld a,1
       out (speedPort),a
$$:    pop af
       ret
ForceSlowSpeed:
       call NZIf83Plus
       ret nz
       push af
       xor a
       out (speedPort),a
       pop af
       ret

MakeOffPageCall:
       in a,(memPageAPort)
       push af
       ld a,(offPageCallPage)
       out (memPageAPort),a
       ld hl,P1OutputRet
       push hl
       ld hl,(offPageCallAddress)
       jp (hl)
P1OutputRet:
       pop af
outMemPageAPort:
       out (memPageAPort),a
       ret

GetBytePaged:
       or a
       ret z
       ld b,a
       in a,(memPageAPort)
       ld c,a
       ld a,b
       out (memPageAPort),a
       ld b,(hl)
       ld a,c
       jr outMemPageAPort

outputPage:
       call translatePage
       out (memPageAPort),a
       ret
translatePage:
       call NZIf83Plus
       jr z,$F
       and 1Fh
       ret
$$:    call Is84P
       ret nz
       and 3Fh
       ret

Is84P:
       push bc
       push af
       in a,(modelPort)
       and 3
       jr $F
NZIf83Plus:
       push bc
       push af
       in a,(statusPort)
       and STATUS_NON_83P_MASK
       xor STATUS_NON_83P_MASK
$$:    pop bc
       ld a,b
       pop bc
       ret

NZIf84PlusSeries:
       push bc
       push af
       in a,(statusPort)
       and STATUS_84P_SERIES_MASK
       pop bc
       ld a,b
       pop bc
       ret

