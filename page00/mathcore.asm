 DEFINE PAGE00, SPACE=ROM
 SEGMENT PAGE00

 include "includes\os2.inc"

 PUBLIC OP1ToOP4,OP1ToOP2,MovToOP1,EquToOP1,MovFrOP1,Mov8B

OP1ToOP4:
       ld hl,OP1
       ld de,OP4
       jr Mov11B
Mov11B:ldi
Mov10B:ldi
Mov9B: ldi
Mov8B: ldi
Mov7B: ldi
       ldi
       ldi
       ldi
       ldi
       ldi
       ldi
       ret

OP1ToOP2:
       ld hl,OP1
       ld de,OP2
       jr Mov11B

MovToOP1:
       ld de,OP1
       jr Mov11B

EquToOP1:
       ld a,EquObj
HLToOP1A:
       ld (OP1),a
HLToOP1:
       ld (OP1+1),hl
       xor a
       ld (OP1+3),a
       ret

MovFrOP1:
       ld hl,OP1
       jr Mov11B

