 DEFINE PAGE00, SPACE=ROM
 SEGMENT PAGE00

 include "includes\os2.inc"

 PUBLIC CheckLowBatteriesTurnOff,_ZERO_PORT_3,_OUT_PORT_3,PowerOff,UpdateAPD,resetAPDTimer
 EXTERN SaveOScreen,LCDDelay,CalculateOSChecksum

PowerOff:
       ld a,INTERRUPT_MASK_POWER
       call _OUT_PORT_3
       res monAbandon,(iy+monFlags) ;don't start any long putaway process (this will be more important later)
       res shift2nd,(iy+shiftFlags)
       res onRunning,(iy+onFlags)
       ld a,lcdTurnOff
       call LCDDelay
       out (LCDinstPort),a
       call CalculateOSChecksum
       ld (RAMChecksum),hl
       ld a,STATE_MODE0_DEFAULT
       out (interruptStatusPort),a
       ld a,INTERRUPT_MASK_LINK+INTERRUPT_MASK_ON
       out (interruptEnPort),a
       ex af,af'
       exx
       ei
$$:    halt
       jr $B

UpdateAPD:
       ld a,(flags+apdFlags)
       bit apdAble,a
       ret z
       bit apdRunning,a
       ret z
       ld hl,apdSubTimer
       dec (hl)
       ret nz
       inc hl
       dec (hl)
       ret nz
       call SaveOScreen
       res apdRunning,(iy+apdFlags)
       set apdWarmStart,(iy+apdFlags)
       jr PowerOff
resetAPDTimer:
       ld hl,apdTimer
       ld (hl),74h ;hard-coded default value, about 5 minutes
       ret

_ZERO_PORT_3:
       sub a
_OUT_PORT_3:
       push af
       ld a,INTERRUPT_MASK_POWER
       out (interruptEnPort),a
       pop af
       out (interruptEnPort),a
       sub a
       out (31h),a
       out (34h),a
       out (37h),a
       ret

CheckLowBatteriesTurnOff:
       ld a,STATE_MODE0_DEFAULT
       out (interruptStatusPort),a
       bit batteriesGood,(iy+interruptFlags)
       ret nz
       xor a
       out (57h),a
       out (5Bh),a
       in a,(54h)
       bit 1,a
       jr nz,$F
       ;ViewScreen attached? Maybe? Not sure.
       ld a,2
       out (54h),a
       xor a
       out (4Ch),a
$$:    pop hl
       pop hl
       ld a,lcdTurnOff             ;turn off the LCD
       out (LCDinstPort),a
       res enableHW2Timer,(iy+interruptFlags)
       ld hl,0A55Ah                ;set special checksum for power on from forced turnoff
       ld (RAMChecksum),hl
       ld a,INTERRUPT_MASK_ON      ;this is putting the calculator in low power mode
       out (interruptEnPort),a
       res shift2nd,(iy+shiftFlags)
       res onRunning,(iy+onFlags)
       res turnOffIfLowBatteriesFound,(iy+batteryCheckFlags)
       ld a,STATE_MODE0_DEFAULT
       out (interruptStatusPort),a
       xor a
       out (memPageBPort),a
       ei
$$:    halt
       jr $B

