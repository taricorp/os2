 DEFINE PAGE00, SPACE=ROM
 SEGMENT PAGE00

 include "includes\os2.inc"

 PUBLIC _PULLDOWNCHK,clrTR
 EXTERN savepartialWin,rstrpartialWin,RestoreTextShadow,PutS,LdHLInd,PutPS,EraseEOL,PutC
 EXTERN _JForceHexEditor

_PULLDOWNCHK:
;Key code is in A. Check to see if it's a menu key and back out if not, otherwise handle it.
;Return carry flag set if you want the system monitor to restart, otherwise the current cxMain handler will get called for this key.
;In other words return carry flag set if you're handling this keypress.
       cp kInputDone
       jr nz,$F
skipKey:
       or a
       ret
$$:    call IsSystemMenuKey
       jr c,HandleSystemMenuKey
       call IsInCatalogMenu
       jr z,skipKey
       call IsInMenu
       jr nz,HandleMenuOpenKey
MenuCatCommon:
       cp appStart
       jr c,skipKey
;       cp echoStart1
;       call c,RestoreOldContext
       cp kQuit
       jr z,skipKey
       or a
       ret
HandleSystemMenuKey:
;       call IsInMenu
;       jr nz,keyHandled
       ld c,(iy+menuFlags)
       res useCustomDisplay,(iy+menuFlags)
       cp kExtendEcho
       jr nz,$F
       ld b,a
       ld a,(keyExtend)
       cp k2Tools
       ld a,b
       jr z,SetupMenuCustom
       jr skipKey
$$:    cp kAppsMenu
       jr z,SetupMenu
skipSystemKey:
       ld (iy+menuFlags),c
       or a
       ret
       xor a
SetupMenuCustom:
       set useCustomDisplay,(iy+menuFlags)
SetupMenu:
       push af
       ld a,(flags+curFlags)
       ld (curFlagsBackup),a
       res appTextSave,(iy+appFlags)
       res curAble,(iy+curFlags)
       res curOn,(iy+curFlags)
       ld a,(menuCurrent)
       or a
       call z,savepartialWin
       pop af
       cp kExtendEcho
       jr nz,$F
       ld a,(keyExtend)
       jr hsmk1
$$:    sub 2Ah ;menuStart-1
hsmk1: ld (menuCurrent),a
       ld hl,0
       ld (menuCurrent+1),hl
       B_CALL ClrLCDFull
       ld hl,0
       ld (curRow),hl
       call GetMenuPointer
       ld a,(hl)
       ld (numHeaders),a
       ld a,(menuCurrent+1)
       ld b,a
       inc b
$$:    inc hl
       djnz $B
       ld a,(hl)
       ld (numItemsForThisHeader),a
       xor a
       ld (menuCurrent+2),a
       call DisplayMenu
keyHandled:
       scf
       ret
HandleMenuOpenKey:
       cp kLeft
       jr nz,notLeft
       call DisplayMenu
       jr keyHandled
notLeft:
       cp kRight
       jr nz,notRight
       call DisplayMenu
       jr keyHandled
notRight:
       cp kUp
       jr nz,notUp
       call DisplayMenu
       jr keyHandled
notUp: cp kDown
       jr nz,notDown
       
       call DisplayMenu
       jr keyHandled
notDown:
       cp kAlphaDown
       jr nz,notAlphaDown
       jr notAlphaDown
       ld a,(numItemsForThisHeader)
       ld hl,menuCurrent+2
       sub (hl)
       ld hl,curRow
       add a,(hl)
       cp 9
       jr c,$F
       ld a,(menuCurrent+2)
       ld hl,curRow
       sub (hl)
       add a,7
       jr RedrawAlphaMenu
$$:    ld a,6
       ld (curOffset),a
       ;TODO: finish this...
RedrawAlphaMenu:
       ld (menuCurrent+2),a
;       call 
notAlphaDown:
       cp kEnter
       jr nz,notEnter
       ld a,(menuCurrent+2)
       call ExecuteSelected
       jr keyHandled
ExecuteSelected:
       ld hl,numItemsForThisHeader
       cp (hl)
       ret nc
       push af
       call GetCurrentMenuPointer
       pop bc
       ld a,b
       inc hl
       or a
       jr z,$F
enterLoop:
       inc hl
       inc hl
       djnz enterLoop
$$:    bit useCustomDisplay,(iy+menuFlags)
       jr z,$F
       ld d,(hl)
       dec hl
       ld e,(hl)
       ex de,hl
       ld c,(hl)
       ld b,0
       add hl,bc
       inc hl
       push hl
       set appTextSave,(iy+appFlags)
       ld hl,menuCurrent
       ld (hl),0
       pop hl
       jp (hl)
$$:    ld b,(hl)
       dec hl
       ld a,(hl)
       ld (keyExtend),a
       or a
       ld a,b
       jr z,$F
       ld a,(keyExtend)
       ld c,a
       ld a,b
       ld (keyExtend),a
       ld a,c
       cp 83h
       jr nz,$F
       ld a,kExtendEcho
       jr HandleSystemMenuKey
$$:    push af
       call RestoreTextShadow
       ld a,(curFlagsBackup)
       ld (flags+curFlags),a
       set appTextSave,(iy+appFlags)
       ld hl,menuCurrent
       ld (hl),0
       pop af
       or a
       ret
notEnter:
       cp kClear
       jr nz,maybeNumberKey
       call RestoreTextShadow
       ld a,(curFlagsBackup)
       ld (flags+curFlags),a
       set appTextSave,(iy+appFlags)
       ld hl,menuCurrent
       ld (hl),0
       jr keyHandled
maybeNumberKey:
       ld b,8Fh
       cp 8Fh
       jr c,$F
       cp 98h
       jr c,mnk1
$$:    ld b,85h
       cp 8Eh
       jr z,mnk1
       ld b,90h
       cp 9Ah
       jr c,keyHandled ;not exactly right
       cp 0B4h
       jr c,mnk1
       ld b,0A8h
       cp 0CCh
       jr nz,keyHandled ;not exactly right
mnk1:  sub b
       call ExecuteSelected
       jr keyHandled
IsSystemMenuKey:
;HACK: until we get all the menus between menuStart and menuEnd implemented, we have to hard-code
;  which menu keys we support, which for now is just APPS.
       cp kAppsMenu
       scf
       ret z
       or a
       ret
       cp menuEnd+1
       ret nc
       cp menuStart
       ccf
       ret
DisplayMenu:
       ld a,(menuCurrent+2)
DisplayMenu_1:
       ld c,a
       push bc
       ld a,1
       ld (topMenuRow),a
       B_CALL ClrLCDFull
       ld hl,0
       ld (curRow),hl
       call EraseEOL
       call DisplayMenuHeader
       call GetCurrentMenuPointer
       pop bc
       ld a,(numItemsForThisHeader)
       or a
       jr z,noItems
       ld a,c
       sla a
       ld e,a
       ld d,0
       add hl,de
       call DispMenuChoices
       ld a,(topMenuRow)
       jr $F
noItems:
       ld a,1
$$:    ld (curRow),a
       ret
DispMenuChoices:
       ld a,(topMenuRow)
DispMenuChoicesLoop:
       ld (curRow),a
       call DisplayCurMenuNumColonArrow
       ld d,(hl)
       inc hl
       ld e,(hl)
       inc hl
       push hl
       push bc
       call DisplayMenuItem
       pop bc
       pop hl
       inc c
       ld a,(numItemsForThisHeader)
       cp c
       ret z
       ret c
       ld a,(curRow)
       cp 7
       ret z
       inc a
       jr DispMenuChoicesLoop
DisplayMenuItem:
       bit useCustomDisplay,(iy+menuFlags)
       jr z,$F
       ld h,e
       ld l,d
       jr dmiDisplay
$$:    B_CALL KeyToString
dmiDisplay:
       call PutPS
       ld a,(curCol)
       cp 15
       call c,EraseEOL
       ret
DisplayCurMenuNumColonArrow:
       xor a
       ld (curCol),a
       ld a,(menuCurrent+2)
       cp c
       jr nz,$F
DisplayMenuNumColonArow:
       set textInverse,(iy+textFlags)
$$:    ld b,'1'
       ld a,c
       ld e,a
       cp 9
       jr c,dispLessThan9
       jr nz,dispHigherThan10
       ld a,'0'
       jr displayMenuIndexChar
dispSpace:
       ld a,' '
       jr displayMenuIndexChar
dispTheta:
       ld a,5Bh
       jr displayMenuIndexChar
dispHigherThan10:
       ld a,(menuCurrent)
       cp 3
       jr z,dispSpace
       cp 28h
       jr z,dispSpace
       cp 2
       jr z,dispSpace
       cp 16
       jr nz,$F
       ld a,(menuCurrent+1)
       or a
       jr z,dispSpace
       jr notProgEdit
$$:    cp 2Ah
       jr nz,notProgEdit
       ld a,(menuCurrent+1)
       cp 2
       jr z,dispSpace
notProgEdit:
       ld a,e
       ld b,'7'
       cp 36
       jr z,dispTheta
       jr nc,dispSpace
dispLessThan9:
       add a,b
displayMenuIndexChar:
       call PutC
       push bc
       ld a,(topMenuRow)
       ld b,a
       ld a,(curRow)
       cp b
       pop bc
       jr nz,$F
       ld a,e
       or a
       jr z,dispColon
       ld a,1Eh
       jr dispArrowColon
$$:    cp 7
       jr nz,dispColon
       ld a,(numItemsForThisHeader)
       dec a
       cp e
       ld a,1Fh
       jr z,dispColon
       jr nc,dispArrowColon
dispColon:
       ld a,':'
dispArrowColon:
       call PutC
       res textInverse,(iy+textFlags)
       ret
GetCurrentMenuPointer:
       call GetMenuPointer
       ld a,(menuCurrent+1)
       or a
       ld b,a
       ld a,(hl)
       ld c,a
       jr z,$F
       push hl
gcmpLoop:
       inc hl
       add a,(hl)
       djnz gcmpLoop
       pop hl
$$:    sla a
       sub c
       add a,(hl)
       inc hl
       ld e,a
       ld d,0
       add hl,de
       ret
DisplayMenuHeader:
       call GetMenuPointer
       ld a,(hl)
       ld b,a
       inc hl
       inc hl
       ld a,(hl)
       ;A is the string index for the header
       set textInverse,(iy+textFlags)
       call GetBuiltInString
       call PutPS
       res textInverse,(iy+textFlags)
       ret
GetMenuPointer:
       ld a,(menuCurrent)
       ld h,0
       ld l,a
       add hl,hl
       ld de,menuTables-2
       add hl,de
       jr LdHLInd
GetBuiltInString:
       ld h,0
       ld l,a
       add hl,hl
       ld de,ptrStrings-2
       add hl,de
       jr LdHLInd
menuTables:
       DW 0000h
       DW mntApps
       DW mntPrograms
       DW mntTools
;Each table has the following:
;      DB bNumberOfHeaders
;      DB bNumberOfChoices1
;      DB bHeaderStringIndex
;      DB keyExtend1,keyCode1
;Custom menu entries also hvae:
;      DW wJumpAddress
mntPrograms:
mntApps:
       DB 1
       DB 1
       DB 01h
       DB 83h,k2Tools
mntTools:
       DB 1
       DB 1
       DB 02h
       DW HexEditor
ptrStrings:
       DW sApplications
       DW sTools
sApplications:
       DB sTools-$F
$$:    DB "APPLICATIONS"
sTools:
       DB sEnd-$F
$$:    DB "TOOLS"
sEnd:

HexEditor:
       DB $F-HexEditor-1
       DB "Hex Editor"
$$:    call _JForceHexEditor
   
clrTR:
       cp kCatalog
       jr z,clrTRCatalogPressed
       call IsInCatalogMenu
       jr z,DoCatalogKeyPress
       jr MenuCatCommon
clrTRCatalogPressed:
DoCatalogKeyPress:
       scf
       ret
IsInCatalogMenu:
       ld b,a
       ld a,(menuCurrent)
       cp mCatalog
       ld a,b
       ret
IsInMenu:
       ld b,a
       ld a,(menuCurrent)
       or a
       ld a,b
       ret

