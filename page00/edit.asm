 DEFINE PAGE00, SPACE=ROM
 SEGMENT PAGE00

 include "includes\os2.inc"

 PUBLIC IsAtEditTail,IsAtBtm

IsAtEditTail:
       ld hl,(editCursor)
       ld de,(editTail)
cphlde:push hl
       or a
       sbc hl,de
       pop hl
       ret

IsAtBtm:
       ld de,(editTail)
       ld hl,(editBtm)
       jr cphlde

