 DEFINE PAGE00, SPACE=ROM
 SEGMENT PAGE00

 include "includes\os2.inc"

 PUBLIC ResetStacks,FindSym
 EXTERN JErrorNo,OP1ToOP4,EquToOP1,MovToOP1,MovFrOP1

ResetStacks:
       ld hl,(OPBase)
       ld (OPS),hl
       ld hl,(fpBase)
       ld (FPS),hl
       ld hl,(onSP)
       ld (errSP),hl
       ret

InsertMem:
       call InsertMem_1
       push de
       dec de
       call InsertMem_2
       pop de
       ret
InsertMem_1:
       push de
       push hl
       ld hl,(FPS)
       or a
       sbc hl,de
       jr z,$F
       ld b,h
       ld c,l
       add hl,de
       dec hl
       pop de
       push de
       add hl,de
       ex de,hl
       ld hl,(FPS)
       dec hl
       lddr
$$:    pop bc
       pop de
       ret
       
FINDPROGSYM:
       ld a,ProgObj
$$:    call FINDVARSYM
       ret
chkFindSym:
       call IsOP1NamedType
       jr z,$B
FindSym:
       call ISO1NONTEMPLST
       jr z,isNonTempList
       ld de,(progPtr)
       ld a,(OP1+1)
       cp 24h
       jr nz,$F
       ld hl,(pTemp)
       ld de,(OPBase)
       jr FindSym_1
$$:    ld hl,symTable
FindSym_1:
       inc de
       xor a
       ld b,a
FindSym_2:
       ld a,(hl)
       call ToVarTypeByte
       and 1Fh
       sbc hl,de
       ret c
       add hl,de
       ld a,(OP1+1)
       cp (hl)
       jr z,$F
FindSym_3:
       ld c,3
       or a
       sbc hl,bc
       jr FindSym_2
$$:    push hl
       dec hl
       ld a,(OP1+2)
       cp (hl)
       jr nz,FindSym_4
       dec hl
       ld a,(OP1+3)
       cp (hl)
       jr nz,FindSym_4
       pop hl
       inc hl
       ld b,(hl)
       inc hl
       ld d,(hl)
       inc hl
       ld e,(hl)
       inc hl
       inc hl
       inc hl
       ld a,(hl)
       ld (OP1),a
       ret
FindSym_4:
       pop hl
       ld a,(OP1+1)
       jr FindSym_3
isNonTempList:
       ld a,1
       push af
       call CMPPRGNAMELEN
       inc a
       jr $F
FINDVARSYM:
       push af
       call CMPPRGNAMELEN
$$:    ld de,(pTemp)
       ld hl,(progPtr)
       pop bc
       ld c,a
       push bc
       inc de
       xor a
       ld b,a
FINDVARSYM_Loop:
       ld a,(hl)
       call ToVarTypeByte
       and 1Fh
       sbc hl,de
       jr c,FINDVARSYM_P1ret
       add hl,de
       pop bc
       push bc
       cp AppVarObj
       jr nz,$F
       cp b
       jr nz,FINDVARSYM_1
       jr FINDVARSYM_2
$$:    cp GroupObj
       jr nz,$F
       cp b
       jr nz,FINDVARSYM_1
       jr FINDVARSYM_2
$$:    ld a,b
       cp AppVarObj
       jr z,FINDVARSYM_1
       cp GroupObj
       jr z,FINDVARSYM_1
FINDVARSYM_2:
       ld a,c
       ld c,(hl)
       inc c
       cp (hl)
       jr nz,FINDVARSYM_3
       ld b,a
       call ISO1NONTEMPLST
       jr nz,$F
       dec b
$$:    push de
       push hl
       ld de,OP1
$$:    dec hl
       inc de
       ld a,(de)
       cp (hl)
       jr nz,FINDVARSYM_4
       djnz $B
       dec hl
       ld a,(hl)
       ld (OP1+9),a
       pop hl
       pop de
       push hl
       inc hl
       inc hl
       inc hl
       inc hl
       inc hl
       bit 6,(hl)
       pop hl
       jr z,$F
       bit 0,(iy+40h)
       jr nz,FINDVARSYM_5
       jr FINDVARSYM_1
$$:    bit 0,(iy+40h)
       jr nz,FINDVARSYM_1
FINDVARSYM_5:
       xor a
       pop bc
       jr FINDVARSYM_6
FINDVARSYM_4:
       pop hl
       pop de
       jr FINDVARSYM_3
FINDVARSYM_1:
       ld c,(hl)
       inc c
FINDVARSYM_3:
       xor a
       ld b,a
       sbc hl,bc
       jr FINDVARSYM_Loop
FINDVARSYM_P1ret:
       pop de
       ret
FINDVARSYM_6:
       inc hl
       ld b,(hl)
       inc hl
       ld d,(hl)
       inc hl
       ld e,(hl)
       inc hl
       inc hl
       inc hl
       ld a,(hl)
       ld (OP1),a
       ret
ToVarTypeByte:
       dec hl
       dec hl
       dec hl
       dec hl
       dec hl
       dec hl
       ret
CMPPRGNAMELEN:
;Gets the length of the program name in OP1
       push hl
       ld hl,OP1+1
       ld a,(hl)
       sub 5Dh
       ld d,a
       ld bc,8
       xor a
       cpir
       pop hl
       ld a,8
       ret nz
       inc c
       sub c
       cp 1
       ret nz
       ld e,a
       ld a,d
       or a
       ld a,e
       ret nz
       inc a
       ret
ISO1NONTEMPLST:
       ld a,(OP1+1)
       cp 5Dh
       ret
IsOP1NamedType:
       ld a,(OP1)
       and 1Fh
IsANamedType:
       cp AppVarObj
       ret z
       cp GroupObj
       ret z
IsAProgType:
       cp ProgObj
       ret z
       cp TempProgObj
       ret z
       cp ProtProgObj
       ret
IsOP1ProgType:
       ld a,(OP1)
       and 1Fh
       jr IsAProgType

createVar:
       push hl
       call CreateVarEntries
       pop bc
       ex de,hl
       ld (hl),c
       inc hl
       ld (hl),b
       dec hl
       ex de,hl
       ret
CreateVarEntries:
       ld de,2
       add hl,de
       jr c,jpErrMemory
       ld (OP1),a
       call IsANamedType
       jr z,$F
       ld a,(OP1+1)
       cp 5Dh
       jr nz,createVarNotList
       call CMPPRGNAMELEN
       cp 7
       jr nc,jpErrMemory
       inc a
       jr createVar_1
$$:    call CMPPRGNAMELEN
createVar_1:
       push af
       call EnoughMemPlusVAT
       jr c,jpErrMemory
CREATEPVAR3:
       or 1
       call AdjustDataPointers
       ld b,0
       push bc
       push de
       call OP1ToOP4
       ld de,OP3-8
       ld hl,OP1+8
       ld bc,8
       lddr
       pop de
       ld (OP1+3),de
       pop bc
       ld a,b
       ld (OP3-6),a
       xor a
       ld (OP1+2),a
       ld (OP1+1),a
       pop af
       ld (OP1+6),a
       add a,7
       ld c,a
       call AdjustSymPointers
createVar_2:
       ld de,(OP1+3)
       ld a,(OP3-6)
       ld b,a
       ret
createVarNotList:
       call EnoughMemPlusVAT
       jr c,jpErrMemory
CREATEVAR3:
       ld a,(OP1+1)
       cp 24h
       call AdjustDataPointers
       ld b,0
       push bc
       push de
       call OP1ToOP4
       ld de,OP1+8
       ld hl,OP1+3
       ld bc,3
       lddr
       pop de
       ld (OP1+3),de
       pop bc
       ld a,b
       ld (OP1+5),a
       xor a
       ld (OP1+1),a
       ld (OP1+2),a
       ld c,9
       call AdjustSymPointers
       jr createVar_2
jpErrMemory:
       jp JErrorNo
EnoughMemPlusVAT:
       push hl
       call GetSymEntryLengthToBC
       add hl,bc
       call EnoughMem
       pop bc
       ret
GetSymEntryLengthToBC:
       add a,7
       ld b,0
       ld c,a
       call IsOP1NamedType
       ret z
       ld a,(OP1+1)
       cp 5Dh
       ret z
       ld c,9
       ret
EnoughMem:
       ex de,hl
EnoughMem_1:
       call MemChk
       or a
       sbc hl,de
       ret nc
       push de
       ld hl,(pTemp)
       ld bc,(OPBase)
       inc bc
       ld d,0
EnoughMem_2:
       xor a
       sbc hl,bc
       jr c,EnoughMem_3
       add hl,bc
       bit 7,(hl)
       jr z,$F
       dec hl
       dec hl
       dec hl
       ld e,(hl)
       dec hl
       ld d,(hl)
       dec hl
       ld b,(hl)
       inc hl
       inc hl
       inc hl
       inc hl
       inc hl
       call DelVar
       pop de
       jr EnoughMem_1
$$:    ld e,9
       sbc hl,de
       jr EnoughMem_2
EnoughMem_3:
       pop de
       ret
MemChk:
       ld hl,(OPS)
       ld bc,(FPS)
       or a
       sbc hl,bc
       jr nc,$F
       ld hl,0
       ret
$$:    inc hl
       ret

DelVar:
;TODO: come back to this...
       ld a,b
       or a
       jr nz,jpErrArchived
       ld a,(hl)
       bit 6,a
       call nz,SetGraphTableDirty
       push af
       ld bc,9
       push de
       call IsFixedName
       jr nz,DelVar_1
       ld a,(hl)
       and 1Fh
       call IsList
       jr nz,DelVar_2
       pop de
       push hl
       call DelVar_3
       pop hl
       or a
       jr z,$F
       pop bc
       push af
       call DelVar_4
       ld de,9315h
       call MovFrOP1
       pop hl
       ld l,3Fh
       call EquToOP1
       rst rFINDSYM
       call nc,DelVar
       ld a,(OP1+2)
       call DelVar_5
       cpl
       and (hl)
       ld (hl),a
       ld hl,9315h
       jr MovToOP1
DelVar_4:
       push bc
$$:    push de
DelVar_2:
       ld b,0
       push hl
       call ToVarTypeByte
       ld a,(hl)
       add a,7
       ld c,a
       pop hl
DelVar_1:
       call DelMemBC
       pop hl
       pop af
       call DataSize
DelMem:
       call DelMem_1
AdjustMemPointers:
       call AdjustData_8
       ld hl,(fpBase)
       or a
       sbc hl,bc
       ld (fpBase),hl
       ld hl,(FPS)
       or a
       sbc hl,bc
       ld (FPS),hl
       call AdjustBasicPointers
       ld hl,(tempMem)
       or a
       sbc hl,de
       jr c,$F
       jr z,$F
       add hl,de
       or a
       sbc hl,bc
       ld (tempMem),hl
       jr DelMem_2
$$:    ld hl,(pTemp)
       or a
       jr DelMem_3
InsertMem_2:
       call NegateBC
       jr AdjustMemPointers
DelMem_2:
       ld hl,symTable
DelMem_3:
       ld (freeRAM),bc
       dec hl
       dec hl
       dec hl
DelMem_Loop:
       push af
       ld c,(hl)
       dec hl
       ld b,(hl)
       dec hl
       ld a,(hl)
       inc hl
       or a
       jr nz,DelMem_4
       ex de,hl
       or a
       sbc hl,bc
       jr nc,$F
       add hl,bc
       push hl
       ld h,b
       ld l,c
       ld bc,(freeRAM)
       or a
       sbc hl,bc
       ex de,hl
       ld (hl),d
       inc hl
       ld (hl),e
       dec hl
       pop de
       jr DelMem_4
$$:    add hl,bc
       ex de,hl
DelMem_4:
       inc hl
       inc hl
       inc hl
       inc hl
       call IsFixedName
       ld c,12
       jr nz,$F
       call ToVarTypeByte
       ld c,(hl)
       inc c
       inc c
       inc c
       inc c
$$:    ld b,0
       pop af
       or a
       sbc hl,bc
       ld bc,(OPBase)
       sbc hl,bc
       ret c
       add hl,bc
       jr DelMem_Loop
DelMem_1:
       push de
       push hl
       add hl,de
       ex de,hl
       ld hl,(FPS)
       or a
       sbc hl,de
       ld b,h
       ld c,l
       pop hl
       push hl
       ex de,hl
       jr z,$F
       ldir
$$:    pop de
       pop bc
       ret
DataSize:
       ld de,9
       and 1Fh
       ret z
       ld de,18
       cp 0Ch
       ret z
       ld e,(hl)
       inc hl
       ld d,(hl)
       dec hl
DataSizeContinue:
       push hl
       ex de,hl
       cp AppVarObj
       jr z,DataSize_1
       cp GroupObj
       jr z,DataSize_1
       cp TempProgObj
       jr z,DataSize_1
       cp 0Dh
       jr nz,$F
       add hl,hl
       jr DataSize_2
$$:    cp 3
       jr nc,DataSize_1
       cp 1
       call nz,DataSize_3
DataSize_2:
       call HLTimes9
       jr c,jpErrMemory
DataSize_1:
       ld de,2
       add hl,de
       jr c,jpErrMemory
       ex de,hl
       pop hl
       or a
       ret
DataSize_3:
       ld b,h
       ld h,0
       ld d,h
       ld e,h
       ex de,hl
       or a
$$:    adc hl,de
       djnz $B
       ret
HLTimes9:
       ld b,h
       ld c,l
       add hl,hl
       ret c
       add hl,hl
       ret c
       add hl,hl
       ret c
       add hl,bc
       ret
DelMemBC:
       push hl
       or a
       sbc hl,bc
       pop de
       push bc
       ld bc,(OPS)
       push hl
       sbc hl,bc
       ld b,h
       ld c,l
       pop hl
       push de
       jr z,$F
       lddr
$$:    ld (OPS),de
       pop de
       pop bc
       ld hl,(OPBase)
       add hl,bc
       ld (OPBase),hl
       call DelVarEntry
       ret
DelVar_3:
       call ToVarTypeByte
       ld a,(hl)
       cp 24h
       jr nz,$F
       xor a
       ret
$$:    ld b,0
       ld c,a
       or a
       sbc hl,bc
       ld a,(hl)
       ret
DelVar_5:
       ld hl,9320h
       push bc
       ld c,a
       and 0F8h
       rrca
       rrca
       rrca
       ld e,a
       ld d,0
       add hl,de
       push hl
       ld a,c
       and 7
       ld e,a
       ld a,(hl)
       ld hl,DelVar5_Table
       add hl,de
       ld c,(hl)
       and c
       ld a,c
       pop hl
       pop bc
       ret
DelVar5_Table:
       DB 01h,02h,04h,08h,10h,20h,40h,80h
jpErrArchived:
       jp JErrorNo
SetGraphTableDirty:
       bit 1,(iy+graphFlags)
       jr nz,$F
       set 1,(iy+smartFlags)
       bit 1,(iy+graphFlags)
       jr nz,$F
       set reTable,(iy+tblFlags)
$$:    set graphDraw,(iy+graphFlags)
       ret

IsFixedName:
       ld a,(hl)
       and 1Fh
       call IsANamedType
       ret z
       call IsList
       ret nz
       push hl
       call ToVarTypeByte
       ld a,(hl)
       cp 24h
       jr z,$F
       cp 72h
       jr z,$F
       cp 3Ah
       jr z,$F
       ld a,1
$$:    cp 1
       pop hl
       ret
IsList:cp 1
       ret z
       cp 0Dh
       ret

AdjustSymPointers:
       ld b,0
       push bc
       ld hl,(OPBase)
       or a
       sbc hl,bc
       ld (OPBase),hl
       ld a,(OP1+6)
       cp 24h
       jr z,AdjustSym_1
       ld hl,(pTemp)
       or a
       sbc hl,bc
       ld (pTemp),hl
       cp 72h
       jr z,$F
       cp 3Ah
       jr z,$F
       ld a,(OP1)
       and 1Fh
       call IsList
       jr z,AdjustSym_1
       call IsOP1NamedType
       jr z,AdjustSym_1
$$:    ld hl,(progPtr)
       or a
       sbc hl,bc
       ld (progPtr),hl
AdjustSym_1:
       ld de,(OPS)
       add hl,bc
       push hl
       sbc hl,de
       push af
       push hl
       ex de,hl
       sbc hl,bc
       ld (OPS),hl
       inc hl
       ld d,h
       ld e,l
       add hl,bc
       pop bc
       pop af
       jr z,$F
       ldir
$$:    pop de
       pop bc
       push de
       push bc
       ld b,c
       ld hl,OP1
       dec b
$$:    ld a,(hl)
       ld (de),a
       inc hl
       dec de
       djnz $B
       ld a,(OP1)
       and 1Fh
       call IsList
       jr nz,$F
       ld a,(OP1+6)
       cp 24h
       jr z,$F
       ld (hl),0
$$:    ld a,(hl)
       ld (de),a
       pop bc
       ld a,(OP1+6)
       cp 24h
       jr z,AdjustSym_2
       cp 5Eh
       jr z,$F
       cp 21h
       jr z,$F
       cp 23h
       jr nz,AdjustSym_3
$$:    ld a,(OP1+5)
       or a
       jr nz,AdjustSym_3
       ld hl,(OP1+3)
       ld (newDataPtr),hl
AdjustSym_3:
       call NegateBC
       pop de
       push de
       inc de
       call DelVarEntry_1
AdjustSym_2:
       pop hl
       ret
NegateBC:
       xor a
       ld h,a
       ld l,a
       sbc hl,bc
       ld b,h
       ld c,l
       ret
DelVarEntry:
       ld hl,(pTemp)
       or a
       sbc hl,de
       jr nc,$F
       add hl,de
       add hl,bc
       ld (pTemp),hl
$$:    ld hl,(progPtr)
       or a
       sbc hl,de
       jr nc,DelVarEntry_1
       add hl,de
       add hl,bc
       ld (progPtr),hl
DelVarEntry_1:
       ld hl,(editSym)
       or a
       sbc hl,de
       jr nc,$F
       add hl,de
       add hl,bc
       ld (editSym),hl
$$:    ld hl,(tSymPtr1)
       or a
       sbc hl,de
       jr nc,$F
       add hl,de
       add hl,bc
       ld (tSymPtr1),hl
$$:    ld hl,(tSymPtr2)
       or a
       sbc hl,de
       jr nc,$F
       add hl,de
       add hl,bc
       ld (tSymPtr2),hl
$$:    ld hl,(chkDelPtr3)
       or a
       sbc hl,de
       jr nc,$F
       add hl,de
       add hl,bc
       ld (chkDelPtr3),hl
$$:    ld hl,(chkDelPtr4)
       or a
       sbc hl,de
       jr nc,$F
       add hl,de
       add hl,bc
       ld (chkDelPtr4),hl
$$:    ld hl,(XOutSym)
       or a
       sbc hl,de
       jr nc,$F
       add hl,de
       add hl,bc
       ld (XOutSym),hl
$$:    ld hl,(YOutSym)
       or a
       sbc hl,de
       jr nc,$F
       add hl,de
       add hl,bc
       ld (YOutSym),hl
$$:    ld hl,(inputSym)
       or a
       sbc hl,de
       jr nc,$F
       add hl,de
       add hl,bc
       ld (inputSym),hl
       ret
$$:    ld hl,(fmtMatSym)
       or a
       sbc hl,de
       ret nc
       add hl,de
       add hl,bc
       ld (fmtMatSym),hl
       ret

AdjustDataPointers:
       push af
       ld hl,(fpBase)
       push bc
       push hl
       add hl,bc
       ld (fpBase),hl
       sbc hl,bc
       ex de,hl
       ld hl,(FPS)
       sbc hl,de
       push hl
       push af
       add hl,de
       add hl,bc
       ld (FPS),hl
       dec hl
       ld d,h
       ld e,l
       sbc hl,bc
       pop af
       pop bc
       jr z,AdjustData_1
$$:    ldd
       ldd
       ldd
       ldd
       ldd
       ldd
       ldd
       ldd
       ldd
       ld a,b
       or c
       jr nz,$B
AdjustData_1:
       pop de
       pop bc
       pop af
       jr z,AdjustData_2
       ld hl,(tempMem)
       add hl,bc
       ld (tempMem),hl
       ld hl,(newDataPtr)
       push hl
       ex de,hl
       sbc hl,de
       jr z,AdjustData_3
       push bc
       ld b,h
       ld c,l
       add hl,de
       dec hl
       ld de,(fpBase)
       dec de
       lddr
       ex de,hl
       pop bc
       call NegateBC
       call AdjustData_4
       ld bc,(freeRAM)
$$:    call AdjustBasicPointers
       call AdjustData_8
       pop de
       ret
AdjustData_8:
       ld hl,(iMathPtr1)
       or a
       sbc hl,de
       jr c,$F
       jr z,$F
       add hl,de
       or a
       sbc hl,bc
       ld (iMathPtr1),hl
$$:    ld hl,(iMathPtr2)
       or a
       sbc hl,de
       jr c,$F
       jr z,$F
       add hl,de
       or a
       sbc hl,bc
       ld (iMathPtr2),hl
$$:    ld hl,(iMathPtr3)
       or a
       sbc hl,de
       jr c,$F
       jr z,$F
       add hl,de
       or a
       sbc hl,bc
       ld (iMathPtr3),hl
$$:    ld hl,(iMathPtr4)
       or a
       sbc hl,de
       jr c,$F
       jr z,$F
       add hl,de
       or a
       sbc hl,bc
       ld (iMathPtr4),hl
$$:    ld hl,(iMathPtr5)
       or a
       sbc hl,de
       jr c,ADJM7
       jr z,ADJM7
       add hl,de
       or a
       sbc hl,bc
       ld (iMathPtr5),hl
ADJM7: ld hl,(asm_data_ptr1)
       or a
       sbc hl,de
       jr c,$F
       jr z,$F
       add hl,de
       or a
       sbc hl,bc
       ld (asm_data_ptr1),hl
$$:    ld hl,(asm_data_ptr2)
       or a
       sbc hl,de
       jr c,$F
       jr z,$F
       add hl,de
       or a
       sbc hl,bc
       ld (asm_data_ptr2),hl
$$:    ld hl,(fmtMatMem)
       or a
       sbc hl,de
       jr c,$F
       jr z,$F
       add hl,de
       or a
       sbc hl,bc
       ld (fmtMatMem),hl
$$:    ld hl,(newDataPtr)
       or a
       sbc hl,de
       jr c,$F
       jr z,$F
       add hl,de
       or a
       sbc hl,bc
       ld (newDataPtr),hl
$$:    ld hl,(EQS)
       or a
       sbc hl,de
       jr c,$F
       jr z,$F
       add hl,de
       or a
       sbc hl,bc
       ld (EQS),hl
$$:    ld hl,(equPtr1)
       or a
       sbc hl,de
       jr c,$F
       jr z,$F
       add hl,de
       or a
       sbc hl,bc
       ld (equPtr1),hl
$$:    ld hl,(ES)
       or a
       sbc hl,de
       jr c,$F
       jr z,$F
       add hl,de
       or a
       sbc hl,bc
       ld (ES),hl
$$:    ld hl,(insDelPtr)
       or a
       sbc hl,de
       jr c,$F
       jr z,$F
       add hl,de
       or a
       sbc hl,bc
       ld (insDelPtr),hl
$$:    ld hl,(customPtr1)
       or a
       sbc hl,de
       jr c,$F
       jr z,$F
       add hl,de
       or a
       sbc hl,bc
       ld (customPtr1),hl
$$:    ld hl,(customPtr2)
       or a
       sbc hl,de
       jr c,$F
       jr z,$F
       add hl,de
       or a
       sbc hl,bc
       ld (customPtr2),hl
$$:    ld hl,(editDat)
       or a
       sbc hl,de
       jr c,$F
       jr z,$F
       add hl,de
       or a
       sbc hl,bc
       ld (editDat),hl
$$:    ld hl,(chkDelPtr1)
       or a
       sbc hl,de
       jr c,$F
       jr z,$F
       add hl,de
       or a
       sbc hl,bc
       ld (chkDelPtr1),hl
$$:    ld hl,(XOutDat)
       or a
       sbc hl,de
       jr c,$F
       jr z,$F
       add hl,de
       or a
       sbc hl,bc
       ld (XOutDat),hl
$$:    ld hl,(YOutDat)
       or a
       sbc hl,de
       jr c,$F
       jr z,$F
       add hl,de
       or a
       sbc hl,bc
       ld (YOutDat),hl
$$:    ld hl,(fOutDat)
       or a
       sbc hl,de
       jr c,$F
       jr z,$F
       add hl,de
       or a
       sbc hl,bc
       ld (fOutDat),hl
$$:    ld hl,(customPtr3)
       or a
       sbc hl,de
       jr c,$F
       jr z,$F
       add hl,de
       or a
       sbc hl,bc
       ld (customPtr3),hl
$$:    ld hl,(inputDat)
       or a
       sbc hl,de
       jr c,$F
       jr z,$F
       add hl,de
       or a
       sbc hl,bc
       ld (inputDat),hl
$$:    ld hl,(chkDelPtr2)
       or a
       sbc hl,de
       ret c
       ret z
       add hl,de
       or a
       sbc hl,bc
       ld (chkDelPtr2),hl
       ret
AdjustBasicPointers:
       ld hl,(basic_start)
       or a
       sbc hl,de
       ret c
       ret z
       add hl,de
       sbc hl,bc
       ld (basic_start),hl
       ld hl,(nextParseByte)
       or a
       sbc hl,bc
       ld (nextParseByte),hl
       ld hl,(basic_end)
       or a
       sbc hl,bc
       ld (basic_end),hl
       ret
AdjustData_2:
       push de
AdjustData_3:
       dec de
       call NegateBC
       jr $B
AdjustData_4:
       ld hl,symTable
       ld (freeRAM),bc
       dec hl
       dec hl
       dec hl
AdjustData_5:
       push af
       ld c,(hl)
       dec hl
       ld b,(hl)
       dec hl
       ld a,(hl)
       inc hl
       or a
       jr nz,AdjustData_6
       ex de,hl
       or a
       sbc hl,bc
       jr nc,$F
       add hl,bc
       push hl
       ld h,b
       ld l,c
       ld bc,(freeRAM)
       or a
       sbc hl,bc
       ex de,hl
       ld (hl),d
       inc hl
       ld (hl),e
       dec hl
       pop de
       jr AdjustData_6
$$:    add hl,bc
       ex de,hl
AdjustData_6:
       inc hl
       inc hl
       inc hl
       inc hl
       call IsFixedName
       ld c,12
       jr nz,$F
       call ToVarTypeByte
       ld c,(hl)
       inc c
       inc c
       inc c
       inc c
$$:    ld b,0
       pop af
       or a
       sbc hl,bc
       ld bc,(OPBase)
       sbc hl,bc
       ret c
       add hl,bc
       jr AdjustData_5

