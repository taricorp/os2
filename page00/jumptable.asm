 DEFINE PAGE00, SPACE=ROM
 SEGMENT PAGE00

 include "includes\os2.inc"

 PUBLIC _OutputProtectedPort,Page0Call,_GetCharacterBitmap,_JForceHexEditor,_GetSmallFontCharacterBitmap
 PUBLIC _HandleUSBACablePluggedIn,_HandleUSBBCablePluggedIn,_HandleUSBACableUnplugged,_HandleUSBBCableUnplugged
 PUBLIC _HandleUSBInterruptInitialize,_HandleDefaultUSBInterrupt,_ReadUSBInterruptData,_GetSStringLength
 EXTERN OutputProtectedPort,GetCharacterBitmap,JForceHexEditor,GetSStringLength
 EXTERN HandleUSBACablePluggedIn,HandleUSBBCablePluggedIn,HandleUSBACableUnplugged,HandleUSBBCableUnplugged
 EXTERN HandleUSBInterruptInitialize,HandleDefaultUSBInterrupt,ReadUSBInterruptData,GetSmallFontCharacterBitmap

Page0Call:
       push hl
       push hl
       push af
       push de
       push hl
       ld hl,11
       add hl,sp
       ld d,(hl)
       dec hl
       ld e,(hl)
       in a,(6)
       ld (hl),a
       dec hl
       ;HACK: until I figure out how to shift address bits in ZDS...
       ld (hl),00h
       dec hl
       ld (hl),75h
       dec hl
       dec hl
       ld a,(de)
       inc de
       ld (hl),a
       inc hl
       ld a,(de)
       inc de
       ld (hl),a
       in a,(2)
       and 80h
       jr nz,$F
       ld a,(de)
       and 1Fh
       jr Page0Call_1
$$:    in a,(21h)
       and 3
       ld a,(de)
       jr nz,Page0Call_1
       and 3Fh
Page0Call_1:
       out (6),a
       pop hl
       pop de
       pop af
       ret

_OutputProtectedPort:
       call Page0Call
       DW OutputProtectedPort
       DB 7Ch
_GetCharacterBitmap:
       call Page0Call
       DW GetCharacterBitmap
       DB 01h
_GetSmallFontCharacterBitmap:
       call Page0Call
       DW GetSmallFontCharacterBitmap
       DB 01h
_HandleUSBACablePluggedIn:
       call Page0Call
       DW HandleUSBACablePluggedIn
       DB 02h
_HandleUSBACableUnplugged:
       call Page0Call
       DW HandleUSBACableUnplugged
       DB 02h
_HandleUSBBCablePluggedIn:
       call Page0Call
       DW HandleUSBBCablePluggedIn
       DB 02h
_HandleUSBBCableUnplugged:
       call Page0Call
       DW HandleUSBBCableUnplugged
       DB 02h
_HandleUSBInterruptInitialize:
       call Page0Call
       DW HandleUSBInterruptInitialize
       DB 02h
_HandleDefaultUSBInterrupt:
       call Page0Call
       DW HandleDefaultUSBInterrupt
       DB 02h
_ReadUSBInterruptData:
       call Page0Call
       DW ReadUSBInterruptData
       DB 02h
_JForceHexEditor:
       call Page0Call
       DW JForceHexEditor
       DB 00h
_GetSStringLength:
       call Page0Call
       DW GetSStringLength
       DB 01h

