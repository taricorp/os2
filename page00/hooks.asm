 DEFINE PAGE00, SPACE=ROM
 SEGMENT PAGE00

 include "includes\os2.inc"

 PUBLIC CallUSBActivityHook,RunLinkActivityHook
 EXTERN GetBytePaged,NZIf83Plus,MakeOffPageCall,ZIfSlowSpeed,Page0Call

RunLinkActivityHook:
       push af
       ld hl,LinkActivityHookBlock
       call GetHookByte
       jr z,RunHook
       pop af
       res LinkActivityHookEnabled,(iy+scriptFlag)
       ret
RunHook:
       pop af
       ld (tempWord),a
       in a,(speedPort)
       push af
       bit runAtFastSpeed,(iy+customFlag1)
       call ZIfSlowSpeed
       pop af
       push af
       ld a,(tempWord)
       push hl
       ld hl,nestedHookLevels
       inc (hl)
       ld hl,RunHookSafeRet
       ex (sp),hl
       push hl
       ld hl,RunHookRet
       ex (sp),hl
       push hl
       ld hl,hookExecTemp
       ex (sp),hl
       jr Page0Call
RunHookSafeRet:
       pop af
       push hl
       ld hl,nestedHookLevels
       dec (hl)
       pop hl
       call NZIf83Plus
       jr nz,$F
       and 1
       out (speedPort),a
$$:    pop af
       pop af
       pop af
       ret
RunHookRet:
       push af
       push hl
       ld hl,nestedHookLevels
       dec (hl)
       pop hl
       call NZIf83Plus
       jr nz,$F
       pop af
       ld (tempWord),bc
       pop bc
       pop bc
       push af
       ld a,b
       and 1
       out (speedPort),a
       pop af
       ld bc,(tempWord)
       ret
$$:    pop af
       inc sp
       inc sp
       inc sp
       inc sp
       ret
GetHookByte:
       push bc
       push de
       ld e,(hl)
       inc hl
       ld d,(hl)
       inc hl
       ld a,(hl)
       cp 0FFh
       jr z,$F
       ld (hookExecTemp+2),a
       ex de,hl
       call GetBytePaged
       inc hl
       ld (hookExecTemp),hl
       ld a,b
$$:    pop de
       pop bc
       cp 83h
       ret

CallUSBActivityHook:
       ld hl,USBActivityHookBlock
       ld e,(hl)
       inc hl
       ld d,(hl)
       inc hl
       ld a,(hl)
       cp 0FFh
       jr z,noUSBActivityHook
       push bc
       ld (offPageCallPage),a
       ex de,hl
       call GetBytePaged
       inc hl
       ld a,b
       cp 83h
       pop bc
       jr z,$F
noUSBActivityHook:
       res USBActivityHookEnabled,(iy+hookFlags4)
       or 1
       ret
$$:    ld (offPageCallAddress),hl
       call MakeOffPageCall
       ld a,b
       or a
       ret

